<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use App\Helpers\Helper;
use App\Mail\WelcomeUser;
use Mail;
use Session;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    /**
     * Validate and create a newly registered user.
     *
     * @param  array  $input
     * @return \App\Models\User
     */
    /*public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => $this->passwordRules(),
        ])->validate();

        return User::create([
            'name' => $input['name'],
            'email' => $input['email'],
            'password' => Hash::make($input['password']),
        ]);
    }*/
    public function create(array $input)
    {
        Validator::make($input, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'country' => ['required'],
            'webinar' => ['required','exists:webinars,id'],
            'phone' => ['required'],
            //'password' => $this->passwordRules(),
        ])->validate();

        $webinar = Helper::getLiveWebinarById($input['webinar']);
        
        if(!$webinar){
            return back()->with(['error'=>'Webinar ends!']);
        }

        $user = User::updateOrcreate(['email'=>$input['email']], [
            'name' => $input['name'],
            'email' => $input['email'],
            'phone' => $input['phone'],
            'password' => '123456789',
            'country' => $input['country'],
            'state' => !empty($input['state']) ? $input['state'] : 0
        ]);
        $user->webinars()->attach($input['webinar']);
        
        Mail::to($user->email)->send(new WelcomeUser($user, $webinar));
        Session::put(['webinar'=>$webinar]);
        Helper::prt($user);
        return $user;
    }
}
