<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\Front\CronController;

class WebinarReminderMailCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'webinar_reminder_mail:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cronMethods = new CronController;
        $cronMethods->webinar_reminder();
        \Log::info('WebinarReminderMailCron Ran at:'.date('Y-m-d h:i:s'));
        $this->info('WebinarReminderMailCron:Cron Command Run successfully!');
        return 0;
    }
}