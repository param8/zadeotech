<?php

use Auth as myauth;
use Carbon\Carbon;
 
function prt($data,$die=false)
{
  echo "<pre>";
  print_r($data);
  echo "</pre>";
  
  if($die==true)
  die();
}
function getCompanyId()
{
	if(myauth::guard('company')->check()){
		return myauth::guard('company')->id();
	}
}
function getAdminId()
{
	if(myauth::guard('admin')->check()){
		return myauth::guard('admin')->id();
	}
}
function admin_loggedin()
{
	if(myauth::guard('company')->check()){
		return $user = Auth::guard('company')->user();
	}else if(myauth::guard('moderator')->check()){
		return $user = Auth::guard('moderator')->user();
	}
}
function is_copmpany()
{
	if(myauth::guard('company')->check()){
		return true;
	}
}
function is_moderator()
{
	if(myauth::guard('moderator')->check()){
		return myauth::guard('moderator')->user();
	}
}
function webinarStartDateTime($webinar)
{
	$dt = $webinar->start_date->format('Y-m-d'. ' '.$webinar->start_time);
	$dt = Carbon::parse($dt);
	return $dt->isoFormat('MMM Do YYYY hh:mm A');
}