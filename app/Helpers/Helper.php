<?php
namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use App\Models\Webinar;
use App\Models\Quiz;
use App\Models\Poll;
use App\Models\UserQuizAnswer;
use App\Models\UserPollAnswer;

class Helper
{

  public static function dateRangeParse($date_range)
  {
    $date_range_exp = explode('-',$date_range);

    return [
      'from' => Carbon::parse($date_range_exp[0])->startOfDay(),
      'to' => Carbon::parse($date_range_exp[1])->startOfDay()
    ];
  }

  public static function toDateRange($start_date,$end_date)
  {
    $start_date = Carbon::parse($start_date)->format('m/d/Y h:i A');
    $end_date = Carbon::parse($end_date)->format('m/d/Y h:i A');
    return $date_range = $start_date.' - '.$end_date;
  }
  public static function changeDateFormat($date)
  {
    return $date->format('m-d-Y, h:i:s');
  }
  public static function changeTimeFormat($time)
  {
    return Carbon::createFromFormat('H:i:s', $time)->format('h:i A');
  }

  static public function getWebinar($webinar_id)
  {
    return $webinar = Webinar::whereCompanyId(getCompanyId())->whereId($webinar_id)->latest()->first();
  }
  public static function generatePassword()
  {
    $n=8; 
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = null;
    for ($i = 0; $i < $n; $i++) { 
        $index = rand(0, strlen($characters) - 1); 
        $randomString .= $characters[$index]; 
    } 
    return $randomString; 
  }
  static public function getLiveWebinarById($webinar_id)
  {
    //return $webinar = Webinar::whereDate('end_date','<=',Carbon::now())->whereId($webinar_id)->latest()->first();
    return $webinar = Webinar::whereId($webinar_id)->latest()->first();
    return $webinar = Webinar::whereDate('end_date','<=',Carbon::now())->whereId($webinar_id)->latest()->first();
  }
  public static function getUnAnsweredQuizByWebinarId($webinar_id)
  {
    $userQuizAnswer = UserQuizAnswer::select('quiz_id')->whereUserId(Auth::id())->get()->pluck('quiz_id')->toArray();

    $quizzes_q = Quiz::whereWebinarId($webinar_id)->whereStatus(1);

    if(count($userQuizAnswer)){
        $quizzes_q->whereNotIn('id',$userQuizAnswer);
    }

    return $quizzes_q->paginate(1);
  }
  public static function getUnAnsweredPollByWebinarId($webinar_id)
  {
    $userPollAnswer = UserPollAnswer::select('poll_id')->whereUserId(Auth::id())->get()->pluck('poll_id')->toArray();

    $polls_q = Poll::whereWebinarId($webinar_id)->whereStatus(1);

    if(count($userPollAnswer)){
        $polls_q->whereNotIn('id',$userPollAnswer);
    }

    return $polls_q->paginate(1);
  }
  public static function userQuizResult($usr, $webinar)
  {
    if($webinar->quizzes->count())
    {
      $user_quiz_answers = UserQuizAnswer::
            whereIn('quiz_id', $webinar->quizzes->pluck('id'))
            ->where('user_id',$usr->id)->with('correct_option')->get();
          
      $i = 0;
      if($user_quiz_answers->count())
      {
        foreach($user_quiz_answers as $cur_and):
          if($cur_and->correct_option)
            $i++;

        endforeach;
      }
      return $i.'/'.$user_quiz_answers->count();
    }
  }
  public static function userQuizResult_old($user)
  {
    $count = $user->user_quiz_answers->count(); 
    $correct_ans_count = 0;
    foreach($user->user_quiz_answers as $value){
      $user_ans_option_id = $value->option_id;
      foreach($value->quiz->options as $options)
      {
        if(($options->is_correct == 1) and ($options->id==$user_ans_option_id)){
          $correct_ans_count++;
        }
      }
    }
    return $correct_ans_count.'/'.$count;
  }
  static public function getQuizSummery($quiz)
  {
    $total_answers = self::getTotalAnswer($quiz);
    $total_correct_answer = self::getTotalCorrectAnswer($quiz);

    
    return [
      'total_answers' => $total_answers,
      'total_correct_answer' => $total_correct_answer,
      'percent' => self::calculatePercent($total_answers,$total_correct_answer)
    ];
  }
  public static function calculatePercent($total_answers,$total_correct_answer)
  {
    if($total_answers!=0 and $total_correct_answer!=0)
    {
      $percent = $total_correct_answer/$total_answers;
      return $percent_friendly = number_format( $percent * 100, 1 ) . '%';
    }
    return 0;
  }
  static public function getTotalAnswer($quiz)
  {
    return $quiz->user_quiz_answers->count();
  }
  static public function getTotalCorrectAnswer($quiz)
  {
    return $quiz->user_quiz_answers->where('option_id',$quiz->correct_options->id)->count();
  }
  public static function sharePollResultView($webinar_id)
  {
    $upa = UserPollAnswer::select('poll_id')->whereUserId(Auth::id())->get()->pluck('poll_id')->toArray();
    return Poll::where('webinar_id',$webinar_id)
                  ->where('polls.webinar_id',$webinar_id)
                  ->where('share_result',1)
                  ->find($upa);
    
  }
  public static function shareQuizResultView($webinar_id)
  {
    $uqa = UserQuizAnswer::select('quiz_id')->whereUserId(Auth::id())->get()->pluck('quiz_id')->toArray();
    return Quiz::where('webinar_id',$webinar_id)
                  ->where('quizzes.webinar_id',$webinar_id)
                  ->where('share_result',1)
                  ->find($uqa);
    
  }
  public static function getOptionalFieldUserData($user_id,$webinar_id)
  {
    $a = \DB::table('user_webinar')->select('optional_field')->where('user_id',$user_id)->where('webinar_id',$webinar_id)->orderBy('id','desc')->first();
    
    if($a)
      return $a->optional_field;
  }
}