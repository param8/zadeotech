<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Mail\CompanyApproveMail;
use App\Helpers\Helper;
use Mail;
use Carbon\Carbon;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    public function index(Request $request)
    {
    	$query = Company::latest();

        if($request->has('filter'))
        {
            if(!empty($request->key))
            {
                $query->where('company_name', 'LIKE', '%'.$request->key.'%')
                ->orWhere('email', 'LIKE', '%'.$request->key.'%')
                ->orWhere('display_name', 'LIKE', '%'.$request->key.'%');
            }
            if(!empty($request->approve_status) and $request->approve_status=='approved')
            {
                $query->whereNotNull('approved_at');
            }
            if(!empty($request->approve_status) and $request->approve_status=='pending')
            {
                $query->whereNull('approved_at')->whereNull('rejected_at');
            }
            
            if(!empty($request->approve_status) and $request->approve_status=='rejected')
            {
                $query->whereNotNull('rejected_at');
            }
            if(!empty($request->status) and $request->status=='activated')
            {
                $query->whereStatus(1);
            }
            if(!empty($request->status) and $request->status=='deactivated')
            {
                $query->whereStatus(0)->whereNotNull('approved_at');
            }
        }

        $companies = $query->paginate(30);
        return view('admin.company.list', ['companies'=>$companies]);
    }
    public function export()
    {
        $companies = Company::latest()->get();
        if($companies->count()<1)
            return back()->with('error','No companies to export.');

        $fileName = 'Company-list.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Display Name', 'First Name', 'Last Name', 'Email', 'Phone', 'Approve Status', 'Status', 'Created At'];

        $callback = function() use($companies, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($companies as $company) {

                $ap_status = "Pending";
                if(!empty($company->approved_at)){
                    $ap_status = 'Approved';
                }elseif(!empty($company->rejected_at)){
                    $ap_status = 'Rejected';
                }

                $status = null;
                if($company->status==1){
                    $status = 'Activated';
                }else{
                    $status = 'Deactivated';
                }

                $row['Display Name']  = $company->company_name;
                $row['First Name']  = $company->first_name;
                $row['Last Name']  = $company->last_name;
                $row['Email']  = $company->email;
                $row['Phone']  = $company->phone;
                $row['Approve Status']  = $ap_status;
                $row['Status']  = $status;
                $row['Created At']  = $company->created_at;
                

               fputcsv($file, array($row['Display Name'], $row['First Name'], $row['Last Name'], $row['Email'], $row['Phone'], $row['Approve Status'], $row['Status'], $row['Created At']));
            }

         fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
    public function deactivate(Company $company)
    {
    	$company->update(['status'=>0]);
    	return back()->with('success','Company deactivated.');
    }
    public function reactivate(Company $company)
    {
    	$password = Helper::generatePassword();
    	$company->update(['password'=>\Hash::make($password), 'status'=>1]);
        
        Mail::to($company->email)->send(new CompanyApproveMail($company, $password));

    	return back()->with('success','Company reactivated.');
    }
    
    public function approve(Request $request, Company $company)
    {
        if($request->has('approve'))
        {
            $rules = [
                'slug'   => 'required|min:2|unique:companies,slug,'.$company->id,
                'password'   => 'required|min:6',
            ];

            if ($request->hasFile('image')) {
                $rules['image'] = 'image|mimes:jpeg,png,jpg|max:2048';
            }
            
            $this->validate($request, $rules);

            //$password = Helper::generatePassword();
            $password = $request->password;
            $data = [
                'password'=>\Hash::make($password),
                'slug' => \Str::slug($request->slug),
                'status'=>1,
                'rejected_at' => null,
                'approved_at' => Carbon::now()
            ];

            if($request->hasFile('image'))
            {
                //Upload banner
                $image = 'bg-image-'.time().'.'.$request->image->extension();  
                $request->image->move(public_path('uploads/company'), $image);

                $data['bg_image'] = $image;
            }

            $company->update($data);
            
            Mail::to($company->email)->send(new CompanyApproveMail($company, $password));
            return back()->with('success','Company Approved.');
        }
        else
        {
            $company->update([
                'rejected_at' => Carbon::now(),
                'status' => 0
            ]);
            return back()->with('success','Company Rejected.');
        }

    }
    
    public function edit(Company $company)
    {
    	return view('admin.company.edit', ['company'=>$company]);
    }
    public function update(Request $request, Company $company)
    {
        $rules = [
            'company_name'   => 'required|min:2|max:191',
            'slug'   => 'required|min:2|unique:companies,slug,'.$company->id,
            'display_name'   => 'required|min:2|max:191|unique:companies,id,'.getCompanyId(),
            'first_name'   => 'required|min:2|max:100',
            'last_name'   => 'required|min:2|max:100',
            'company_address'   => 'required|min:2|max:200',
            'email'   => 'required|email|unique:companies,id,'.getCompanyId(),
            'phone'   => 'required|min:8|max:13',
            'logo'   => 'image|mimes:jpeg,png,jpg|max:2048',
        ];
        if($request->has('logo'))
        {
            $rules['logo'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('image')) {
            $rules['image'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        $this->validate($request, $rules);

        $slug = \Str::slug($request->slug);

        $data = [
            'company_name'   => $request->company_name,
            'display_name'   => $request->display_name,
            'slug'   => $slug,
            'first_name'   => $request->first_name,
            'last_name'   => $request->last_name,
            'company_address'   => $request->company_address,
            'email'   => $request->email,
            'phone'   => $request->phone,
            'gstin'   => $request->gstin,
            'pan'     => $request->pan
        ];

        if($request->has('password')){
            $data['password'] = \Hash::make($request->password);
        }

        if($request->has('logo'))
        {
            $logo = $slug.'-'.time().'.'.$request->logo->extension();  
            $request->logo->move(public_path('uploads/company_logo'), $logo);
            $data['logo'] = $logo;
        }

        if($request->hasFile('image'))
        {
            //Upload banner
            $image = 'bg-image-'.time().'.'.$request->image->extension();  
            $request->image->move(public_path('uploads/company'), $image);

            $data['bg_image'] = $image;
        }

        $company = Company::find($company->id)->update($data);
        return redirect()->route('admin.company')->with('success','Company Profile updated successfully.');
    }
    public function delete(Company $company)
    {
        $company->delete();
        return back()->with(['success'=>'Company deleted successfully.']);
    }
    public function trashed()
    {
        $companies = Company::onlyTrashed()->latest()->paginate(30);
        return view('admin.company.recycle-company', ['companies'=>$companies]);
    }
    public function restore(Request $request)
    {
        $company = Company::onlyTrashed()->find($request->company_id);
        $company->restore();
        return back()->with(['success'=>'Company restored successfully.']);

    }
}
