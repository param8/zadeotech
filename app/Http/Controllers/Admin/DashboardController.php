<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rules\MatchOldPasswordAdmin;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;
use App\Models\Company;
use Auth;

class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function dashboard()
    {
        $aproved = Company::whereNotNull('approved_at')->count();
        $pending = Company::whereNull('approved_at')->whereNull('rejected_at')->count();
        //$rejected = Company::whereNotNull('rejected_at')->count();
        $deactivated = Company::whereNotNull('approved_at')->whereStatus(0)->count();
    	return view('admin.dashboard', ['aproved'=>$aproved, 'pending' => $pending, 'deactivated' => $deactivated]);
    }
    public function change_password()
    {
    	return view('admin.change-password');
    }
    public function update_password(Request $request)
    {
    	$request->validate([
            'current_password' => ['required', new MatchOldPasswordAdmin],
            'new_password' => ['required','min:6'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        Admin::find(getAdminId())->update(['password'=> Hash::make($request->new_password)]);

        return back()->with('success', 'Password updated successfully.');
    }
}
