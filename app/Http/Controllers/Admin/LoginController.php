<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Mail\AdminPasswordResetMail;
use App\Mail\AdminPasswordResetSuccessMail;
use Mail;
use Session;
use Illuminate\Support\Facades\Password;
use DB;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:admin')->except('logout');
    }

    public function login_form()
    {
        return view('admin.auth.login');
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password, 'status'=>1])) {

            return redirect()->route('admin.dashboard');
        }

        return back()->withInput($request->only('email'));
    }
    public function broker()
    {
        return Password::broker('admins');
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function password_reset_form(Request $request)
    {
        return view('admin.auth.forgot-password');
    }
    public function password_reset_request(Request $request)
    {
        $request->validate(['email' => 'required|email']);


        $user = Admin::whereEmail($request->email)
                ->whereStatus(1)
                ->first();

        //Check if the user exists
        if (!$user) {
            return back()->withErrors(['email' => 'Email does not exists or account may have been disabled.']);
        }

        PasswordReset::whereEmail($request->email)->delete();

        //Create Password Reset Token
        $tokenData = PasswordReset::create([
            'email' => $request->email,
            'token' => \Str::random(60)
        ]);
        

        $link =  url('admin/password/reset/' . $tokenData->token . '?email=' . urlencode($user->email));
        Mail::to($user->email)->send(new AdminPasswordResetMail($user, $link));

        
        return back()->with('success', trans('We have emailed your password reset link!'));
        
    }
    
    public function enter_password_form(Request $request)
    {
        if(!isset($request->email) or !isset($request->token))
            return redirect()->route('admin.login');

        return view('admin.auth.reset',['email'=>$request->email, 'token'=>$request->token]);
    }
    public function enter_reset(Request $request)
    {
        //Validate input
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email|exists:admins,email',
            'password' => 'required|confirmed',
            'token' => 'required'
        ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return back()->withErrors(['email' => 'Please complete the form']);
        }

        $password = $request->password;
        // Validate the token
        $tokenData = PasswordReset::where('token', $request->token)->latest()->first();
        // Redirect the user back to the password reset request form if the token is invalid
        if (!$tokenData) return redirect()->route('admin.login');

        $user = Admin::whereEmail($request->email)
                ->whereStatus(1)
                ->first();

        // Redirect the user back if the email is invalid
        if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);
        //Hash and update the new password
        $user->password = \Hash::make($password);
        $user->update(); //or $user->save();

        //login the user immediately they change password successfully
        Auth::guard('admin')->login($user);

        //Delete the token
        $tokenData->delete();

        Mail::to($request->email)->send(new AdminPasswordResetSuccessMail($user));

        return redirect()->route('admin.dashboard')->with('success','Your password has been reset!');
    }
}
