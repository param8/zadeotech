<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;

class ManagerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $managers = Admin::where('id','!=',1)->get();
        return view('admin.manager.list', ['managers'=>$managers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.manager.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule = [
            'name'   => 'required|min:2',
            'email'   => 'required|email|unique:admins',
            'password'   => 'required|min:6',
        ];

        $this->validate($request, $rule);


        $data = [
            'name'   => $request->name,
            'email'   => $request->email,
            'password'   => $request->password,
            'status'    => 1
        ];

        Admin::create($data);

        return redirect()->route('admin.manager')->with('success','Manager created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Admin $admin)
    {
        return view('admin.manager.update', ['admin'=>$admin]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Admin $admin)
    {
        $rule = [
            'name'   => 'required|min:2',
            'email'   => 'required|min:2|unique:admins,email,'.$admin->id
        ];

        $data = [
            'name'   => $request->name,
            'email'   => $request->email
        ];

        if($request->has('password') and !empty($request->password))
        {
            $rule['password'] = 'required|min:6';
            $data['password'] = \Hash::make($request->password);
        }

        $this->validate($request, $rule);

        Admin::find($admin->id)->update($data);

        return redirect()->route('admin.manager')->with('success','Manager updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Admin $admin)
    {
        if($admin->id == 1)
            return redirect()->route('admin.manager')->with('error', 'Sorry something went wrong. Please try again.');

        $admin->delete();
        return back()->with('success', 'Manager deleted successfully.');
    }
}