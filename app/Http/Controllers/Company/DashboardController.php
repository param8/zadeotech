<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;
use App\Models\Company;
use App\Models\Webinar;
use Auth;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:company');
    }

    public function dashboard()
    {

        /*$total_webinar_today = Webinar::whereDate('start_date','>=',DATE('2020-10-30 00:00:00'))
                                        ->orWhereDate('end_date','<=',DATE('2020-10-30 59:59:59'))
                                        ->count();*/
        $todaySod = Carbon::now()->startOfDay()->toDateTimeString();
        $todayEod = Carbon::now()->endOfDay()->toDateTimeString();
        $todayNow = Carbon::now()->toDateTimeString();
        $todayDate = Carbon::now()->format('Y-m-d');
        //prt($todayDate);
        //prt([$todaySod,$todayEod]);

        //$total_webinar_today = Webinar::whereBetween('start_date', [$todaySod, $todayEod])->count();

        /*
        Today
        SELECT * FROM webinars WHERE company_id=2 AND (start_date BETWEEN '2020-11-07' AND '2020-11-07' OR '2020-11-07' BETWEEN start_date AND end_date)
        */
        //echo $todayDate;
        $company_id = Auth::guard('company')->id();
        $today_webinars = DB::select("SELECT * FROM webinars WHERE company_id='$company_id' AND status=1 AND (start_date BETWEEN '$todayDate' AND '$todayDate' OR '$todayDate' BETWEEN start_date AND end_date)");
        
        //prt(count($today_webinars));


        /*$total_webinar_today = Webinar::whereCompany_id($company_id)
                                        ->whereDate('start_date', '>=',date('Y-m-d'))
                                        //->whereDate('end_date', '<=',date('Y-m-d'))
                                        ->get();*/
        //prt($total_webinar_today->toArray());
        $total_upcoming_webinar = Webinar::whereCompany_id($company_id)
                                            ->whereDate('start_date','>=',DATE($todaySod))
                                            ->count();

        $total_completed_webinar = Webinar::whereCompany_id($company_id)
                                            ->whereDate('end_date','<=',Carbon::now())
                                            ->count();
    	return view('company.dashboard', [
            'total_webinar_today' => count($today_webinars),
            'total_upcoming_webinar' => $total_upcoming_webinar,
            'total_completed_webinar' => $total_completed_webinar
        ]);
    }
    
    public function manage_profile()
    {
    	return view('company.manage-profile');
    }
    public function update_profile(Request $request)
    {
        $rules = [
            'company_name'   => 'required|min:2|max:191',
            'display_name'   => 'required|min:2|max:191|unique:companies,id,'.getCompanyId(),
            'first_name'   => 'required|min:2|max:100',
            'last_name'   => 'required|min:2|max:100',
            'company_address'   => 'required|min:2|max:200',
            'email'   => 'required|email|unique:companies,id,'.getCompanyId(),
            'phone'   => 'required|min:8|max:13',
            'logo'   => 'image|mimes:jpeg,png,jpg|max:2048',
        ];
        if($request->has('logo'))
        {
            $rules['logo'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        $this->validate($request, $rules);

        $slug = \Str::slug($request->display_name);

        $data = [
            'company_name'   => $request->company_name,
            'display_name'   => $request->display_name,
            'slug'   => $slug,
            'first_name'   => $request->first_name,
            'last_name'   => $request->last_name,
            'company_address'   => $request->company_address,
            'email'   => $request->email,
            'phone'   => $request->phone
        ];

        if($request->has('logo'))
        {
            $logo = $slug.'-'.time().'.'.$request->logo->extension();  
            $request->logo->move(public_path('uploads/company_logo'), $logo);
            $data['logo'] = $logo;
        }

        $company = Company::find(getCompanyId())->update($data);
        return back()->with('success','Profile updated successfully.');

    }
    public function change_password()
    {
    	return view('company.change-password');
    }
    public function update_password(Request $request)
    {
    	$request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required','min:6'],
            'new_confirm_password' => ['same:new_password'],
        ]);
   
        Company::find(getCompanyId())->update([
            'password'=> Hash::make($request->new_password),
            'first_time_pwd_reset_at' => Carbon::now()
        ]);

        return back()->with('success', 'Password updated successfully.');
    }
    public function change_bg_form()
    {
        return view('company.change-bg');
    }
    public function change_bg(Request $request)
    {
        $request->validate([
            'image'   => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $image = 'bg-'.time().'.'.$request->image->extension();  
        $request->image->move(public_path('uploads/company'), $image);
   
        Company::find(getCompanyId())->update([
            'bg_image'=> $image
        ]);

        return back()->with('success', 'Password updated successfully.');
    }
    public function change_bg_remove()
    {
        Company::find(getCompanyId())->update([
            'bg_image'=> null
        ]);
        return back()->with('success', 'Background removed successfully.');
    }
}
