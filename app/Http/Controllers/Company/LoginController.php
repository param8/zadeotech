<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\Mail\CompanyRegistrationMail;
use App\Mail\PasswordResetMail;
use App\Mail\PasswordResetSuccessMail;
use Mail;
use Session;
use Illuminate\Support\Facades\Password;
use DB;
use Carbon\Carbon;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:company');
    }

    public function login_form()
    {
        return view('company.auth.login-register',['tab_active'=>'login']);
    }

    public function login(Request $request)
    {
        Session::flash("tab_active","login");

        $this->validate($request, [
            'user_email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('company')->attempt(['email' => $request->user_email, 'password' => $request->password, 'status'=>1])) {

            return redirect()->route('company.dashboard');
        }

        return back()->with('error_login', 'Invalid login credentials.')->withInput($request->only('user_email'));
    }
    public function register_form()
    {
        return view('company.auth.login-register',['tab_active'=>'register']);
    }
    public function register(Request $request)
    {
        Session::flash("tab_active","register");

        $this->validate($request, [
            'company_name'   => 'required|min:2|max:191',
            'display_name'   => 'required|min:2|max:191|unique:companies',
            'first_name'   => 'required|min:2|max:100',
            'last_name'   => 'required|min:2|max:100',
            'company_address'   => 'required|min:2|max:200',
            'email'   => 'required|email|unique:companies',
            'phone'   => 'required|min:8|max:13',
            'logo'   => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $slug = \Str::slug($request->display_name);

        $logo = $slug.'-'.time().'.'.$request->logo->extension();  
        $request->logo->move(public_path('uploads/company_logo'), $logo);

        $company = Company::create([
            'company_name'   => $request->company_name,
            'display_name'   => $request->display_name,
            'slug'   => $slug,
            'first_name'   => $request->first_name,
            'last_name'   => $request->last_name,
            'company_address'   => $request->company_address,
            'email'   => $request->email,
            'phone'   => $request->phone,
            'logo'   => $logo
        ]);

        //Mail::to($request->email)->send(new CompanyRegistrationMail($company));

        //$token = \Str::random(60);
        
        //$company->sendPasswordResetNotification($token);

        return back()->with('success_register','Registered successfully.');

    }

    public function broker()
    {
        return Password::broker('companies');
    }

    protected function guard()
    {
        return Auth::guard('company');
    }

    public function password_reset_form(Request $request)
    {
        return view('company.auth.forgot-password');
    }
    public function password_reset_request(Request $request)
    {
        $request->validate(['email' => 'required|email']);


        $user = Company::whereEmail($request->email)
                ->whereStatus(1)
                ->whereNotNull('approved_at')
                ->whereNull('rejected_at')
                ->first();

        //Check if the user exists
        if (!$user) {
            return back()->withErrors(['email' => 'Email does not exists or account may have been disabled.']);
        }

        PasswordReset::whereEmail($request->email)->delete();

        //Create Password Reset Token
        $tokenData = PasswordReset::create([
            'email' => $request->email,
            'token' => \Str::random(60)
        ]);
        

        $link =  url('company/password/reset/' . $tokenData->token . '?email=' . urlencode($user->email));
        Mail::to($user->email)->send(new PasswordResetMail($user, $link));

        
        return back()->with('success', trans('We have emailed your password reset link!'));
        
    }
    
    public function enter_password_form(Request $request)
    {
        if(!isset($request->email) or !isset($request->token))
            return redirect()->route('company.login');

        return view('company.auth.reset',['email'=>$request->email, 'token'=>$request->token]);
    }
    public function enter_reset(Request $request)
    {
        //Validate input
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email|exists:companies,email',
            'password' => 'required|confirmed',
            'token' => 'required'
        ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return back()->withErrors(['email' => 'Please complete the form']);
        }

        $password = $request->password;
        // Validate the token
        $tokenData = PasswordReset::where('token', $request->token)->latest()->first();
        // Redirect the user back to the password reset request form if the token is invalid
        if (!$tokenData) return redirect()->route('company.login');

        $user = Company::whereEmail($request->email)
                ->whereStatus(1)
                ->whereNotNull('approved_at')
                ->whereNull('rejected_at')
                ->first();

        // Redirect the user back if the email is invalid
        if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);
        //Hash and update the new password
        $user->password = \Hash::make($password);
        $user->update(); //or $user->save();

        //login the user immediately they change password successfully
        Auth::guard('company')->login($user);

        //Delete the token
        $tokenData->delete();

        Mail::to($request->email)->send(new PasswordResetSuccessMail($user));

        return redirect()->route('company.dashboard')->with('success','Your password has been reset!');
    }
}
