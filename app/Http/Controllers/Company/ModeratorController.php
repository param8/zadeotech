<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Moderator;
use App\Models\Webinar;
use App\Mail\NotifyModeratorMail;
use Mail;

class ModeratorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:company');
    }

    public function index(Request $request, Webinar $webinar)
    {
        return view('company.moderator.list',['moderators'=>$webinar->moderators, 'webinar'=>$webinar]);
    }
    public function create(Request $request, Webinar $webinar)
    {
        return view('company.moderator.create',['webinar'=>$webinar]);
    }
    public function store(Request $request, Webinar $webinar)
    {
        $this->validate($request, [
            'name'   => 'required|min:3|max:200',
            'email'   => 'required|email',
            'password'   => 'required|min:6',
        ]);


        $moderator = Moderator::updateOrCreate(['email'=>$request->email],[
            'name' => $request->name,
            'email' => $request->email,
            'password' => \Hash::make($request->password)
        ]);

        if($moderator->webinars->contains($webinar->id)) {
            $moderator->webinars()->detach($webinar->id);
        }
            
        $moderator->webinars()->attach($webinar->id,[
            'access_to_quiz'=>$request->access_to_quiz==1?1:0,
            'access_to_poll'=>$request->access_to_poll==1?1:0,
            'access_to_query'=>$request->access_to_query==1?1:0
        ]);

        if($request->notify)
        {
            //Notify to user
            Mail::to($moderator->email)->send(new NotifyModeratorMail($moderator, $webinar, $request->password));
        }

        return back()->with('success','Moderator added successfully.');
    }
    public function edit(Webinar $webinar, Moderator $moderator)
    {
        return view('company.moderator.update',['webinar'=>$webinar, 'moderator'=>$moderator]);
    }
    public function update(Request $request, Webinar $webinar, Moderator $moderator)
    {
    	$rule = [
    		'name'   => 'required|min:3|max:200',
            'email'   => 'required|email',
    	];
    	if($request->has('password') and !empty($request->password))
    	{
    		$rule['password'] = 'min:6';
    	}

        $this->validate($request, $rule);

        if($request->has('email') and !empty($request->email))
        {
            $moderatordata = [
                'name' => $request->name,
                'email' => $request->email
            ];

            if($request->has('password') and !empty($request->password))
            {
                $moderatordata['password'] = \Hash::make($request->password);
            }


            $moderator = Moderator::updateOrCreate(['email'=>$request->email],$moderatordata);

            if($moderator->webinars->contains($webinar->id)) {
                $moderator->webinars()->detach($webinar->id);
            }

            if($request->notify and $request->has('password') and !empty($request->password))
            {
                //Notify to user
                Mail::to($moderator->email)->send(new NotifyModeratorMail($moderator, $webinar, $request->password));
            }
                
            $moderator->webinars()->attach($webinar->id,[
                'access_to_quiz'=>$request->access_to_quiz==1?1:0,
                'access_to_poll'=>$request->access_to_poll==1?1:0,
                'access_to_query'=>$request->access_to_query==1?1:0
            ]);
        }
        
        return redirect()->route('company.webinar.moderator',$webinar->id)->with('success','Moderator updated successfully.');
    }
    public function destroy(Webinar $webinar, Moderator $moderator)
    {
        $moderator->webinars()->detach($webinar->id);
        return redirect()->route('company.webinar.moderator',$webinar->id)->with('success','Moderator deleted successfully.');
    }
}
