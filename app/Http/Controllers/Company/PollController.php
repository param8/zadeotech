<?php

namespace App\Http\Controllers\Company;
use App\Http\Controllers\Controller;

use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Models\Poll;
use App\Models\PollOption;
use App\Models\Webinar;
use Illuminate\Http\Request;
use Slug;
use App\Events\ShowPollEvent;
use App\Events\SharePollResultEvent;

class PollController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:company')->except(['index', 'export', 'view_result', 'result', 'share_result', 'individual_result_export', 'individual_result']);
        $this->middleware('block_past_webinar')->only(['create','store','edit','update','manageOptions','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }

        return view('company.poll.list',['polls'=>$webinar->polls, 'webinar'=>$webinar]);
    }

    public function export(Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }

        $polls = $webinar->polls;

        if($polls->count()<1)
            return back()->with('error','No polls to export.');

        $fileName = 'Poll-'.$webinar->slug.'.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Poll', 'Options', 'Votes'];

        $callback = function() use($polls, $webinar, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['Poll Report']);
            fputcsv($file, ['Report Generated', Carbon::now()]);

            fputcsv($file, ['Webinar', 'Start Time', 'End Time']);

            fputcsv($file, [preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title)), $webinar->start_time, $webinar->end_time]);

            fputcsv($file, ['Poll Report']);

            fputcsv($file, $columns);

            foreach ($polls as $poll) {

                $options = null;
                $correct_option = null;
                foreach ($poll->poll_options as $key => $opt) {
                    $options .= $opt->option.'|';
                }

                $poll_options = $poll->poll_options;
                $votes = $poll->user_poll_answers;
                $total_votes = $poll->user_poll_answers->count();
                $votes_percent = null;

                foreach ($poll_options as $poll_option):
                    if($poll_option->user_poll_answers->count()){
                        $poll_option_total_votes = $poll_option->user_poll_answers->count();
                        $poll_option_ans_percent = round(($poll_option_total_votes/$total_votes)*100);

                        $votes_percent .= $poll_option->option.':'.$poll_option_ans_percent.'%'.'|';
                    }else{
                        $votes_percent .= $poll_option->option.':0%'.'|';
                    }

                endforeach;

                $row['Poll']  = $poll->poll;
                $row['Options']    = trim($options,'|');
                $row['Votes']    = trim($votes_percent,'|');

               fputcsv($file, array($row['Poll'], $row['Options'], $row['Votes']));
            }

         fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Webinar $webinar)
    {
        return view('company.poll.create',['webinar'=>$webinar]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Webinar $webinar)
    {
        //Helper::prt($request->all());
        $this->validate($request, [
            'question'   => 'required|min:3|max:200',
            'options.*' => 'required',
        ]);

        $poll = Poll::create([
            'webinar_id' => $webinar->id,
            'poll' => $request->question
        ]);

        $this->manageOptions($request, $poll);

        return redirect()->route('company.webinar.poll',$webinar->id)->with('success','Poll added successfully.');
    }

    public function manageOptions(Request $request, $poll)
    {
        foreach($request->options as $option):
            PollOption::create([
                'poll_id' => $poll->id,
                'option' => $option
            ]);
        endforeach;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Poll $poll
     * @return \Illuminate\Http\Response
     */
    public function show(Webinar $webinar, Poll $poll)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Poll $poll
     * @return \Illuminate\Http\Response
     */
    public function edit(Webinar $webinar, Poll $poll)
    {
        return view('company.poll.update',['webinar'=>$webinar, 'poll'=>$poll]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Poll $poll
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Webinar $webinar, Poll $poll)
    {
        $this->validate($request, [
            'question'   => 'required|min:3|max:200',
            'options.*' => 'required',
        ]);

        $polll = Poll::whereWebinarId($webinar->id)->whereId($poll->id)->update([
            'poll' => $request->question
        ]);

        if($poll->poll_options->count())
        {
            foreach ($poll->poll_options as $key => $opt) {
                $opt->delete();
            }
        }

        $this->manageOptions($request, $poll);
        
        return redirect()->route('company.webinar.poll',$webinar->id)->with('success','Poll updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Poll $poll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Webinar $webinar, Poll $poll)
    {
        if($poll->poll_options->count())
        {
            foreach ($poll->poll_options as $key => $opt) {
                $opt->delete();
            }
        }

        PollOption::wherePollId($poll->id)->delete();
        $poll->delete();

        return back()->with('success','Poll deleted successfully');
    }
    public function view_result(Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }
        return view('company.poll.view-result',['webinar'=>$webinar]);
    }
    public function status(Request $request, Webinar $webinar, Poll $poll)
    {
        if($poll->status==1){
           $status = 0;
           $msg = 'Poll disabled successfully';
        }else{
            $status = 1;
            $msg = 'Poll enabled successfully';
        }


        Poll::find($poll->id)->update(['status'=>$status]);
        
        //hide/show poll to frontend
        event(new ShowPollEvent($poll->id));
        //event(new ShowPollEvent($this->getEnabledPolls($webinar)));

        return response()->json(['success'=>$msg,'status'=>$status, 'item' => $poll]);
    }
    private function getEnabledPolls($webinar)
    {
        $polls = Helper::getUnAnsweredPollByWebinarId($webinar->id);
        
        return view('front.poll',[
            'polls'=>$polls,
            'webinar'=>$webinar
        ])->render();
    }
    public function individual_result(Webinar $webinar, Poll $poll)
    {
        return view('company.poll.individual-result',['poll'=>$poll, 'webinar'=>$webinar]);
    }
    public function individual_result_export(Webinar $webinar, Poll $poll)
    {
        if($poll->user_poll_answers->count()<1)
            return back()->with('error','No result to export.');
       

        $fileName = 'Poll-result-'.$webinar->slug.'.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Name', 'Email', 'Phone', 'Location', 'Answer', 'Date/Time'];

        $callback = function() use($poll, $webinar, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['Poll Report']);
            fputcsv($file, ['Report Generated', Carbon::now()]);

            fputcsv($file, ['Webinar',  'Start Time', 'End Time', 'User Answers']);

            //$res = Helper::getQuizSummery($quiz);
            //$resulttt = $res['total_correct_answer']. '/'. $res['total_answers'] . ' ('.$res['percent']. ')';

            fputcsv($file, [preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title)), $webinar->start_time, $webinar->end_time]);

            fputcsv($file, ['Poll Answer Report']);
            fputcsv($file, $columns);

            foreach($poll->user_poll_answers as $key=>$ans){
                $state = $ans->user->state ? $ans->user->state->name.' / ' : null;
                $contr = $ans->user->country ? $ans->user->country->name : null;

                $row['Name']    = $ans->user->name;
                $row['Email']    = $ans->user->email;
                $row['Phone']    = $ans->user->phone;
                $row['Location']    =  $state.$contr;
                $row['Answer']    =  $ans->poll_option->option;
                $row['Date/Time']    =  Helper::changeDateFormat($ans->created_at);
                

                fputcsv($file, array($row['Name'], $row['Email'], $row['Phone'], $row['Location'], $row['Answer'], $row['Date/Time']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
    public function share_result(Request $request, Webinar $webinar, Poll $poll)
    {
        if($poll->share_result==1){
           $status = 0;
           $msg = 'Result not showing to audience.';
        }else{
            $status = 1;
            $msg = 'Result showing to audience.';
        }


        Poll::find($poll->id)->update(['share_result'=>$status]);
        
        //hide/show poll to frontend
        event(new SharePollResultEvent($poll->id));

        return response()->json(['success'=>$msg,'status'=>$status, 'item' => $poll]);
    }
}