<?php

namespace App\Http\Controllers\Company;
use App\Http\Controllers\Controller;

use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Models\Quiz;
use App\Models\Option;
use App\Models\Webinar;
use App\Models\UserQuizAnswer;
use App\Models\User;
use Illuminate\Http\Request;
use Slug;
use DB;
use App\Events\ShwoQuizEvent;
use App\Events\ShareQuizResultEvent;

class QuizController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:company')->except(['index', 'export', 'export_result', 'result', 'share_result', 'individual_result_export', 'individual_result']);
        $this->middleware('block_past_webinar')->only(['create','store','edit','update','manageOptions','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }

        $quizzes = Quiz::whereWebinarId($webinar->id)->get();

        return view('company.quiz.list',['quizzes'=>$quizzes, 'webinar'=>$webinar]);
    }

    public function export(Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }

        $quizzes = $webinar->quizzes;

        if($quizzes->count()<1)
            return back()->with('error','No quizzes to export.');

        $fileName = 'Quiz-'.$webinar->slug.'.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Question', 'Options', 'Correct Option'];

        $callback = function() use($quizzes, $webinar, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['Quiz Report']);
            fputcsv($file, ['Report Generated', Carbon::now()]);

            fputcsv($file, ['Webinar',  'Start Time', 'End Time']);

            fputcsv($file, [preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title)), $webinar->start_time, $webinar->end_time]);

            fputcsv($file, ['Quiz Answer Report']);
            fputcsv($file, $columns);

            foreach ($quizzes as $quiz) {

                $options = null;
                $correct_option = null;
                foreach ($quiz->options as $key => $opt) {
                    $options .= $opt->option.'|';

                    if($opt->is_correct == true){
                        $correct_option = $opt->option;
                    }
                }

                $row['Question']  = $quiz->question;
                $row['Options']    = trim($options,'|');
                $row['Correct Option']    = $correct_option;


                fputcsv($file, array($row['Question'], $row['Options'], $row['Correct Option']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, Webinar $webinar)
    {
        return view('company.quiz.create',['webinar'=>$webinar]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Helper::prt($request->all());
        $this->validate($request, [
            'question'   => 'required|min:3|max:200',
            'options.*' => 'required',
            'correct_option' => 'required'
        ]);

        $webinar = Helper::getWebinar($request->webinar);

        $quiz = Quiz::create([
            'webinar_id' => $webinar->id,
            'question' => $request->question
        ]);

        $this->manageOptions($request, $quiz);

        return redirect()->route('company.webinar.quiz',$webinar->id)->with('success','Quiz added successfully.');
        return back()->with('success','Question added successfully.');
    }

    public function manageOptions(Request $request, $quiz)
    {
        foreach($request->options as $option):
            Option::create([
                'quiz_id' => $quiz->id,
                'option' => $option
            ]);
        endforeach;

        Option::whereQuizId($quiz->id)->whereOption($request->correct_option)->update([
            'is_correct' => 1
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function show(Webinar $webinar, Quiz $quiz)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function edit(Webinar $webinar, Quiz $quiz)
    {
        return view('company.quiz.update',['webinar'=>$webinar, 'quiz'=>$quiz]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Webinar $webinar, Quiz $quiz)
    {
        $this->validate($request, [
            'question'   => 'required|min:3|max:200',
            'options.*' => 'required',
            'correct_option' => 'required'
        ]);
        $quizz = Quiz::whereWebinarId($webinar->id)->whereId($quiz->id)->update([
            'question' => $request->question
        ]);

        foreach ($quiz->options as $key => $opt) {
            $opt->delete();
        }

        $this->manageOptions($request, $quiz);
        
        return redirect()->route('company.webinar.quiz',$webinar->id)->with('success','Quiz updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Quiz  $quiz
     * @return \Illuminate\Http\Response
     */
    public function destroy(Webinar $webinar, Quiz $quiz)
    {
        foreach ($quiz->options as $key => $opt) {
            $opt->delete();
        }

        Option::whereQuizId($quiz->id)->delete();
        $quiz->delete();

        return back()->with('success','Quiz deleted successfully');
    }
    public function result(Webinar $webinar)
    {
        /*$users = User::select('users.*','user_webinar.user_id','user_webinar.webinar_id'
                    //, 
                    //DB::raw("COUNT(user_quiz_answers.user_id) as user_answers_count")
                    )
                        ->leftJoin('user_webinar', 'users.id', '=', 'user_webinar.user_id')
                        ->leftJoin('webinars', 'user_webinar.webinar_id', '=', 'webinars.id')
                        ->join('user_quiz_answers', 'users.id', '=', 'user_quiz_answers.user_id')
                        //->join('quizzes', 'webinars.id', '=', 'quizzes.webinar_id')
                        ->where('user_webinar.webinar_id',$webinar->id)
                        //->where('quizzes.webinar_id',$webinar->id)
                        ->groupBy('id')
                        ->get();*/


        $users = User::select('users.*','user_quiz_answers.user_id','user_quiz_answers.quiz_id')
                    ->join('user_quiz_answers', 'users.id', '=', 'user_quiz_answers.user_id')
                    ->join('quizzes', 'user_quiz_answers.quiz_id', '=', 'quizzes.id')
                    ->where('quizzes.webinar_id',$webinar->id)
                    ->groupBy('users.id')
                    ->get();

        //prt($users->toArray());
        return view('company.quiz.result',['webinar'=>$webinar, 'users'=>$users]);
    }
    public function export_result(Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }
        
        $users = User::select('users.*','user_quiz_answers.user_id','user_quiz_answers.quiz_id')
                    ->join('user_quiz_answers', 'users.id', '=', 'user_quiz_answers.user_id')
                    ->join('quizzes', 'user_quiz_answers.quiz_id', '=', 'quizzes.id')
                    ->where('quizzes.webinar_id',$webinar->id)
                    ->groupBy('users.id')
                    ->get();

        if($users->count()<1)
            return back()->with('error','No result to export.');

       

        $fileName = 'Quiz-result-'.$webinar->slug.'.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Name', 'Email', 'Phone', 'Location', 'Currect Answer'];

        $callback = function() use($users, $webinar, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['Quiz Report']);
            fputcsv($file, ['Report Generated', Carbon::now()]);

            fputcsv($file, ['Webinar',  'Start Time', 'End Time', 'Registerd']);

            fputcsv($file, [preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title)), $webinar->start_time, $webinar->end_time, $users->count()]);

            fputcsv($file, ['Quiz Report']);
            fputcsv($file, $columns);

            foreach ($users as $usr) {
                $state = $usr->state ? $usr->state->name.' / ' : null;
                $contr = $usr->country ? $usr->country->name : null;

                $row['Name']    = $usr->name;
                $row['Email']    = $usr->email;
                $row['Phone']    = $usr->phone;
                $row['Location']    =  $state.$contr;
                $row['Currect Answer']    = '#'.Helper::userQuizResult($usr,$webinar);

                fputcsv($file, array($row['Name'], $row['Email'], $row['Phone'], $row['Location'], $row['Currect Answer']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
    public function status(Request $request, Webinar $webinar, Quiz $quiz)
    {
        if($quiz->status==1){
           $status = 0;
           $msg = 'Quiz disabled successfully';
        }else{
            $status = 1;
            $msg = 'Quiz enabled successfully';
        }

        Quiz::find($quiz->id)->update(['status'=>$status]);

        
        //hide/show quizzes to frontend
        event(new ShwoQuizEvent($quiz->id));
        //event(new ShwoQuizEvent($this->getEnabledQuizzes($webinar)));

        return response()->json(['success'=>$msg,'status'=>$status, 'item'=>$quiz]);
    }
    private function getEnabledQuizzes($webinar)
    {
        $quizzes = Helper::getUnAnsweredQuizByWebinarId($webinar->id);
        
        return view('front.quiz',[
            'quizzes'=>$quizzes,
            'webinar'=>$webinar
        ])->render();
    }
    public function individual_result(Webinar $webinar, Quiz $quiz)
    {
        return view('company.quiz.individual-result',['quiz'=>$quiz, 'webinar'=>$webinar]);
    }
    public function individual_result_export(Webinar $webinar, Quiz $quiz)
    {
        if($quiz->user_quiz_answers->count()<1)
            return back()->with('error','No result to export.');

       

        $fileName = 'Quiz-result-'.$webinar->slug.'.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Name', 'Email', 'Phone', 'Location', 'Answer', 'Correct', 'Date/Time'];

        $callback = function() use($quiz, $webinar, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['Quiz Report']);
            fputcsv($file, ['Report Generated', Carbon::now()]);

            fputcsv($file, ['Webinar',  'Start Time', 'End Time', 'User Answers']);

            $res = Helper::getQuizSummery($quiz);
       $resulttt = $res['total_correct_answer']. '/'. $res['total_answers'] . ' ('.$res['percent']. ')';

            fputcsv($file, [preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title)), $webinar->start_time, $webinar->end_time, $resulttt]);

            fputcsv($file, ['Quiz Answer Report']);
            fputcsv($file, $columns);

            foreach($quiz->user_quiz_answers as $key=>$ans){
                $state = $ans->user->state ? $ans->user->state->name.' / ' : null;
                $contr = $ans->user->country ? $ans->user->country->name : null;

                $row['Name']    = $ans->user->name;
                $row['Email']    = $ans->user->email;
                $row['Phone']    = $ans->user->phone;
                $row['Location']    =  $state.$contr;
                $row['Answer']    =  $ans->option->option;
                $row['Correct']    =  $ans->option->is_correct==1 ? 'Yes' : 'No';
                $row['Date/Time']    =  Helper::changeDateFormat($ans->created_at);
                

                fputcsv($file, array($row['Name'], $row['Email'], $row['Phone'], $row['Location'], $row['Answer'], $row['Correct'], $row['Date/Time']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
    
    public function share_result(Request $request, Webinar $webinar, Quiz $quiz)
    {
        if($quiz->share_result==1){
           $status = 0;
           $msg = 'Result not showing to audience.';
        }else{
            $status = 1;
            $msg = 'Result showing to audience.';
        }


        Quiz::find($quiz->id)->update(['share_result'=>$status]);
        
        //hide/show quiz to frontend
        event(new ShareQuizResultEvent($quiz->id));

        return response()->json(['success'=>$msg,'status'=>$status, 'item' => $quiz]);
    }
}