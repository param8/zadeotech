<?php

namespace App\Http\Controllers\Company;
use App\Http\Controllers\Controller;

use App\Models\Ticker;
use Illuminate\Http\Request;
use App\Models\Webinar;
use App\Helpers\Helper;

class TickerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:company');
        $this->middleware('block_past_webinar')->only(['updateOrCreate']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $webinar = Helper::getWebinar($request->webinar);

        $ticker = Ticker::whereWebinarId($webinar->id)->latest()->first();

        return view('company.ticker.ticker',['ticker'=>$ticker, 'webinar'=>$webinar]);
    }

    public function updateOrCreate(Request $request, Webinar $webinar)
    {
        $this->validate($request, [
            'ticker'   => 'required|min:2',
        ]);

        $newUser = Ticker::updateOrCreate([
            'webinar_id'   => $webinar->id,
        ],[
            'webinar_id'   => $webinar->id,
            'ticker'       => $request->ticker,
            'speed'       => $request->speed,
        ]);
        
        return back()->with('success','Ticker updated successfully.');
    }

    public function destroy(Ticker $ticker)
    {
        //
    }
}
