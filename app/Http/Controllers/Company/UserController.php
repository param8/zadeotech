<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Webinar;
use Carbon\Carbon;
use App\Helpers\Helper;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:company')->except(['user', 'export']);
    }

    public function user(Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }

        return view('company.user.list',['webinar'=>$webinar]);
    }
    public function export(Webinar $webinar)
    {
        if(is_copmpany()==false and is_moderator()==false){
            return redirect()->route('welcome');
        }
        
    	$users = $webinar->users;

        if($users->count()<1)
            return back()->with('error','No users to export.');

        $fileName = 'users-list-'.$webinar->slug.'.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Name', 'Email', 'Phone', 'Country', 'State', 'Optional Field', 'Created At'];

        $callback = function() use($users, $webinar, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, ['Attendee Report']);
            fputcsv($file, ['Report Generated', Carbon::now()]);
            
            fputcsv($file, ['Webinar', 'Start Time', 'End Time', 'Registerd']);

            fputcsv($file, [preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title)), $webinar->start_time, $webinar->end_time, $users->count()]);

            fputcsv($file, ['Attendee']);
            fputcsv($file, $columns);

            foreach ($users as $user) {

                $row['Name']  = $user->name;
                $row['Email']    = $user->email;
                $row['Phone']    = $user->phone;
                $row['Country']    = $user->country?$user->country->name:null;
                $row['State']    = $user->state?$user->state->name:null;
                $row['Optional Field'] = Helper::getOptionalFieldUserData($user->id,$webinar->id);
                $row['Created At']    = $user->created_at;

                fputcsv($file, array($row['Name'], $row['Email'], $row['Phone'], $row['Country'], $row['State'], $row['Optional Field'], $row['Created At']));
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
}
