<?php

namespace App\Http\Controllers\Company;
use App\Http\Controllers\Controller;

use App\Models\Webinar;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Hall;
use App\Models\Moderator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;
use App\Mail\WebinarRegisterMail;
use Mail;
 
class WebinarController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:company')->except(['index', 'user_query_export', 'user_query']);
        $this->middleware('block_past_webinar')->only(['update','destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $webinars = Webinar::whereCompanyId(getCompanyId());
        
        if($request->has('filter'))
        {
            if($request->has('webinar_title') and !empty($request->webinar_title))
            {
                $webinars->where('webinar_title','LIKE','%'.$request->webinar_title.'%');
            }

            if($request->has('webinar_filter') and !empty($request->webinar_filter))
            {
                if($request->webinar_filter=='today')
                {
                    $webinars->whereDate('start_date','=',Carbon::now());
                }
                if($request->webinar_filter=='upcomming')
                {
                    $webinars->whereDate('start_date','>=',Carbon::now());
                }
                else if($request->webinar_filter=='past')
                {
                    $webinars->whereDate('end_date','<=',Carbon::now());
                }
            }
        }

        $webinars = $webinars->orderBy('start_date','desc')->orderBy('start_time','asc')->paginate(30);
        return view('company.webinar.list',['webinars'=>$webinars]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $halls = Hall::where('company_id',getCompanyId())->get();
        return view('company.webinar.create',['halls'=>$halls]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        
        $rules = [
            'webinar_title'   => 'required|min:2|unique:webinars,id',
            'date_range'   => 'required',
            'description'   => 'required|min:2',
            'ifram_url'   => 'required|min:2',
            'date_range'   => 'required',
            'start_time'   => 'required',
            'end_time'   => 'required',
            //'phone'   => 'required|min:8|max:13',
            //'banner'   => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'brochure'   => 'required',
            'brochure.*'   => 'required|image|mimes:jpeg,png,jpg|max:2048',
        ];

        if ($request->hasFile('banner')) {
            $rules['banner'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('logo1')) {
            $rules['logo1'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }
        
        if ($request->hasFile('logo2')) {
            $rules['logo2'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }
        
        if ($request->hasFile('logo3')) {
            $rules['logo3'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('header_banner')) {
            $rules['header_banner'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('branding_logo')) {
            $rules['branding_logo'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('branding_banner')) {
            $rules['branding_banner'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if($request->has('branding_page') and $request->branding_page==1)
        {
            $rules['branding_logo'] = 'required|image|mimes:jpeg,png,jpg|max:2048';
            $rules['branding_banner'] = 'required|image|mimes:jpeg,png,jpg|max:2048';
            $rules['branding_bg'] = 'required|image|mimes:jpeg,png,jpg|max:2048';
            $rules['branding_youtube_video'] = 'required';
        }

        $this->validate($request, $rules);

        $webinar_title_clear = preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($request->webinar_title));
        $slug = \Str::slug($webinar_title_clear);
        

        //Upload banner
        $banner = null;
        if ($request->hasFile('banner')) {
            $banner = 'banner-'.time().'.'.$request->banner->extension();
            $request->banner->move(public_path('uploads/webinar'), $banner);
        }

        //Upload broucher
        //$brochure = 'brochure-'.time().'.'.$request->brochure->extension();
        //$request->brochure->move(public_path('uploads/webinar'), $brochure);

        $images = $request->file('brochure');
        $arr = [];
        foreach ($images as $item):
            $imageName = 'brochure-'.time() . '-' . $item->getClientOriginalName();
            $item->move(public_path('/uploads/webinar'), $imageName);
            $arr[] = $imageName;
        endforeach;
        $brochure = implode(",", $arr);

        //Upload logo1
        $logo1 = null;
        if ($request->hasFile('logo1')) {
            $logo1 = 'logo1-'.time().'.'.$request->logo1->extension();
            $request->logo1->move(public_path('uploads/webinar'), $logo1);
        }
        //Upload logo2
        $logo2 = null;
        if ($request->hasFile('logo2')) {
            $logo2 = 'logo2-'.time().'.'.$request->logo2->extension();
            $request->logo2->move(public_path('uploads/webinar'), $logo2);
        }
        //Upload logo3
        $logo3 = null;
        if ($request->hasFile('logo3')) {
            $logo3 = 'logo3-'.time().'.'.$request->logo3->extension();
            $request->logo3->move(public_path('uploads/webinar'), $logo3);
        }

        //Upload header_banner
        $header_banner = null;
        if ($request->hasFile('header_banner')) {
            $header_banner = 'header_banner-'.time().'.'.$request->header_banner->extension();
            $request->header_banner->move(public_path('uploads/webinar'), $header_banner);
        }

        $date_range = Helper::dateRangeParse($request->date_range);
        $youtube_hall = array();
        foreach($request->hall_id as $key=>$hallID){

          $youtube_hall[$key] = $request->youtube_link[$key];
        }
       $hall_youtube =  json_encode($youtube_hall);
        $data = [
            'company_id' => getCompanyId(),
            'webinar_title' => $request->webinar_title,
            'slug' => $slug,
            'start_date' => $date_range['from'],
            'end_date' => $date_range['to'],
            'start_time' => $request->start_time,
            'cron_ran_at' => Carbon::now()->subDay()->startOfDay(),
            'end_time' => $request->end_time,
            'description' => $request->description,
            'ifram_url' => $request->ifram_url,
            'banner' => $banner,
            'brochure' => $brochure,
            'logo1' => $logo1,
            'logo2' => $logo2,
            'logo3' => $logo3,
            'header_banner' => $header_banner,
            'facebook' => $request->facebook,
            'youtube' => $request->youtube,
            'status' => $request->status,
            'optional_field' => $request->optional_field,
            'hall_youtube' => $hall_youtube
        ];

        if($request->has('branding_page') and $request->branding_page==1)
        {
            //Upload branding_logo
            if ($request->hasFile('branding_logo')) {
                $branding_logo = 'branding_logo-'.time().'.'.$request->branding_logo->extension();
                $request->branding_logo->move(public_path('uploads/webinar'), $branding_logo);
                $data['branding_logo'] = $branding_logo;
            }

            //Upload branding_banner
            if ($request->hasFile('branding_banner')) {
                $branding_banner = 'branding_banner-'.time().'.'.$request->branding_banner->extension();
                $request->branding_banner->move(public_path('uploads/webinar'), $branding_banner);
                $data['branding_banner'] = $branding_banner;
            }

            //Upload branding_bg
            if ($request->hasFile('branding_bg')) {
                $branding_bg = 'branding_bg-'.time().'.'.$request->branding_bg->extension();
                $request->branding_bg->move(public_path('uploads/webinar'), $branding_bg);
                $data['branding_bg'] = $branding_bg;
            }

            $data['branding_youtube_video'] = $request->branding_youtube_video;

            $data['branding_page'] = 1;
        }
        else
        {
            $data['branding_page'] = 0;
        }

        $webinar = Webinar::create($data);
        
        ///Mail::to($webinar->company->email)->send(new WebinarRegisterMail($webinar));

        return redirect()->route('company.webinar')->with('success','Webinar created successfuly.');
    }
    public function delete_moderator(Moderator $moderator)
    {
        $moderator->webinars()->detach();
        return back()->with('success','Moderator deleted successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Webinar  $webinar
     * @return \Illuminate\Http\Response
     */
    public function show(Webinar $webinar)
    {
        return view('company.webinar.show',['webinar'=>$webinar]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Webinar  $webinar
     * @return \Illuminate\Http\Response
     */
    public function edit(Webinar $webinar)
    {
        return view('company.webinar.update',['webinar'=>$webinar]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Webinar  $webinar
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Webinar $webinar)
    {
        $rules = [
            'webinar_title'   => 'required|min:2|unique:webinars,id,'.$webinar->id,
            'date_range'   => 'required',
            'description'   => 'required|min:2',
            'ifram_url'   => 'required|min:2',
            'date_range'   => 'required',
            'start_time'   => 'required',
            'end_time'   => 'required',
            //'phone'   => 'required|min:8|max:13',
            'banner'   => 'image|mimes:jpeg,png,jpg|max:2048',
            'brochure.*'   => 'image|mimes:jpeg,png,jpg|max:2048',
        ];

        if($request->has('email') and !empty($request->email))
        {
            //$rules['email'] = 'required|email|unique:moderators';
            $rules['email'] = 'required|email';
            $rules['name'] = 'required|min:2|max:100';
        }

        if ($request->hasFile('logo1')) {
            $rules['logo1'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }
        
        if ($request->hasFile('logo2')) {
            $rules['logo2'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }
        
        if ($request->hasFile('logo3')) {
            $rules['logo3'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('header_banner')) {
            $rules['header_banner'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('branding_logo')) {
            $rules['branding_logo'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('branding_banner')) {
            $rules['branding_banner'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if ($request->hasFile('branding_bg')) {
            $rules['branding_bg'] = 'image|mimes:jpeg,png,jpg|max:2048';
        }

        if($request->has('branding_page') and $request->branding_page==1)
        {
            $rules['branding_youtube_video'] = 'required';
        }

        $this->validate($request, $rules);

        $webinar_title_clear = preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($request->webinar_title));
        $slug = \Str::slug($webinar_title_clear);

        $date_range = Helper::dateRangeParse($request->date_range);

        $data = [
            'company_id' => getCompanyId(),
            'webinar_title' => $request->webinar_title,
            //'slug' => $slug,
            'start_date' => $date_range['from'],
            'end_date' => $date_range['to'],
            'start_time' => $request->start_time,
            'end_time' => $request->end_time,
            'description' => $request->description,
            'ifram_url' => $request->ifram_url,
            'facebook' => $request->facebook,
            'youtube' => $request->youtube,
            'status' => $request->status == 1 ? 1 : 0,
            'optional_field' => $request->optional_field
        ];
        
        if($request->hasFile('banner'))
        {
            //Upload banner
            $banner = 'banner-'.time().'.'.$request->banner->extension();  
            $request->banner->move(public_path('uploads/webinar'), $banner);

            $data['banner'] = $banner;
        }

        if($request->hasFile('brochure'))
        {
            //die('1111');
            //Upload broucher
            /*$brochure = 'brochure-'.time().'.'.$request->brochure->extension();
            $request->brochure->move(public_path('uploads/webinar'), $brochure);

            $data['brochure'] = $brochure;*/

            $images = $request->file('brochure');
            $arr = [];
            foreach ($images as $item):
                $imageName = 'brochure-'.time() . '-' . $item->getClientOriginalName();
                $item->move(public_path('/uploads/webinar'), $imageName);
                $arr[] = $imageName;
            endforeach;
            $data['brochure'] = implode(",", $arr);
        }



        //Upload logo1
        if ($request->hasFile('logo1')) {
            $logo1 = 'logo1-'.time().'.'.$request->logo1->extension();
            $request->logo1->move(public_path('uploads/webinar'), $logo1);
            $data['logo1'] = $logo1;
        }
        //Upload logo2
        if ($request->hasFile('logo2')) {
            $logo2 = 'logo2-'.time().'.'.$request->logo2->extension();
            $request->logo2->move(public_path('uploads/webinar'), $logo2);
            $data['logo2'] = $logo2;
        }
        //Upload logo3
        if ($request->hasFile('logo3')) {
            $logo3 = 'logo3-'.time().'.'.$request->logo3->extension();
            $request->logo3->move(public_path('uploads/webinar'), $logo3);
            $data['logo3'] = $logo3;
        }

        //Upload header_banner
        if ($request->hasFile('header_banner')) {
            $header_banner = 'header_banner-'.time().'.'.$request->header_banner->extension();
            $request->header_banner->move(public_path('uploads/webinar'), $header_banner);
            $data['header_banner'] = $header_banner;
        }

        if($request->has('branding_page') and $request->branding_page==1)
        {
            //Upload branding_logo
            if ($request->hasFile('branding_logo')) {
                $branding_logo = 'branding_logo-'.time().'.'.$request->branding_logo->extension();
                $request->branding_logo->move(public_path('uploads/webinar'), $branding_logo);
                $data['branding_logo'] = $branding_logo;
            }

            //Upload branding_banner
            if ($request->hasFile('branding_banner')) {
                $branding_banner = 'branding_banner-'.time().'.'.$request->branding_banner->extension();
                $request->branding_banner->move(public_path('uploads/webinar'), $branding_banner);
                $data['branding_banner'] = $branding_banner;
            }

            //Upload branding_bg
            if ($request->hasFile('branding_bg')) {
                $branding_bg = 'branding_bg-'.time().'.'.$request->branding_bg->extension();
                $request->branding_bg->move(public_path('uploads/webinar'), $branding_bg);
                $data['branding_bg'] = $branding_bg;
            }

            $data['branding_youtube_video'] = $request->branding_youtube_video;

            $data['branding_page'] = 1;
        }
        else
        {
            $data['branding_page'] = 0;
        }


        $webinar->whereId($webinar->id)->whereCompanyId(getCompanyId())->update($data);
        
        return redirect()->route('company.webinar')->with('success','Webinar updated successfuly.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Webinar  $webinar
     * @return \Illuminate\Http\Response
     */
    public function destroy(Webinar $webinar)
    {
        $webinar->delete();
        return back()->with('success','Webinar deleted successfully');
    }
    public function user_query(Webinar $webinar)
    {
        return view('company.webinar.user-query',['webinar'=>$webinar]);
    }
    public function user_query_export(Webinar $webinar)
    {
        $user_queries = $webinar->user_queries;

        if($user_queries->count()<1)
            return back()->with('error','No query to export.');

        $fileName = 'User-query-'.$webinar->slug.'.csv';
        $headers = [
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        ];

        $columns = ['Name', 'Email', 'Phone', 'State', 'Country', 'Question', 'Created at'];

        $callback = function() use($user_queries, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

            foreach ($user_queries as $query) {


                $row['Name']  = $query->user ? $query->user->name : null;
                $row['Email']    = $query->user ? $query->user->email : null;
                $row['Phone']    = $query->user ? $query->user->phone : null;
                $row['State']    = $query->user ? ($query->user->state ? $query->user->state->name : null) : null;
                $row['Country']    = $query->user ? ($query->user->country ? $query->user->country->name : null) : null;
                $row['Question']    = $query->query;
                $row['Created at']    = $query->created_at;

               fputcsv($file, array($row['Name'], $row['Email'], $row['Phone'], $row['State'], $row['Country'], $row['Question'], $row['Created at']));
            }

         fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }
    public function remove_banner(Webinar $webinar)
    {
        $webinar->update(['banner'=>null]);
        return back()->with('success','Banner removed successfully.');
    }
    public function remove_logo1(Webinar $webinar)
    {
        $webinar->update(['logo1'=>null]);
        return back()->with('success','Logo removed successfully.');
    }
    public function remove_logo2(Webinar $webinar)
    {
        $webinar->update(['logo2'=>null]);
        return back()->with('success','Logo removed successfully.');
    }
    public function remove_logo3(Webinar $webinar)
    {
        $webinar->update(['logo3'=>null]);
        return back()->with('success','Logo removed successfully.');
    }
    public function remove_header(Webinar $webinar)
    {
        $webinar->update(['header_banner'=>null]);
        return back()->with('success','Banner removed successfully.');
    }

    public function hall()
    {
        $halls = Hall::where('company_id',getCompanyId())->get();
        
        return view('company.hall.list',['halls'=>$halls]);
    }

    public function store_hall(Request $request)
    {
    $company_id = getCompanyId();
     $hall = Hall::where('name',$request->name)->where('company_id',$company_id)->first();
       
     if($hall){
        return redirect()->route('company.webinar.hall')->with('error','Hall already exists');
     }else{
        
        $data = [
            'company_id' => $company_id,
            'name' => $request->name
        ];
        $webinar = Hall::create($data);
        
        return redirect()->route('company.webinar.hall')->with('success','Hall created successfuly.');
     }
        
    }

    public function get_hall_form(Request $request){
        $hallids = $request->products;
        foreach($hallids as $hallId){
         $hall = Hall::where('id',$hallId)->first();   
        ?>
        <div class="col-md-5 col-sm-4 mt-2">
        <input type="text" class="form-control" id="hall_id" name="hall_id[<?=$hallId?>]" value="<?=$hall->name?>" readonly>
        </div>
        <div class="col-md-5 col-sm-4 mt-2">
        <input type="text" class="form-control" id="youtube" name="youtube_link[<?=$hallId?>]" placeholder="Enter Youtube link">
        </div>
        <?php }
    }
}