<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Webinar;
use DB;
use Carbon\Carbon;
use App\Mail\WebinarReminderMail;
use Mail;

class CronController extends Controller
{
    public function webinar_reminder()
    {
        echo $todayDate = Carbon::now()->format('Y-m-d');
        echo "<br>";
        echo $todayNow = Carbon::now()->format('H:i:s');
        echo "<hr>";
        $today = Carbon::now();
        //23::55 > 22:55-1 = 21:55
        /*$today_webinars = DB::select("SELECT id,company_id,webinar_title,slug,start_date,end_date,start_time,end_time FROM webinars WHERE status=1 AND (start_date BETWEEN '$todayDate' AND '$todayDate' OR '$todayDate' BETWEEN start_date AND end_date) AND start_time > NOW() - INTERVAL 1 HOUR");*/

        //company_id,webinar_title,slug,start_date,end_date,start_time,end_time

        $today_webinars = DB::select("SELECT id FROM webinars WHERE status=1 AND start_date BETWEEN '$todayDate' AND '$todayDate' OR '$todayDate' BETWEEN start_date AND end_date");

        $ids = [];
        if(count($today_webinars))
        {
        	foreach ($today_webinars as $key => $value) {
        		$ids[] = $value->id;
        	}
        }

        if(!count($ids))
        	return ;

        $nowBefore1hrTime = Carbon::now()->subHour()->format('H:m:i');
        $nowTime = Carbon::now()->format('H:m:i');

        $webinars = Webinar::select('id','company_id','webinar_title','slug','start_date','end_date','start_time','end_time','cron_ran_at')
        	->whereDate('cron_ran_at', '<', date('Y-m-d'))
            ->whereTime('start_time', '>', $nowBefore1hrTime)
            ->whereTime('start_time', '<', $nowTime)
        	->find($ids);

        //prt($ids,false);

        //prt($webinars);

        $web = [];
        foreach ($webinars as $key => $webinar) {
        	$users = $webinar->users;
        	if($users->count())
        	{
        		foreach($users as $value){
        			//prt($value->email);
					Mail::to($value->email)->send(new WebinarReminderMail($webinar, $value));
        		}
        		$web[] = $webinar->toArray();
        	}
        	$webinar->update(['cron_ran_at'=>Carbon::now()]);
        }

        //prt($web,false);
    }
}
