<?php

namespace App\Http\Controllers\Front;
use App\Http\Controllers\Controller;

use App\Models\Webinar;
use Illuminate\Http\Request;
use App\Models\Company;
use App\Models\Country;
use App\Models\State;
use App\Models\Quiz;
use App\Models\Poll;
use Auth;
use Carbon\Carbon;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Helper;
use App\Mail\WelcomeUser;
use App\Mail\QueryMail;
use App\Models\UserQuery;
use App\Models\UserQuizAnswer;
use App\Models\UserPollAnswer;
use Mail;
use Session;
use App\Events\ShowUserQueryEvent;

class FinessseController extends Controller
{
    public function show_webinar_branding(Request $request, $company, $webinar)
    {
        $webnr = Webinar::whereSlug($webinar)->whereStatus(1)->latest()->first();
        $comp = Company::whereSlug($company)->latest()->first();
        
        if(!$comp or !$webnr)
            abort(404);

        if($webnr->branding_page!=1)
            return redirect()->route('show_webinar',['company'=>$comp->slug, 'webinar'=>$webnr->slug]);

        return view('front.branding',[
            'company'=>$comp, 
            'webinar'=>$webnr
        ]);
    }

    public function show_webinar(Request $request, $company, $webinar)
    {
        $webnr = Webinar::whereSlug($webinar)->whereStatus(1)->latest()->first();
        $comp = Company::whereSlug($company)->latest()->first();
        
        if(!$comp or !$webnr)
            abort(404);


        if(Auth::check())
        {
            $uuu = User::find(Auth::id());
            if (! $uuu->webinars->contains($webnr->id)) {
                $uuu->webinars()->attach($webnr->id);
            }
        }

        if(isset($request->email))
        {
            $user = User::whereEmail($request->email)->first();
            if($user){
                if (! $user->webinars->contains($webnr->id)) {
                    $user->webinars()->attach($webnr->id);
                }

                Auth::login($user);
            }
        }

        $countries = Country::whereStatus(1)->orderBy('name')->get();

        $polls = null;
        $quizzes = null;

        if(Auth::check())
        {
            //$quizzes = Helper::getQuiz($webnr->id);
            $quizzes = Helper::getUnAnsweredQuizByWebinarId($webnr->id);
            $polls = Helper::getUnAnsweredPollByWebinarId($webnr->id);
        }

        if($request->ajax() and $request->type == 'quiz'){
            
            $html = view('front.quiz',[
                'quizzes'=>$quizzes,
                'webinar'=>$webnr
            ])->render();

            return response()->json(['html' => $html]);
        }

        if($request->ajax() and $request->type == 'poll'){
            
            $html = view('front.poll',[
                'polls'=>$polls,
                'webinar'=>$webnr
            ])->render();

            return response()->json(['html' => $html]);
        }
        
        return view('front.home',[
            'company'=>$comp, 
            'webinar'=>$webnr, 
            'countries'=>$countries, 
            'quizzes'=>$quizzes, 
            'polls'=>$polls
        ]);
    }
    public function get_state(Request $request)
    {
    	$states = State::whereCountry_id($request->country)->whereStatus(1)->orderBy('name')->get();

        $country = Country::find($request->country);

    	return response()->json([
    		'states' => view('front.states',['states'=>$states, 'country_name' => $country->name])->render()
    	]);
    }
    public function user_register(Request $request)
    {
        $msg = [
            'state.numeric' => 'Please select state'
        ];
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'country' => 'required',
            'state' => 'required|numeric',
            'webinar' => 'required','exists:webinars,id',
            'phone' => 'required',
            //'password' => $this->passwordRules(),
        ],$msg);
        
        $webinar = Helper::getLiveWebinarById($request->webinar);
        
        if(!$webinar){
            return back()->with(['error'=>'Webinar ends!']);
        }

        $user = User::updateOrcreate(['email'=>$request->email], [
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make('123456789'),
            'country_id' => $request->country,
            'state_id' => !empty($request->state) ? $request->state : 0
        ]);

        Auth::login($user);

        $opt_field = $request->has('optional_field') ? $request->optional_field : null;

        $user->webinars()->detach($request->webinar);
        $user->webinars()->attach($request->webinar, ['optional_field' => $opt_field]);

        
        Session::put(['webinar'=>$webinar]);
        //Mail::to($user->email)->send(new WelcomeUser($user, $webinar));
        if($webinar->branding_page==1)
        {
            return redirect()->route('show_webinar_branding', [
                'company'=>$webinar->company->slug, 'webinar'=>$webinar->slug
            ]);
        }

        //prt($user);
        return back();
    }
    public function user_login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string|email|exists:users,email',
            'webinar' => 'required','exists:webinars,id',
        ]);

        $webinar = Helper::getLiveWebinarById($request->webinar);
        
        if(!$webinar){
            return back()->with(['error'=>'Webinar ends!']);
        }

        $user = User::whereEmail($request->email)->first();

        Auth::login($user);

        if (! $user->webinars->contains($request->webinar)) {
            $user->webinars()->attach($request->webinar);
        }

        Session::put(['webinar'=>$webinar]);
        if($webinar->branding_page==1)
        {
            return redirect()->route('show_webinar_branding', [
                'company'=>$webinar->company->slug, 'webinar'=>$webinar->slug
            ]);
        }

        return back();
    }
    public function user_query(Request $request, $company, $webinar)
    {
        $validator = Validator::make($request->all(), [
            'user_query' => 'required|string|max:255'
        ]);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()]);
        }

        $webinar = Webinar::whereSlug($webinar)->latest()->first();

        UserQuery::create([
            'query' => $request->user_query,
            'webinar_id' => $webinar->id,
            'user_id' => Auth::id()
        ]);

        
        //notify admin
        event(new ShowUserQueryEvent('New comments added : '. strip_tags($webinar->webinar_title) . '.'));

        return response()->json(['success'=>'Thankyou for submitting.']);
    }
    public function user_quiz_answer(Request $request, Webinar $webinar, Quiz $quiz)
    {
        $msg = [
            'quiz_option.required' => 'Please select an option.'
        ];
        $validator = Validator::make($request->all(), [
            'quiz_option' => 'required'
        ],$msg);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()]);
        }

        UserQuizAnswer::create([
           'user_id'=>Auth::id(), 
           'quiz_id'=>$quiz->id, 
           'option_id'=>$request->quiz_option
        ]);

        $quizzes = Helper::getUnAnsweredQuizByWebinarId($webinar->id);
            
        $html = view('front.quiz',[
            'quizzes'=>$quizzes,
            'webinar'=>$webinar
        ])->render();

        //notify admin
        event(new ShowUserQueryEvent('Attendee responded for quiz: '. $quiz->question));

        return response()->json([
            'html' => $html,
            'success'=>'Quiz submitted successfully.'
        ]);
    }
    public function user_poll_answer(Request $request, Webinar $webinar, Poll $poll)
    {
        $msg = [
            'poll_option.required' => 'Please select an option.'
        ];
        $validator = Validator::make($request->all(), [
            'poll_option' => 'required'
        ],$msg);

        if($validator->fails()){
            return response()->json(['error'=>$validator->errors()]);
        }

        UserPollAnswer::create([
           'user_id'=>Auth::id(), 
           'poll_id'=>$poll->id, 
           'poll_option_id'=>$request->poll_option
        ]);

        /*$polls = Helper::getUnAnsweredPollByWebinarId($webinar->id);
            
        $next_poll_view = view('front.poll',[
            'polls'=>$polls,
            'webinar'=>$webinar
        ])->render();*/

        //Calculate poll result

        /*$poll_options = $poll->poll_options;
        $votes = $poll->user_poll_answers;
        $total_votes = $poll->user_poll_answers->count();*/

        /*$attended_poll_result = view('front.poll-result',[
            'poll' => $poll,
            'poll_options' => $poll->poll_options,
            'votes' => $poll->user_poll_answers,
            'total_votes' => $poll->user_poll_answers->count(),
            'next_poll' => route('next_poll',['webinar'=>$webinar->id])
        ])->render();*/

        //notify admin
        event(new ShowUserQueryEvent('Attendee responded for poll: '. $poll->poll));
        
        return response()->json([
            //'next_poll_view' => $next_poll_view,
            //'attended_poll_result' => $attended_poll_result,
            'success'=>'Poll submitted successfully.'
        ]);
    }
    public function next_poll($webinar)
    {
        $polls = Helper::getUnAnsweredPollByWebinarId($webinar);

        //prt($polls->toArray());

        /*foreach($polls as $key=>$poll):
            foreach($poll->poll_options as $key=>$poll1):
                prt($poll1->id);
            endforeach;
        endforeach;*/

        $next_poll_view = view('front.poll',[
            'polls'=>$polls,
            'webinar'=>$webinar
        ])->render();
        return response()->json([
            'html' => $next_poll_view
        ]);
    }
    public function logout(Request $request)
    {
        Auth::logout();
        return back();
    }
    public function poll_result(Request $request, Webinar $webinar)
    {
        $html = view('front.poll-result',[
         'webinar' => $webinar
        ])->render();
        return response()->json(['html'=>$html]);
    }
    public function quiz_result(Request $request)
    {
        $html = view('front.quiz-result',[
         'webinar' => $webinar
        ])->render();
        return response()->json(['html'=>$html]);
    }
    public function contact_us(Request $request)
    {
	/*\Artisan::call('cache:clear');
    \Artisan::call('route:cache');
    \Artisan::call('route:clear');
    \Artisan::call('view:clear');
    \Artisan::call('config:cache');
    return 'cache cleared';*/
        $this->validate($request, [
            'name' => 'required|string|max:50',
            'email' => 'required|string|email|max:255',
            'phone' => 'required',
            'message' => 'required',
            'intrested_in' => 'required'
        ]);

        Mail::to('sunil@zadeotech.in')->send(new QueryMail($request));
        return back()->with('success', 'Thankyou for submitting your query, we will get back to you soon.');
        //return new QueryMail($request);
    }
}