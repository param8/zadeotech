<?php

namespace App\Http\Controllers\Moderator;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Webinar;
use App\Models\Company;
use App\Models\Moderator;
use Auth;
use Carbon\Carbon;
use App\Helpers\Helper;

class DashboardController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:moderator');
    }

    public function dashboard()
    {
        //$moderator_id = Auth::guard('moderator')->id();
        
    	return view('moderator.dashboard');
    }
    public function webinar(Request $request)
    {
    	$company_id = admin_loggedin()->company_id;
    	
        $mod_id = Auth::guard('moderator')->id();

        $modid = \DB::table('moderator_webinar')
                    ->select('webinar_id')
                    ->where('moderator_id',$mod_id)
                    ->get()
                    ->pluck(['webinar_id'])->toArray();

        

        $webinars = Webinar::whereIn('id',$modid);
        
        if($request->has('filter'))
        {
            if($request->has('webinar_title') and !empty($request->webinar_title))
            {
                $webinars->where('webinar_title','LIKE','%'.$request->webinar_title.'%');
            }

            if($request->has('webinar_filter') and !empty($request->webinar_filter))
            {
                if($request->webinar_filter=='upcomming')
                {
                    $webinars->whereDate('start_date','>=',Carbon::now());
                }
                else if($request->webinar_filter=='past')
                {
                    $webinars->whereDate('end_date','<=',Carbon::now());
                }
            }
        }
    	$webinars = $webinars->latest()->paginate(30);
        return view('company.webinar.list',['webinars'=>$webinars]);
    }
}
