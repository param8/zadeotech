<?php

namespace App\Http\Controllers\Moderator;

use App\Http\Controllers\Controller;
use App\Models\Moderator;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:moderator')->except('logout');
    }

    public function login_form()
    {
        return view('moderator.auth.login');
    }
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('moderator')->attempt(['email' => $request->email, 'password' => $request->password, 'status'=>1])) {

            return redirect()->route('moderator.webinar');
            return redirect()->route('moderator.dashboard');
        }

        return back()->with('error', 'Invalid login credentials.')->withInput($request->only('email'));
    }
}
