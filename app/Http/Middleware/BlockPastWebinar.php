<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use \Carbon\Carbon;
use App\Modles\Webinar;

class BlockPastWebinar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        /*if($request->webinar)
        {
            $webinar = Webinar::find($request->webinar);

            if($webinar->end_date->lt(Carbon::now())){
                return redirect()->route('company.webinar')->with('error','Past webinar, Action could not be performed.');
            }
        }*/
        return $next($request);
    }
}
