<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class ForceCompanyToChangePassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::guard('company')->check() and empty(Auth::guard('company')->user()->first_time_pwd_reset_at))
        {
            return redirect()->route('company.change-password')->with(['warning' =>'Please reset your password first!']);
        }
        
        return $next($request);
    }
}
