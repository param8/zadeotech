<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Models\Webinar;
use Auth;

class ValidateCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        //$company_id = getCompanyId();

        if(Auth::guard('company')->check()){
            $company_id = Auth::guard('company')->id();

           if($request->webinar and $request->webinar instanceof Webinar){
            if($request->webinar and $company_id !== $request->webinar->company_id){
                return redirect()->route('company.dashboard')->with('error','Opps! You are trying to reach an invalid route.');
            }
            }elseif($request->webinar){
               $web = Webinar::whereCompanyId($company_id)->whereId($request->webinar)->first();
               if(!$web){
                    return redirect()->route('company.dashboard')->with('error','Opps! You are trying to reach an invalid route.');
               }
            } 

        }else if(Auth::guard('moderator')->check()){
            $webinars = Auth::guard('moderator')->user()->webinars;
            
            if($request->webinar and $request->webinar instanceof Webinar){
                $webinar_id = $request->webinar->id;
            }
            elseif($request->webinar){
                $webinar_id = $request->webinar;
            }

            if (!$webinars->contains($webinar_id)) {
               return redirect()->route('moderator.dashboard')->with('error','Opps! You are trying to reach an invalid route.'); 
            }
        }

        return $next($request);
    }
}
