<?php

namespace App\Listeners;

use App\Events\SharePollResultEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SharePollResultEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SharePollResultEvent  $event
     * @return void
     */
    public function handle(SharePollResultEvent $event)
    {
        //
    }
}
