<?php

namespace App\Listeners;

use App\Events\ShareQuizResultEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ShareQuizResultEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShareQuizResultEvent  $event
     * @return void
     */
    public function handle(ShareQuizResultEvent $event)
    {
        //
    }
}
