<?php

namespace App\Listeners;

use App\Events\ShowPollEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ShowPollEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShowPollEvent  $event
     * @return void
     */
    public function handle(ShowPollEvent $event)
    {
        //
    }
}
