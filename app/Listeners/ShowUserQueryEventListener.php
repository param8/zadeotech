<?php

namespace App\Listeners;

use App\Events\ShowUserQueryEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ShowUserQueryEventListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShowUserQueryEvent  $event
     * @return void
     */
    public function handle(ShowUserQueryEvent $event)
    {
        //
    }
}
