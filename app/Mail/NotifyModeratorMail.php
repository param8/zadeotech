<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Moderator;
use App\Models\Webinar;

class NotifyModeratorMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $moderator;
    public $webinar;
    public $password;

    public function __construct(Moderator $moderator, Webinar $webinar, $password)
    {
        $this->moderator = $moderator;
        $this->webinar = $webinar;
        $this->password = $password;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.notify-moderator')->subject('You are added as moderator');
    }
}
