<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Company;

class PasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;

    public $company;
    public $link;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Company $company, $link)
    {
        $this->company = $company;
        $this->link = $link;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Reset Password Notification')->markdown('emails.password_reset');
    }
}
