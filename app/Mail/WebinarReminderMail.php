<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Webinar;

class WebinarReminderMail extends Mailable
{
    use Queueable, SerializesModels;

    public $webinar;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Webinar $webinar)
    {
        $this->webinar = $webinar;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Webinar reminder')->markdown('emails.webinar_reminder');
    }
}
