<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Authenticatable
{
    use HasFactory;
    use Notifiable;
    use SoftDeletes;

    protected $guard = 'company';

    protected $fillable = ['company_name','display_name','slug','first_name','last_name','company_address','email','phone','password','logo','approved_at','rejected_at','status', 'deleted_at', 'first_time_pwd_reset_at', 'bg_image', 'gstin', 'pan'];

    protected $hidden = [
            'password',
        ];

    protected $dates = ['approved_at', 'rejected_at', 'deleted_at', 'first_time_pwd_reset_at'];
    public function admin()
    {
    	return $this->belongsTo('App\Models\Admin');
    }

    public function moderators()
    {
    	return $this->hasMany('App\Models\Moderator');
    }
    
    public function webinars()
    {
        return $this->hasMany('App\Models\Webinar');
    }
    public function getLogoAttribute($value)
    {
        return asset('uploads/company_logo/'.$value);
    }
    public function getBgImageAttribute($value)
    {
        if(!empty($value))
        return asset('uploads/company/'.$value);
    }
}