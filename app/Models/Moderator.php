<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Moderator extends Authenticatable
{
    use HasFactory;
    use Notifiable;

    protected $guard = 'moderator';

    protected $fillable = [
        'name', 'email', 'password'
    ];

    protected $hidden = [
            'password',
        ];

    public function company()
    {
    	return $this->belongsTo('App\Models\Company');
    }
    public function getFirstNameAttribute()
    {
        return "{$this->name}";
    }
    public function webinars()
    {
        return $this->belongsToMany('App\Models\Webinar');
    }
    public function moderator_webinar()
    {
        return $this->hasOne('App\Models\ModeratorWebinar');
    }
}
