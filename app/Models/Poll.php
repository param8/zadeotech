<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Poll extends Model
{
    use HasFactory;

    protected $fillable = ['webinar_id', 'poll', 'status', 'share_result'];

    public function webinar()
    {
    	return $this->belongsTo('App\Models\Company');
    }
    public function poll_options()
    {
    	return $this->hasMany('App\Models\PollOption');
    }
    public function user_poll_answers()
    {
        return $this->hasMany('App\Models\UserPollAnswer');
    }
}
