<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PollOption extends Model
{
    use HasFactory;

    protected $fillable = ['poll_id', 'option'];

    public function poll()
    {
    	return $this->belongsTo('App\Models\Poll');
    }
    
    public function user_poll_answers()
    {
    	return $this->hasMany('App\Models\UserPollAnswer');
    }
}
