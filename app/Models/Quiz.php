<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;

    protected $fillable = ['webinar_id', 'question', 'status', 'share_result'];

    public function webinar()
    {
    	return $this->belongsTo('App\Models\Company');
    }
    public function options()
    {
        return $this->hasMany('App\Models\Option');
    }
    public function correct_options()
    {
        return $this->hasOne('App\Models\Option', 'quiz_id')->whereIs_correct(1);
    }
    public function user_quiz_answers()
    {
    	return $this->hasMany('App\Models\UserQuizAnswer');
    }
}
