<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticker extends Model
{
    use HasFactory;
    protected $fillable = ['webinar_id', 'ticker', 'speed'];

    public function getSpeedAttribute($value)
    {
        if($value=='slow')
        	return 2;
        elseif($value=='medium')
        	return 5;
        elseif($value=='fast')
        	return 10;
    }
}