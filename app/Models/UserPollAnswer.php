<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserPollAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id', 'poll_id', 'poll_option_id'
    ];

    public function poll()
    {
    	return $this->belongsTo('App\Models\Quiz');
    }
    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
    public function poll_option()
    {
        return $this->belongsTo('App\Models\PollOption');
    }
}
