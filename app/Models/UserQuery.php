<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQuery extends Model
{
    use HasFactory;
    protected $fillable = ['query', 'user_id', 'webinar_id'];
    
    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
    public function webinars()
    {
    	return $this->hasMany('App\Models\Webinar');
    }
}
