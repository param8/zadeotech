<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserQuizAnswer extends Model
{
    use HasFactory;

    protected $fillable = [
    	'user_id', 'quiz_id', 'option_id'
    ];

    public function quiz()
    {
    	return $this->belongsTo('App\Models\Quiz');
    }
    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
    public function option()
    {
        return $this->belongsTo('App\Models\Option');
    }
    public function correct_option()
    {
        return $this->belongsTo('App\Models\Option', 'option_id')->where('is_correct',1);
    }
    
}
