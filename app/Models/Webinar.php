<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Webinar extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
    	'company_id',
        'webinar_title',
        'slug',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
        'cron_ran_at',
        'description',
        'ifram_url',
        'banner',
        'brochure',
        'logo1',
        'logo2',
        'logo3',
        'header_banner',
        'facebook',
        'youtube',
        'status',
        'optional_field',
        'branding_page',
        'branding_logo',
        'branding_youtube_video',
        'branding_banner',
        'branding_bg',
        'hall_youtube'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date', 
        'end_date'
    ];


    public function company()
    {
    	return $this->belongsTo('App\Models\Company');
    }
    
    public function ticker()
    {
        return $this->hasOne('App\Models\Ticker');
    }
    public function quizzes()
    {
        return $this->hasMany('App\Models\Quiz');
    }
    public function polls()
    {
        return $this->hasMany('App\Models\Poll');
    }
    public function getBannerAttribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }
    /*public function getBrochureAttribute($value)
    {
        return asset('uploads/webinar/'.$value);
    }*/
    public function getLogo1Attribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }
    public function getLogo2Attribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }
    public function getLogo3Attribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }

    public function getHeaderBannerAttribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }

    public function getBrandingLogoAttribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }
    
    public function getBrandingBgAttribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }

    public function getBrandingBannerAttribute($value)
    {
        if(!empty($value))
        return asset('uploads/webinar/'.$value);
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }
    public function moderators()
    {
        return $this->belongsToMany('App\Models\Moderator');
    }
    public function user_queries()
    {
        return $this->hasMany('App\Models\UserQuery')->latest();
    }
    /*public function users()
    {
        return $this->belongsToMany('App\Models\UserWebinar');
    }*/
}