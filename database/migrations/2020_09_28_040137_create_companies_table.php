<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->id();
            $table->string('company_name');
            $table->string('display_name');
            $table->string('slug'); // on behalf of display_name
            $table->string('first_name');
            $table->string('last_name');
            $table->string('company_address');
            $table->string('email');
            $table->string('phone');
            $table->string('password')->nullable();
            $table->timestamp('first_time_pwd_reset_at')->nullable();
            $table->string('bg_image')->nullable();
            $table->string('logo');
            $table->string('gstin')->nullable();
            $table->string('pan')->nullable();
            $table->boolean('approved_at')->timestamp()->nullable();
            $table->boolean('rejected_at')->timestamp()->nullable();
            $table->boolean('status')->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
