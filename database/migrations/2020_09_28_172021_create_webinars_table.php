<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars', function (Blueprint $table) {
            $table->id();
            $table->integer('company_id');
            $table->longText('webinar_title');
            $table->string('slug'); //webinar_title

            $table->timestamp('start_date')->nullable();
            $table->timestamp('end_date')->nullable();
            $table->time('start_time');
            $table->time('end_time');

            $table->timestamp('cron_ran_at')->nullable();
            $table->longText('description');
            $table->longText('ifram_url');
            $table->string('banner')->nullable();
            $table->string('brochure');
            
            $table->string('logo1')->nullable();
            $table->string('logo2')->nullable();
            $table->string('logo3')->nullable();
            $table->string('header_banner')->nullable();
            

            $table->string('facebook')->nullable();
            $table->string('youtube')->nullable();

            $table->boolean('status')->default(true);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webinars');
    }
}
