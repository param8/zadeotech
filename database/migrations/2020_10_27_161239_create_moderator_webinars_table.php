<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateModeratorWebinarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moderator_webinars', function (Blueprint $table) {
            $table->id();
            $table->integer('webinar_id');
            $table->integer('moderator_id');
            $table->boolean('access_to_quiz')->default(false);
            $table->boolean('access_to_poll')->default(false);
            $table->boolean('access_to_query')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moderator_webinars');
    }
}
