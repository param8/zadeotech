$(document.body).on("click",".add_more", function(e){
  var optional_ans_wrapp = $(".optional_ans_wrapp");
  $(this).removeClass('add_more');
  $(this).addClass('remove_this');
  optional_ans_wrapp.append('<div class="input-group option_wrapper mb-3"><div class="input-group-prepend"><span class="input-group-text"><input required type="radio" name="correct_ans" varlue="1"></span></div><input type="text" required="" name="options[]" class="form-control float-right options"><div class="input-group-append"><span class="input-group-text add_more new"><i class="far fa fa-plus"></i></span></div></div>');
    
    $(".remove_this").html("<i class='far fa fa-times'></i>");
});
$(document.body).on("click",".remove_this", function(e){
  $(this).parents('.option_wrapper').remove();
});

/*$(document.body).on("click","input[name='correct_ans']", function(e){
  var correct_answer_opt = $(this).parents('.option_wrapper').find('.options').val();
  $(".correct_answer").val(correct_answer_opt);
});*/

$(document.body).on("click",".btn-add-que", function(e){
  correct_ans = $("input[name='correct_ans']:checked").parents('.option_wrapper').find('.options').val();
  $(".correct_option").val(correct_ans);
});

$(document.body).on("click",".add_more_poll_option", function(e){
  var optional_ans_wrapp = $(".optional_ans_wrapp");
  $(this).removeClass('add_more_poll_option');
  $(this).addClass('remove_this_poll');
  optional_ans_wrapp.append('<div class="input-group option_wrapper mb-3"><input type="text" required="" name="options[]" class="form-control float-right options"><div class="input-group-append"><span class="input-group-text add_more_poll_option new"><i class="far fa fa-plus"></i></span></div></div>');
    
    $(".remove_this_poll").html("<i class='far fa fa-times'></i>");
});
$(document.body).on("click",".remove_this_poll", function(e){
  $(this).parents('.option_wrapper').remove();
});

//Start timer
$(document.body).on("click",".btn-timer-start", function(e){
  var id = $(this).data('id');
  $(this).addClass('d-none');
  $("#pause-"+id).removeClass('d-none');
  var hrf = $(this).attr('href');
  e.preventDefault();
  interval = setInterval(function() {
    $.ajax({
      type: "post",
      url:  hrf,
      data: {start:1},
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      
      success: function(data)
      {
        $("#timer-"+data.id).html(data.timer);
      }
    });
  }, 1000);
});

$(document.body).on("click",".btn-status", function(e){
  var id = $(this).attr('id');
  thisobj = $(this);
  var hrf = $(this).attr('href');
  $(".text-success").remove();
  $(".loader").remove();
  e.preventDefault();
  $.ajax({
      type: "post",
      url:  hrf,
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        $("body").append("<div class='loader'><p>Loading...</p></div>");
      },
      success: function(data)
      {
        $(".loader").remove();
        thisobj.parents('td').append("<p class='text-success'>"+data.success+"</p>");
        if(data.status==1){
          (thisobj).children('i').addClass('fa-eye-slash');
          (thisobj).children('i').removeClass('fa-eye');
          (thisobj).html('Hide');
          (thisobj).attr('title','Hide');
          (thisobj).removeClass('btn-success');
          (thisobj).addClass('btn-danger');
          //start timer
          var i = 1;
          //console.log(data.quiz.id);
          interval = setInterval(function() {
            $("#timer-"+data.item.id).html(i++);
          },1000);
        }
        else{
          (thisobj).children('i').removeClass('fa-eye-slash');
          (thisobj).children('i').addClass('fa-eye');
          (thisobj).html('Show');
          (thisobj).addClass('btn-success');
          (thisobj).removeClass('btn-danger');
          (thisobj).attr('title','Show');
          clearInterval(interval);
          //$("#timer-"+data.quiz.id).html($("#timer-"+data.quiz.id).html());
          //stop timer
        }
      }
    });
});

$(document.body).on("click",".btn-share", function(e){
  var id = $(this).attr('id');
  thisobj = $(this);
  var hrf = $(this).attr('href');
  $(".text-success").remove();
  $(".loader").remove();
  e.preventDefault();
  $.ajax({
    type: "post",
    url:  hrf,
    headers: {
      'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
    },
    dataType:'JSON',
    beforeSend: function() {
      $("body").append("<div class='loader'><p>Loading...</p></div>");
    },
    success: function(data)
    {
      $(".loader").remove();
      thisobj.parents('td').append("<p class='text-success'>"+data.success+"</p>");
      if(data.status==1){
        (thisobj).html('<i class="fa fa-share"></i> Hide');
        (thisobj).attr('title','Hide result');
        (thisobj).removeClass('btn-success');
        (thisobj).addClass('btn-danger');
      }
      else{
        (thisobj).html('<i class="fa fa-share"></i> Share result');
        (thisobj).addClass('btn-success');
        (thisobj).removeClass('btn-danger');
        (thisobj).attr('title','Share result with audience');
      }
    }
  });
});

$(document.body).on("click",".btn-status1", function(e){
  var url = $(this).data('url');
  var id = $(this).data('id');
  var timerstart = $(this).data('timerstart'); //false/true
  alert(timerstart);
  if(timerstart==false)
  {
    $(this).attr('data-timerstart',true);
  }
  if(timerstart==true)
  {
    $(this).attr('data-timerstart',false);
  }

  $("#eye-"+id).toggleClass('fa-eye-slash');
  $("#eye-"+id).toggleClass('fa-eye');
  //$("#eye-"+id).addClass('fa-eye-slash');
  //alert(url);
});
function printDiv(divName) {
 var printContents = document.getElementById(divName).innerHTML;
 var originalContents = document.body.innerHTML;
 document.body.innerHTML = printContents;
 window.print();
 document.body.innerHTML = originalContents;
}