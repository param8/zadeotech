@extends('layouts.login_app')
@section('title','Admin Account Recovery')
@section('content')
<div class="border bg-light shadow-box">
  <div class="text-center bg-dark py-1 px-1">
  <h4 class="text-white"> Account Recovery </h4>
</div>
<div class="px-5 py-3">
    <p>Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.</p>
  <form action="{{ route('admin.forgot-password') }}" method="post">
    @csrf
    @component('components.alert')
    @endcomponent
      <div class="form-group has-feedback">
    <input id="email" type="email" class="form-control input {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" required autofocus placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
    
    <div class="form-group" align="center">
      <button type="submit" class="primary"> Submit </button>
    </div>
    <div class="text-center">
        <a href="{{ route('admin.login') }}">Login?</a>
    </div>
  </form>
</div>
<!--./px-5 py-3-->
@endsection