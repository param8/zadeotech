@extends('layouts.login_app')
@section('title','Admin Login')
@section('content')
<div class="border bg-light shadow-box">
  <div class="text-center bg-dark py-1 px-1">
  <h4 class="text-white"> Login Here </h4>
</div>
<div class="px-5 py-3">
  <form action="{{ route('admin.login') }}" method="post">
    @csrf
    @component('components.alert')
    @endcomponent
      <div class="form-group has-feedback">
    <input id="email" type="email" class="form-control input {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" required autofocus placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
    <div class="form-group has-feedback">
       <input id="password" type="password" class="form-control input {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
    </div>
    <div class="form-group" align="center">
      <button type="submit" class="primary"> Login </button>
    </div>
    <div class="text-center">
        <a href="{{ route('admin.forgot-password') }}">Forgotten password?</a>
    </div>
  </form>
</div>
<!--./px-5 py-3-->
@endsection