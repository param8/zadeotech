@extends('layouts.dashboard')
@php($user = Auth::guard('admin')->user())
@section('active-change-password','active')
@section('title','Change password')
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">Dashboard</h1>
</div><!-- /.col -->
</div><!-- /.row -->
<!-- /.row -->
<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Change Password</h5>
  </div>
  <div class="card-body">
    <form action="{{ route('admin.change-password') }}" method="post">
      @csrf
      @component('components.alert')
      @endcomponent
      <div class="row">
      <div class="col-sm-6">
      <div class="form-group has-feedback">
        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email',$user->email)}}" placeholder="Current Password" disabled="">
        </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" name="current_password" autofocus required placeholder="Current Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('current_password')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" name="new_password" required placeholder="New Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('new_password')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control input {{ $errors->has('new_confirm_password') ? ' is-invalid' : '' }}" name="new_confirm_password" required placeholder="Confirm Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('new_confirm_password')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Update Password </button>
      </div>
    </form>
  </div>
</div>
</div>
</div>
@endsection