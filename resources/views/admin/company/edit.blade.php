@extends('layouts.dashboard')
@php($user = Auth::guard('admin')->user())
@section('active-company-list','active')
@section('title','Edit Company')
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">Dashboard</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Company</h5>
  </div>
  <div class="card-body">
    
  	<form class="form-horizontal" action="{{ route('admin.company.edit',$company->id) }}" method="post"  enctype="multipart/form-data">
      @csrf
      @component('components.alert')
      @endcomponent
      
      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Company Name</label>
        <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{old('company_name',$company->company_name)}}" placeholder="Company Name">
      </div>
        </div>

        <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Nick</label>
        <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{old('slug',$company->slug)}}" placeholder="Nick">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('slug')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror 
      </div>
        </div>
      
      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Company Display Name</label>
        <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('display_name') ? ' is-invalid' : '' }}" name="display_name" required placeholder="Company Display Name" value="{{old('display_name',$company->display_name)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('display_name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">First Name</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" required placeholder="First Name" value="{{old('first_name',$company->first_name)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('first_name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>


 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Last Name</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" required placeholder="Last Name" value="{{old('last_name',$company->last_name)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('last_name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>


 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Company Address</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('company_address') ? ' is-invalid' : '' }}" name="company_address" required placeholder="Company Address" value="{{old('company_address',$company->company_address)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('company_address')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Email</label>
  <div class="col-sm-9">
        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required placeholder="Email" value="{{old('email',$company->email)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('email')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Phone</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required placeholder="Phone" value="{{old('phone',$company->phone)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('phone')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

  <div class="form-group row has-feedback">
    <label class="col-sm-3 col-form-label">Logo</label>
    <div class="col-sm-9">
    <input accept="image/*" id="logo" type="file" class="file {{ $errors->has('logo') ? ' is-invalid' : '' }}" name="logo" placeholder="Logo">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <div>
        <small class="text-muted"><i class="fa fa-info-circle"></i> File Size between 100Kb-2Mb Dimension 100:200 only JPG or PNG </small>
        </div>
  @error('logo')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
      @endif
      <img src="{{$company->logo}}" width="100px;">
  </div>
  </div>

  <div class="form-group row has-feedback">
    <label class="col-sm-3 col-form-label">Background Image</label>
    <div class="col-sm-9">
    <input accept="image/*" id="image" type="file" class="file {{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" placeholder="Logo">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <div>
        </div>
  @error('image')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
      @endif
      <img src="{{$company->bg_image}}" width="100px;">
  </div>
  </div>

<div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">GSTIN</label>
        <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('gstin') ? ' is-invalid' : '' }}" name="gstin" value="{{old('gstin',$company->gstin)}}" placeholder="GSTIN">
      </div>
        </div>

        <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">PAN</label>
        <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('pan') ? ' is-invalid' : '' }}" name="pan" value="{{old('pan',$company->pan)}}" placeholder="PAN">
      </div>
        </div>
<div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Password</label>
        <div class="col-sm-9">
        <input type="text" class="form-control" name="password" placeholder="Password">
      </div>
        </div>
      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Update </button>
      </div>
    </form>
  </div>
</div>
@endsection