@extends('layouts.dashboard')
@php($user = Auth::guard('admin')->user())
@section('active-company-list','active')
@section('title','Company')
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">Dashboard</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Company</h5>
    <div class="card-tools">
      <div class="btn-group">
        <a href="{{route('admin.company.export')}}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Export</a>
      </div>
    </div>
  </div>
  <div class="card-body">
    <form action="">
    <table class="table table-bordered">
      <thead>
        <tr>
          <th>Name, Email or Phone</th>
          <th>Approve Status</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td><input type="text" class="form-control" name="key" value="{{ isset($_GET['key']) ? $_GET['key'] : null }}"></td>
          <td>
            <select class="form-control" name="approve_status">
              <option value="all">All</option>
              <option value="approved" @if(isset($_GET['approve_status']) and $_GET['approve_status']=='approved') selected @endif>Approved</option>
              <option value="pending" @if(isset($_GET['approve_status']) and $_GET['approve_status']=='pending') selected @endif>Pending</option>
              <option value="rejected" @if(isset($_GET['approve_status']) and $_GET['approve_status']=='rejected') selected @endif>Rejected</option>
            </select>
          </td>
          <td>
            <select class="form-control" name="status">
              <option value="all">All</option>
              <option value="activated" @if(isset($_GET['status']) and $_GET['status']=='activated') selected @endif>Approved</option>
              <option value="deactivated" @if(isset($_GET['status']) and $_GET['status']=='deactivated') selected @endif>Deactivated</option>
            </select>
          </td>
          <td><button name="filter" type="submit" class="btn btn-primary btn-sm"><i class="fa fa-filter"></i> Filter</button></td>
        </tr>
      </tbody>
    </table>
    </form>
    @component('components.alert', ['all_error_hide'=>false])
    @endcomponent
  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Display Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Approve Status</th>
            <th>Status</th>
            <th>Created At</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
        @forelse($companies as $key=>$company)
          <tr>
            <td>{{$key+1}}</td>
            <td>
              <div>{{$company->company_name}}</div>
              <div><small class="text-muted">Nick: {{ $company->slug }}</small></div>
            </td>
            <td>{{$company->email}}</td>
            <td>{{$company->phone}}</td>
            <td>
              


              @if(!empty($company->approved_at))
              <div class="text-success">Approved</div>
              <div>{{$company->approved_at}}</div>
              <div><small class="text-muted">{{$company->approved_at->diffForHumans()}}</small></div>
              @elseif(!empty($company->rejected_at))
              <div class="text-danger">Rejected</div>
              <div>{{$company->rejected_at}}</div>
              <div><small class="text-muted">{{$company->rejected_at->diffForHumans()}}</small></div>
              @if(Auth::guard('admin') and Auth::guard('admin')->id()==1)
              <form onsubmit="return confirm('Are you sure?')" action="{{ route('admin.company.approve',$company->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#approveModal1">Approve</a>
                <!-- Modal -->
                <div class="modal fade" id="approveModal1" tabindex="-1" role="dialog" aria-labelledby="approveModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="approveModalLabel">Approve Company</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <label>Background Image</label>
                        <input type="file" name="image" accept="image/*" class="form-control">
                        <br>
                        <label>Nick</label>
                        <input type="text" name="slug" required class="form-control" value="{{ $company->slug }}">
                        <br>
                        <label>Password</label>
                        <input type="text" name="password" min="6" required class="form-control">
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="approve" class="btn btn-primary">Approve</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              @endif
              @else
              @if(Auth::guard('admin') and Auth::guard('admin')->id()==1)
              <form onsubmit="return confirm('Are you sure?')" action="{{ route('admin.company.approve',$company->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#approveModal">Approve</a>
                <button type="submit" name="reject" class="btn btn-danger btn-xs">Reject</button>
                <!-- Modal -->
                <div class="modal fade" id="approveModal" tabindex="-1" role="dialog" aria-labelledby="approveModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="approveModalLabel">Approve Company</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <label>Background Image</label>
                        <input type="file" name="image" accept="image/*" class="form-control">
                        <br>
                        <label>Nick</label>
                        <input type="text" name="slug" required class="form-control" value="{{ $company->slug }}">
                        <br>
                        <label>Password</label>
                        <input type="text" name="password" min="6" required class="form-control">
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" name="approve" class="btn btn-primary">Approve</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
              @endif
              @endif

            </td>
            <td>
              @if(!empty($company->approved_at))
              @if($company->status==1)
              <div class="text-success">Activated</div>
              @if(Auth::guard('admin') and Auth::guard('admin')->id()==1)
              <form onsubmit="return confirm('Are you sure wants to deactivate this company?')" action="{{ route('admin.company.deactivate',$company->id) }}" method="post">
                @csrf
                <button type="submit" class="btn btn-danger btn-xs">Deactivate</button>
              </form>
              @endif
              @else
              <div class="text-danger">Deactivated</div>
              @if(Auth::guard('admin') and Auth::guard('admin')->id()==1)
              <form onsubmit="return confirm('Are you sure wants to reactivate this company?')" action="{{ route('admin.company.reactivate',$company->id) }}" method="post">
                @csrf
                <button type="submit" class="btn btn-success btn-xs">Reactivate</button>
              </form>
              @endif
              @endif
              @endif
            </td>
            <td>
              <div>{{$company->created_at}}</div>
              <div><small class="text-muted">{{$company->created_at->diffForHumans()}}</small></div>
            </td>
            <td>
              <a href="{{ route('admin.company.edit',$company->id) }}" class="btn btn-primary btn-xs">Edit</a>
              @if(Auth::guard('admin') and Auth::guard('admin')->id()==1)
              <form onsubmit="return confirm('Are you sure wants to delete permanently this company?')" action="{{ route('admin.company.delete',$company->id) }}" method="post">
                @csrf
                <button type="submit" class="btn btn-danger btn-xs">Delete</button>
              </form>
              @endif
              </td>
          </tr>
        @empty
        @endforelse
        </tbody>

      </table>
      
    </div>
    {{$companies->links('vendor.pagination.bootstrap-4')}}
  </div>
</div>
@endsection