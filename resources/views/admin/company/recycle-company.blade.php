@extends('layouts.dashboard')
@php($user = Auth::guard('admin')->user())
@section('active-trashed','active')
@section('title','Recycle Company')
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">Dashboard</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Company ({{$companies->count()}})</h5>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    <div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Display Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Created At</th>
            <th>Deleted At</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
        @forelse($companies as $key=>$company)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$company->company_name}}</td>
            <td>{{$company->email}}</td>
            <td>{{$company->phone}}</td>
            <td>
              <div>{{$company->created_at}}</div>
              <div><small class="text-muted">{{$company->created_at->diffForHumans()}}</small></div>
            </td>
            <td>
              {{ $company->deleted_at }}
            </td>
            <td>
              <form onsubmit="return confirm('Are you sure wants to restore this company?')" action="{{ route('admin.company.trashed.restore',$company->id) }}" method="post">
                @csrf
                <input type="hidden" name="company_id" value="{{ $company->id }}">
                <button type="submit" class="btn btn-success btn-xs">Restore</button>
              </form>
              </td>
          </tr>
        @empty
        <tr>
          <td colspan="7"><h1 class="text-center text-muted"><br><br><br>Empty<br><br><br></h1></td>
        </tr>
        @endforelse
        </tbody>

      </table>
      
    </div>
    {{$companies->links('vendor.pagination.bootstrap-4')}}
  </div>
</div>
@endsection