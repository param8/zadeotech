@extends('layouts.dashboard')
@php($user = Auth::guard('admin')->user())
@section('active-dashboard','active')
@section('title','Dashboard')
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Dashboard</h5>
  </div>
  <div class="card-body">
    <br>
    @component('components.alert')
    @endcomponent
    <div class="row">
          
            <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{ $aproved }}</h3>

                <p>Total Approved</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{route('admin.company')}}?key=&approve_status=approved&status=all&filter=" class="small-box-footer">Go to Company <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{ $pending }}</h3>

                <p>Total Pending Approval</p>
              </div>
              <div class="icon">
                <i class="fas fa fa-server"></i>
              </div>
              <a href="{{route('admin.company')}}?key=&approve_status=pending&status=all&filter=" class="small-box-footer">Go to Company <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{ $deactivated }}</h3>

                <p>Total Deactivated</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{route('admin.company')}}?key=&approve_status=all&status=deactivated&filter=" class="small-box-footer">Go to Company <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          
        </div>
  </div>
</div>
@endsection