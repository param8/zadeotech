<form action="{{ route('admin.manager.create') }}" method="post" enctype="multipart/form-data">
  @csrf
  <div class="row">
    <div class="col-md-6">
      <div class="form-group has-feedback">
        <label>Name</label>
        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
        @error('name')
          <span class="invalid-feedback"> 
            <strong>{{ $message }}</strong> 
          </span>
        @enderror
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group has-feedback">
        <label>Email</label>
        <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
        @error('email')
          <span class="invalid-feedback"> 
            <strong>{{ $message }}</strong> 
          </span>
        @enderror
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-6">
      <div class="form-group has-feedback">
        <label>Password</label>
        <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{old('password')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
        @error('password')
          <span class="invalid-feedback"> 
            <strong>{{ $message }}</strong> 
          </span>
        @enderror
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12">
      <input type="submit" name="submit" class="btn btn-primary">
    </div>
  </div>
</form>