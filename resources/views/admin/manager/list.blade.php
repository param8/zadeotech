@extends('layouts.dashboard')
@php($user = Auth::guard('admin')->user())
@section('active-manager','active')
@section('title','Manager')
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">Dashboard</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Manager ({{$managers->count()}})</h5>
    <div class="card-tools">
      <a href="{{route('admin.manager.create')}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add</a>
    </div>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    @include('admin.manager.create')
    <br>
  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Created At</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
        @forelse($managers as $key=>$manager)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$manager->name}}</td>
            <td>{{$manager->email}}</td>
            <td>
              <div>{{\App\Helpers\Helper::changeDateFormat($manager->created_at)}}</div>
              <div><small class="text-muted">{{$manager->created_at->diffForHumans()}}</small></div>
            </td>
            <td>
                <a href="{{ route('admin.manager.edit',$manager->id) }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil-alt"></i> Edit</a>
                <form action="{{ route('admin.manager.destroy',$manager->id) }}" method="post" accept-charset="utf-8">
                  @csrf
                  <button class="btn btn-danger btn-xs" type="submit"><i class="fa fa-trash-alt"></i> Delete</button>
                </form>
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="5">
              <center>
              <br>
              <br>
              <h1 class="text-muted">Empty</h1>
              <br>
              <a href="{{ route('admin.manager.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create</a>
              <br>
              <br>
              </center>
            </td>
          </tr>
        @endforelse
        </tbody>

      </table>
      
    </div>
    
  </div>
</div>
@endsection