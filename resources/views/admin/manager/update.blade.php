@extends('layouts.dashboard')
@php($user = Auth::guard('admin')->user())
@section('active-manager','active')
@section('title','Update Manager')
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">Dashboard</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Update Manager</h5>
    <div class="card-tools">
      <div class="btn-group">
        <a href="{{ route('admin.manager') }}" class="btn btn-primary btn-xs"><i class="fa fa-reply"></i> Back</a>
      </div>
    </div>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent

    <form action="{{ route('admin.manager.edit',$admin->id) }}" method="post" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <div class="col-md-6">
          <div class="form-group has-feedback">
            <label>Name</label>
            <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name',$admin->name)}}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
            @error('name')
              <span class="invalid-feedback"> 
                <strong>{{ $message }}</strong> 
              </span>
            @enderror
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group has-feedback">
            <label>Email</label>
            <input type="text" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email',$admin->email)}}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
            @error('email')
              <span class="invalid-feedback"> 
                <strong>{{ $message }}</strong> 
              </span>
            @enderror
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group has-feedback">
            <label>Password</label>
            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{old('password')}}">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
            @error('password')
              <span class="invalid-feedback"> 
                <strong>{{ $message }}</strong> 
              </span>
            @enderror
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <input type="submit" name="submit" class="btn btn-primary">
        </div>
      </div>
    </form>

  </div>
</div>
@endsection