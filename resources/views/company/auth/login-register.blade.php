@extends('layouts.company-login-app')
@section('title','Company Login/Register')
@section('content')
  <div class="row mb-3">
      <div class="col-lg-5">

        @php($login_active = null)
        @php($regiser_active = null)
        @if($tab_active=='login')
        @php($login_active = 'active show')
        @elseif($tab_active=='register')
        @php($regiser_active = 'active show')
        @endif

        @if(Session::has('tab_active') and Session::get('tab_active')=='register')
        @php($login_active = null)
        @php($regiser_active = 'active show')
        @endif

        @if(Session::has('tab_active') and Session::get('tab_active')=='login')
        @php($login_active = 'active show')
        @php($regiser_active = null)
        @endif

        <div class="bg-light shadow-box box-radius">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item"> <a class="tab-left nav-link {{$regiser_active}}" data-toggle="tab" href="#register">Register Your Company</a> </li>
            <li class="nav-item"> <a class="tab-right nav-link {{$login_active}}" data-toggle="tab" href="#login">Login</a> </li>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content box-radius">
            <div id="register" class="container tab-pane {{$regiser_active}}"><br>
              <!--Register-->
              <form method="POST" action="{{ route('company.register') }}" enctype="multipart/form-data">
              @csrf
              @if(Session::has('error_register'))
              <div class="alert alert-danger">{{Session::get('error_register')}}</div>
              @endif
              @if(Session::has('success_register'))
              <div class="alert alert-success">{{Session::get('success_register')}}</div>
              @endif
                <div class="form-group has-feedback">
    <input id="company_name" type="text" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{old('company_name')}}" required autofocus placeholder="Name of the company">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('company_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>

            <div class="form-group has-feedback">
    <input id="display_name" type="text" class="form-control {{ $errors->has('display_name') ? ' is-invalid' : '' }}" name="display_name" value="{{old('display_name')}}" required autofocus placeholder="Company Display Name">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('display_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
                                @endif
                            </div>

            <div class="form-group has-feedback">
    <input id="first_name" type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{old('first_name')}}" required autofocus placeholder="First Name">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
            <div class="form-group has-feedback">
    <input id="last_name" type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{old('last_name')}}" required autofocus placeholder="Last Name">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback">
    <input id="company_address" type="text" class="form-control {{ $errors->has('company_address') ? ' is-invalid' : '' }}" name="company_address" value="{{old('company_address')}}" required autofocus placeholder="Company Address">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('company_address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_address') }}</strong>
                                    </span>
                                @endif
                            </div>
            
            <div class="form-group has-feedback">
    <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" required autofocus placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
            <div class="form-group has-feedback">
    <input id="phone" type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{old('phone')}}" required autofocus placeholder="Phone">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback">
                                <p class="mb-0">Upload Company Logo</p>
    <div class="upload_file"> <span><img src="{{asset('frontend/images/paper_clip.png')}}"> Upload Image</span>
                    <input name="logo" type="file" required="" accept="image/*">
                  </div>
                  <div class="file-text">File Size between 200Kb-2Mb only JPG or PNG </div>
        @if ($errors->has('logo'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                @endif
                            </div>

                
                <div class="form-group">
                  <button type="submit" class="pl-4 pr-4 btn btn-primary"> Register </button>
                  <a style="float: right; margin-top: 10px;" href="{{route('company.forgot-password')}}">Forgotten password?</a>
                </div>
                {{-- 
                <div class="mt-2 mb-3">
                  <p class="session-text">Session will be live on</p>
                  <img src="{{asset('frontend/images/fb-live.png')}}"> <img src="{{asset('frontend/images/youtube.png')}}"> </div>
                 --}}
                <br>
              </form>
              <!--./Register-->
            </div>
            <div id="login" class="container tab-pane fade {{$login_active}}"><br>
              <!--Login-->
              <form action="{{ route('login') }}" method="post">
              @csrf
              @if(Session::has('error_login'))
              <div class="alert alert-danger">{{Session::get('error_login')}}</div>
              @endif
              @if(Session::has('success_login'))
              <div class="alert alert-success">{{Session::get('success_login')}}</div>
              @endif
                <div class="form-group has-feedback">
    <input id="user_email" type="user_email" class="form-control {{ $errors->has('user_email') ? ' is-invalid' : '' }}" name="user_email" value="{{old('user_email')}}" required autofocus placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('user_email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('user_email') }}</strong>
                                    </span>
                                @endif
                            </div>
    <div class="form-group has-feedback">
       <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
    </div>
    <div class="form-group mt-3">
      <button type="submit" class="btn btn-primary"> Login </button>
      <a style="float: right; margin-top: 10px;" href="{{route('company.forgot-password')}}">Forgotten password?</a>
    </div>
                <br>
              </form>
              <!--./Login-->
            </div>
          </div>
        </div>
      </div>
    </div>
<!--./px-5 py-3-->
@endsection