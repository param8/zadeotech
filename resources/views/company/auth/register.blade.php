<!DOCTYPE html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custom-style.css')}}">
<title>Company Register</title>
<link rel="shortcut icon" href="{{asset(env('APP_LOGO'))}}">
</head>
<body>
<div class="container-fluid">
    <div class="container">
  <div class="row">
    <div class="col-sm-3 d-flex align-items-center justify-content-center">
      <div class="text-center logo"> <img src="{{asset(env('APP_LOGO'))}}"> </div>
    </div>
    <!-- ./col-sm-3-->
    <div class="col-sm-9">
      <div class="bg-light shadow-box">
        <h6 class="text-center py-2 bg-dark text-white"> Company Register </h6>
        <div class="px-5 py-3">
          <form method="POST" action="{{ route('company.register') }}" enctype="multipart/form-data">
            @component('components.alert')
            @endcomponent
            @csrf
            <div class="form-group has-feedback">
    <input id="company_name" type="text" class="form-control input {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{old('company_name')}}" required autofocus placeholder="Name of the company">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('company_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                @endif
                            </div>

            <div class="form-group has-feedback">
    <input id="display_name" type="text" class="form-control input {{ $errors->has('display_name') ? ' is-invalid' : '' }}" name="display_name" value="{{old('display_name')}}" required autofocus placeholder="Company Display Name">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('display_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
                                @endif
                            </div>

            <div class="form-group has-feedback">
    <input id="first_name" type="text" class="form-control input {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{old('first_name')}}" required autofocus placeholder="First Name">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('first_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
            <div class="form-group has-feedback">
    <input id="last_name" type="text" class="form-control input {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{old('last_name')}}" required autofocus placeholder="Last Name">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('last_name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback">
    <input id="company_address" type="text" class="form-control input {{ $errors->has('company_address') ? ' is-invalid' : '' }}" name="company_address" value="{{old('company_address')}}" required autofocus placeholder="Company Address">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('company_address'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('company_address') }}</strong>
                                    </span>
                                @endif
                            </div>
            
            <div class="form-group has-feedback">
    <input id="email" type="email" class="form-control input {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{old('email')}}" required autofocus placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
            <div class="form-group has-feedback">
    <input id="phone" type="text" class="form-control input {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{old('phone')}}" required autofocus placeholder="Phone">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('phone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group has-feedback">
                                <label>Upload Company Logo</label>
    <input accept="image/*" id="logo" type="file" class="file {{ $errors->has('logo') ? ' is-invalid' : '' }}" name="logo" value="{{old('logo')}}" required autofocus placeholder="logo">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <div class="file-text">File Size between 100Kb-2Mb Dimension 100:200 only JPG or PNG </div>
        @if ($errors->has('logo'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('logo') }}</strong>
                                    </span>
                                @endif
                            </div>
           
            <div class="form-group">
              <button type="submit" class="pl-4 pr-4 primary"> REGISTER </button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!--./container-->
    </div>
    <!-- ./col-sm-9-->
  </div>
  <!--./col-sm-12-->
</div>
<!--./row-->
<div class="footer-box text-center">
  <p class="blue-color mb-0">&copy; {{date('Y')}} zadeotech.in</p>
</div>
</div>
<!--./container-fluid-->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!--<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>-->
<script src="{{asset('frontend/js/jquery-3.4.1.min.js')}}"></script>
<script src="{{asset('frontend/js/popper.min.js')}}" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="{{asset('frontend/js/bootstrap.min.js')}}" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script src="{{asset('frontend/js/custom.js')}}"></script>
</body>
</html>