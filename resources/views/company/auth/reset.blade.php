@extends('layouts.company-login-app')
@section('title','Account recovery')
@section('content')
  <div class="row mb-3">
      <div class="col-lg-5">

        <div class="bg-light shadow-box box-radius">
          
          <div class="tab-content1 box-radius">
            <div id="register" class="container tab-pane"><br>
              <!--Register-->
              <form method="POST" action="{{ route('company.password.reset') }}">
                @csrf
                <input type="hidden" name="token" value="{{$token}}">
                <h5 class="text-center">Account Recovery</h5>

                @if(Session::has('success'))
                <div class="alert alert-success">{{Session::get('success')}}</div>
                @endif

                <div class="form-group has-feedback">
                  <input id="email" type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$email}}" required autofocus placeholder="Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group has-feedback">
                  <input id="password" type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{old('password')}}" required autofocus placeholder="Password">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  @if ($errors->has('password'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                  @endif
                </div>

                <div class="form-group has-feedback">
                  <input id="password_confirmation" type="password" class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" value="{{old('password_confirmation')}}" required autofocus placeholder="Confirm Password">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  @if($errors->has('password_confirmation'))
                    <span class="invalid-feedback">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                    </span>
                  @endif
                </div>

                
                <div class="form-group">
                  <button type="submit" class="pl-4 pr-4 btn btn-primary"> Reset Password </button>
                  <a style="float: right; margin-top: 10px;" href="{{route('login')}}">Login</a>
                </div>
               
            <br>
              </form>
              <!--./Register-->
            </div>
          </div>
        </div>
      </div>
    </div>
<!--./px-5 py-3-->
@endsection