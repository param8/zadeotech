@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-change-bg','active')
@section('title','Change Background')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
  <div class="col-sm-12">
    <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Change Background</h5>
  </div>
  <div class="card-body">
    <form class="form-horizontal" action="{{ route('company.change-bg') }}" method="post"  enctype="multipart/form-data">
      @csrf
      @component('components.alert')
      @endcomponent
      <div class="row">
      <div class="col-sm-6">

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Background Image</label>
        <div class="col-sm-9">
        <input accept="image/*" id="image" type="file" class="file {{ $errors->has('image') ? ' is-invalid' : '' }}" name="image" placeholder="Logo">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -2Mb Dimension 1200:180 Only JPG or PNG </small>
            </div>
            @error('image')
              <span class="invalid-feedback">
                  <strong>{{ $message }}</strong>
              </span>
            @enderror
      </div>
      </div>
      
      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Update </button>
      </div>
    </form>
    <?php
      $bg = Auth::guard('company')->user()->bg_image;
      ?>
      @if(!empty($bg))
      <hr>
      <form onSubmit="return confirm('Are you sure to remove background?')" action="{{route('company.change-bg.change_bg_remove')}}" method="post">
        @csrf
        <button type="submit" class="btn btn-danger btn-xs">Remove</button>
      </form>
      <div class="mt-2"><img src="{{$bg}}" width="200px;"></div>
      @endif
  </div>
</div>
</div>
</div>
@endsection