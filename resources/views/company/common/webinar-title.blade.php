@if(Auth::guard('company')->check())
<h4><strong>Webinar :</strong> <a href="{{ route('company.webinar') }}">{!!$webinar->webinar_title!!}</a></h4>
@else
<h4><strong>Webinar :</strong> <a href="{{ route('moderator.webinar') }}">{!!$webinar->webinar_title!!}</a></h4>
@endif