@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-dashboard','active')
@section('title','Dashboard')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Dashboard</h5>
  </div>
  <div class="card-body">
  	<br>
    @component('components.alert')
      @endcomponent
  	<div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$total_webinar_today}}</h3>

                <p>No. of Webinars today</p>
              </div>
              <div class="icon">
                <i class="fas fa fa-server"></i>
              </div>
              <a href="{{route('company.webinar')}}?webinar_title=&webinar_filter=today&filter=filter" class="small-box-footer">Go to webinar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$total_upcoming_webinar}}</h3>

                <p>No. of Upcoming Webinars</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="{{route('company.webinar')}}?webinar_filter=upcomming&filter=filter" class="small-box-footer">Go to webinar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$total_completed_webinar}}</h3>

                <p>No. of Total Webinars completed</p>
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="{{route('company.webinar')}}?webinar_filter=past&filter=filter" class="small-box-footer">Go to webinar <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          {{-- 
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>65</h3>

                <p>Unique Visitors</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
           --}}
        </div>
  </div>
</div>
@endsection