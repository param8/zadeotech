@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-hall','active')
@section('title','List Hall')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Hall ({{!empty($halls) ? $halls->count() : 0}})</h5>
    <span class="float-right"><a href="javascript:void(0)" onclick="add_hall()" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create Hall</a></span>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    

  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Create Date</th>
            <th width="10%">Action</th>
          </tr>
        </thead>

        <tbody>
        @forelse($halls as $key=>$hall)
          <tr>
            <td>{{$key+1}}</td>
            <td>
              <div>{{$hall->name}}</div>
            </td>
            <td>
              <div>{{ $hall->created_at->format('d-M-Y') }}</div>
            </td>
           <td></td>

          </tr>
        @empty
          <tr>
            <td colspan="8" class="text-center">
              <h1 class="text-muted">Empty</h1>
              
            </td>
          </tr>
        @endforelse
        </tbody>

      </table>
      
    </div>
  </div>
</div>
<div class="modal fade" id="addModalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-plus text-primary"></i> Create Hall </h5>
        <button type="button" class="close"  onclick="close_modal()">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="createConditionForm" action="{{route('company.webinar.store-hall')}}" method="POST">
      @csrf
      <div id="error_message"></div>
        <div class="modal-body">
          <div class="row">
            <div class="col-xl-12">
              <div class="form-group row">
                <label class="col-lg-12 col-md-12 col-xl-12 col-form-label">Hall Name</label>
                <div class="col-lg-12 col-md-12 col-xl-12">
                  <input type="text" class="form-control" id="name" name="name" required>
                </div>
              </div>
            </div>
        
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection
<script>
  function add_hall(){
   $('#addModalForm').modal('show');
  }

</script>