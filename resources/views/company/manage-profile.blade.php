@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-manage-profile','active')
@section('title','Manage Profile')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Manage Profile</h5>
  </div>
  <div class="card-body">
  	<form class="form-horizontal" action="{{ route('company.manage-profile') }}" method="post"  enctype="multipart/form-data">
      @csrf
      @component('components.alert')
      @endcomponent
      
      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Company Name</label>
        <div class="col-sm-7">
        <input type="text" class="form-control {{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{old('company_name',$user->company_name)}}" placeholder="Company Name">
      </div>
        </div>
      
      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Company Display Name</label>
        <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('display_name') ? ' is-invalid' : '' }}" name="display_name" required placeholder="Company Display Name" value="{{old('display_name',$user->display_name)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('display_name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">First Name</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" required placeholder="First Name" value="{{old('first_name',$user->first_name)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('first_name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>


 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Last Name</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" required placeholder="Last Name" value="{{old('last_name',$user->last_name)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('last_name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>


 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Company Address</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('company_address') ? ' is-invalid' : '' }}" name="company_address" required placeholder="Company Address" value="{{old('company_address',$user->company_address)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('company_address')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Email</label>
  <div class="col-sm-9">
        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" required placeholder="Email" value="{{old('email',$user->email)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('email')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Phone</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required placeholder="Phone" value="{{old('phone',$user->phone)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('phone')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

  <div class="form-group row has-feedback">
    <label class="col-sm-3 col-form-label">Logo</label>
    <div class="col-sm-9">
    <label>Upload Company Logo</label>
    <input accept="image/*" id="logo" type="file" class="file {{ $errors->has('logo') ? ' is-invalid' : '' }}" name="logo" value="{{old('logo')}}" placeholder="Logo">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <div>
        <small class="text-muted"><i class="fa fa-info-circle"></i> File Size between 100Kb-2Mb Dimension 100:200 only JPG or PNG </small>
        </div>
  @error('logo')
          <span class="invalid-feedback">
              <strong>{{ $message }}</strong>
          </span>
      @endif
      <img src="{{$user->logo}}" width="100px;">
  </div>
  </div>


      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Update Profile </button>
      </div>
    </form>
  
</div>
</div>
@endsection