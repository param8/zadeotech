
    <form class="form-horizontal" action="{{ route('company.webinar.moderator.create',$webinar->id) }}" method="post"  enctype="multipart/form-data">
      @csrf
      

      <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Name</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Name" value="{{old('name')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Email</label>
  <div class="col-sm-9">
        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{old('email')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('email')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>


<div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Password</label>
  <div class="col-sm-9">
        <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" value="{{old('password')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('password')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Access To </label>
  <div class="col-sm-3">
    <label>
    <input type="checkbox" name="access_to_quiz" value="1">
    Quiz
    </label>
  </div>
  <div class="col-sm-3">
    <label>
    <input type="checkbox" name="access_to_poll" value="1">
    Poll
    </label>
  </div>
  <div class="col-sm-3">
    <label>
    <input type="checkbox" name="access_to_query" value="1">
    User Query/Comment
    </label>
  </div>
</div>
    
    <div class="row">
      <div class="col-sm-3">
            <label>
            <input type="checkbox" name="notify" value="1">
            Notify 
            </label>
      </div>

      <div class="col-sm-3">
      <div class="form-group">
    
        <button type="submit" class="btn btn-primary"> Add Moderator </button>
      </div>
      </div>
      </div>
    </form>
  
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
@endpush