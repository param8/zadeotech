@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Moderator')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Moderator ({{$moderators->count()}})</h5>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    @include('company.common.webinar-title')
    <h5>Moderator Login URL: <a href="https://zadeotech.in/moderator/login" target="_blank">https://zadeotech.in/moderator/login</a></h5>
    @if(is_copmpany())
    @include('company.moderator.create')
    @endif
  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Access To</th>
            <th>Action</th>
          </tr>
        </thead>

        <tbody>
        @forelse($moderators as $key=>$moderator)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{ $moderator->name }}</td>
            <td>{{ $moderator->email }}</td>
            <td>
              <div>{!! $moderator->moderator_webinar->access_to_quiz==1?'<i class="fa fa-check-circle text-success"></i>':'<i class="fa fa-times-circle text-danger"></i>' !!} Quiz</div>

              <div>{!! $moderator->moderator_webinar->access_to_quiz==1?'<i class="fa fa-check-circle text-success"></i>':'<i class="fa fa-times-circle text-danger"></i>' !!} Poll</div>

              <div>{!! $moderator->moderator_webinar->access_to_query==1?'<i class="fa fa-check-circle text-success"></i>':'<i class="fa fa-times-circle text-danger"></i>' !!} User Query/Comment</div>
            </td>
            <td>
              <div class="btn btn-group">
              <a class="btn btn-primary btn-xs" href="{{ route('company.webinar.moderator.edit',['webinar'=>$webinar->id, 'moderator'=>$moderator->id]) }}"><i class="fa fa-pencil-alt"></i></a>
              <a onClick="return confirm('Are you sure want to delete?')" class="btn btn-danger btn-xs" href="{{ route('company.webinar.moderator.detach',['webinar'=>$webinar->id, 'moderator'=>$moderator->id]) }}"><i class="fa fa-trash"></i></a>
            </div>
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="6" class="text-center">
              <h1 class="text-muted">Empty</h1>
              
            </td>
          </tr>
        @endforelse
        </tbody>

      </table>
      
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
@endpush