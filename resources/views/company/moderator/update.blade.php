@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Update Moderator')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Update Moderator</h5>
    <div class="card-tools">
      <a href="{{route('company.webinar.moderator',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-reply"></i> Back</a>
    </div>
  </div>
  <div class="card-body">
    @include('company.common.webinar-title')
    <h5>Moderator Login URL: <a href="https://zadeotech.in/moderator/login" target="_blank">https://zadeotech.in/moderator/login</a></h5>
    <hr>
    <form class="form-horizontal" action="{{ route('company.webinar.moderator.edit',['webinar'=>$webinar->id,'moderator'=>$moderator->id]) }}" method="post"  enctype="multipart/form-data">
      @csrf
      @component('components.alert', ['all_error_hide'=>true])
      @endcomponent

      <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Name</label>
  <div class="col-sm-9">
        <input type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="Name" value="{{old('name',$moderator->name)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('name')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

 <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Email</label>
  <div class="col-sm-9">
        <input type="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{old('email',$moderator->email)}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('email')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>


<div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Password</label>
  <div class="col-sm-9">
        <input type="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" value="{{old('password')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('password')
 <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror </div></div>

<div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Access To </label>
  <div class="col-sm-3">
    <label>
    <input type="checkbox" name="access_to_quiz" value="1" {{ $moderator->moderator_webinar->access_to_quiz==1?'checked':null }}>
    Quiz
    </label>
  </div>
  <div class="col-sm-3">
    <label>
    <input type="checkbox" name="access_to_poll" value="1" {{ $moderator->moderator_webinar->access_to_poll==1?'checked':null }}>
    Poll
    </label>
  </div>
  <div class="col-sm-3">
    <label>
    <input type="checkbox" name="access_to_query" value="1" {{ $moderator->moderator_webinar->access_to_query==1?'checked':null }}>
    User Query/Comment
    </label>
  </div>
</div>
  
  <div class="row">
      <div class="col-sm-3">
            <label>
            <input type="checkbox" name="notify" value="1">
            Notify 
            </label>
      </div>

      <div class="col-sm-3">
      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Update Moderator </button>
      </div>
      </div>
    </form>
  
</div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
@endpush