@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Poll Users Answer')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Poll Users Answer</h5>
    <div class="card-tools">
      <a href="{{route('company.webinar.poll',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-comment"></i> Poll</a>
    </div>
  </div>
  <div class="card-body">
    @include('company.common.webinar-title')
    <hr>
    
    <div class="table-responsive">
      <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>User/Question</th>
              <th>Email</th>
              <th>Location</th>
              <th>Mobile Number</th>
              <th>No of currect answer</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td>1</td>
              <td>User/Question</td>
              <td>Email</td>
              <td>Location</td>
              <td>Mobile Number</td>
              <td>No of currect answer</td>
            </tr>
            <tr>
              <td>2</td>
              <td>User/Question</td>
              <td>Email</td>
              <td>Location</td>
              <td>Mobile Number</td>
              <td>No of currect answer</td>
            </tr>
          </tbody>
      
      </table>
    </div>
  
</div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
@endpush