@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Add Poll')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Add Poll</h5>
    <div class="card-tools">
      <a href="{{route('company.webinar.poll',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-comment"></i> Poll</a>
    </div>
  </div>
  <div class="card-body">
    @include('company.common.webinar-title')
    <hr>
    <form class="form-horizontal" action="{{ route('company.webinar.poll.create',$webinar->id) }}" method="post"  enctype="multipart/form-data">
      @csrf
      @component('components.alert')
      @endcomponent

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Poll</label>
        <div class="col-sm-9">
        <input type="text" required="" class="form-control {{ $errors->has('question') ? ' is-invalid' : '' }}" autofocus="" name="question" value="{{old('question')}}">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
        @error('question')
          <span class="invalid-feedback"> 
            <strong>{{ $message }}</strong> 
          </span>
        @enderror
      </div>
        </div>


         <div class="form-group row has-feedback">
          <label class="col-sm-3 col-form-label">Options</label>

            <div class="col-sm-9 optional_ans_wrapp">
              
              <div class="input-group option_wrapper mb-3">
                <input type="text" required="" name="options[]" class="form-control float-right options">
                <div class="input-group-append">
                  <span class="input-group-text add_more_poll_option">
                    <i class="far fa fa-plus"></i>
                  </span>
                </div>
              </div>


            <!-- /.input group -->
          </div>
          <!-- ./col-sm-9-->
        </div>
        <!-- /.form group -->

        

      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Add Poll </button>
      </div>
    </form>
  
</div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
@endpush