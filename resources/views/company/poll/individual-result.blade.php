@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Poll Result')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Result ({{ $poll->user_poll_answers->count() }})</h5>
    <div class="card-tools">
      <div class="btn-group">
      <a href="{{ Auth::guard('company')->check() ? route('company.webinar') : route('moderator.webinar') }}" class="btn btn-sm btn-primary"><i class="fa fa-reply"></i> Back</a>
      <a href="{{route('company.webinar.poll.individual-result.export',['webinar'=>$webinar->id, 'poll'=>$poll->id])}}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Export</a>
      </div>
    </div>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    @include('company.common.webinar-title')
    <h6><strong>Poll :</strong> {{ $poll->poll }}</h6>
  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Location</th>
            <th>Answer</th>
            <th>Date/Time</th>
          </tr>
        </thead>

        <tbody>
          @forelse($poll->user_poll_answers as $key=>$ans)
            <tr>
              <td>{{ $key+1 }}</td>
              <td>{{ $ans->user->name }}</td>
              <td>{{ $ans->user->email }}</td>
              <td>{{ $ans->user->phone }}</td>
              <td>{{ $ans->user->state ? $ans->user->state->name.' / ' : null }}{{ $ans->user->country ? $ans->user->country->name : null }}</td>
              <td>
                {{ $ans->poll_option->option }}
              </td>
              <td>{{ \App\Helpers\Helper::changeDateFormat($ans->created_at) }}</td>
            </tr>
            @empty
            <h1 class="text-center text-muted">Empty</h1>
          @endforelse
        </tbody>

      </table>
      
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
<script>
  Echo.channel('ShowUserQueryChannel')
  .listen('ShowUserQueryEvent', (e) => {
    //$('body').html(e.html);
    toastr.success('<a href="" style="color:#fff">' + e.query + ' Refresh</a>');
    console.log(e);
  });
</script>
@endpush