@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Poll')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Poll ({{$polls->count()}})</h5>
    <div class="card-tools">
      <div class="btn-group">
      @if(is_copmpany())
      <a href="{{route('company.webinar.poll.create',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Poll</a>
      @endif
      <a href="{{route('company.webinar.poll.export',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Export</a>
      </div>
    </div>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent

    @include('company.common.webinar-title')

  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Poll</th>
            @if(is_copmpany())
            <th>Operation</th>
            @endif
            <th>Result</th>
          </tr>
        </thead>

        <tbody>
        @forelse($polls as $key=>$pol)
          @php($total_votes = $pol->user_poll_answers->count())
          <tr>
            <td>{{$key+1}}</td>
            <td>
              <div>{{$pol->poll}}</div>
              @if($pol->poll_options->count())
              <div class="poll-result">
                <ol type="A" class="ol-style poll_option">
                  @foreach ($pol->poll_options as $poll_option)
                  <div class="poll-question">
                    <li>{{$poll_option->option}}
                      @php($poll_option_total_votes = $poll_option->user_poll_answers->count())
                      @if($poll_option_total_votes)
                        @php($poll_option_ans_percent = round(($poll_option_total_votes/$total_votes)*100))
                        <div class="result-bar" style= "width:{{$poll_option_ans_percent}}%">
                          <label for="{{$poll_option->id}}p">{{$poll_option_ans_percent}}%</label>
                        </div>
                      @else
                      <div class="result-bar" style= "width:0%">
                          <label>0%</label>
                        </div>
                      @endif
                    </li>
                  </div>
                  @endforeach
                </ol>
              </div>
              @else
              <small class="text-muted">No Options</small>
              @endif
            </td>
            @if(is_copmpany())
            <td>
              <div class="btn btn-group">
                <a href="{{ route('company.webinar.poll.edit',['webinar'=>$webinar->id,'poll'=>$pol->id]) }}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil-alt"></i></a>
                <a onclick="return confirm('Are you sure?')" href="{{ route('company.webinar.poll.delete',['webinar'=>$webinar->id,'poll'=>$pol->id]) }}" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
                

                <a href="{{ route('company.webinar.poll.status',['webinar'=>$webinar->id,'poll'=>$pol->id]) }}" id="status-{{$pol->id}}" class="btn {{ $pol->status==0?'btn-success':'btn-danger' }} btn-status" title="{{$pol->status==1?'Hide':'Show'}}">{{$pol->status==1?'Hide':'Show'}}</i></a>

                <div class="btn btn-default" id="timer-{{ $pol->id }}" style="width: 60px">0</div>

              </div>
            </td>
            @endif
            <td>
              <div class="btn-group">
                <a class="btn btn-primary btn-xs" href="{{ route('company.webinar.poll.individual-result',['webinar'=>$webinar->id, 'poll'=>$pol->id]) }}"><i class="fa fa-eye"></i> View Log ({{ $pol->user_poll_answers->count() }})</a>
                @if($pol->share_result==1)
                <a class="btn btn-danger btn-xs btn-share" id="{{ $pol->id }}" href="{{ route('company.webinar.poll.share-result',['webinar'=>$webinar->id, 'poll'=>$pol->id]) }}" title="Hide"><i class="fa fa-share"></i> Hide</a>
                @else
                <a class="btn btn-success btn-xs btn-share" id="{{ $pol->id }}" href="{{ route('company.webinar.poll.share-result',['webinar'=>$webinar->id, 'poll'=>$pol->id]) }}" title="Share result with audience"><i class="fa fa-share"></i> Share Result</a>
                @endif
              </div>
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="6" class="text-center">
              <h1 class="text-muted">Empty</h1>
              @if(is_copmpany())
              <br>
              <a href="{{route('company.webinar.poll.create',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Poll</a>
              <br>
              @endif
            </td>
          </tr>
        @endforelse
        </tbody>

      </table>
      
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
<script>
  Echo.channel('ShowUserQueryChannel')
  .listen('ShowUserQueryEvent', (e) => {
    //$('body').html(e.html);
    toastr.success('<a href="" style="color:#fff">' + e.query + ' Refresh</a>');
    console.log(e);
  });
</script>
@endpush
@push('css')
<style type="text/css">
  /*****Poll**********/
.poll-result {
      display: flex;
      flex-flow: column;
}
.poll-result .poll-question {
      padding-bottom: 10px;
}
.poll-result .poll-question p {
      margin: 0;
      padding: 5px 0;
}
.poll-result .poll-question p span {
      font-size: 14px;
}
.poll-result .poll-question .result-bar {
      display: flex;
      height: 25px;
      min-width: 35px;
      background-color: #38b673;
      border-radius: 5px;
      font-size: 14px;
      color: #FFFFFF;
      justify-content: center;
      align-items: center;
}
.result-bar label{
  margin-bottom: 0px;
}
.poll-question li{
  width: 100%;
}
/*****Poll End******/
</style>
@endpush