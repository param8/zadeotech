@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Quiz')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Quiz Questions ({{$quizzes->count()}})</h5>
    <div class="card-tools">
      <div class="btn-group">
      @if(is_copmpany())
      <a href="{{route('company.webinar.quiz.create',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Quiz</a>
      @endif
      <a href="{{route('company.webinar.quiz.export',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Export</a>
      {{-- 
      <a href="{{route('company.webinar.quiz.result',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Result</a>
       --}}
      </div>
    </div>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    @include('company.common.webinar-title')
  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Question</th>
            <th>Operation</th>
            <th>Quiz Summery</th>
          </tr>
        </thead>

        <tbody>
        @forelse($quizzes as $key=>$quiz)
          <tr>
            <td>{{$key+1}}</td>
            <td>
              <div>{{$quiz->question}}</div>
              @forelse($quiz->options as $k=>$opt)
                <div class="{{$opt->is_correct==true?'text-success':'text-danger'}}">{{$k+1}}. {{$opt->option}}</div>
              @empty
              <small class="text-muted">No Options</small>
              @endforelse
            </td>
            <td>
              <div class="btn btn-group">
                
            @if(is_copmpany())
                <a href="{{ route('company.webinar.quiz.edit',['webinar'=>$webinar->id,'quiz'=>$quiz->id]) }}" class="btn btn-primary" title="Edit"><i class="fa fa-pencil-alt"></i></a>

                <a onclick="return confirm('Are you sure?')" href="{{ route('company.webinar.quiz.delete',['webinar'=>$webinar->id,'quiz'=>$quiz->id]) }}" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></a>
            @endif

                <a href="{{ route('company.webinar.quiz.status',['webinar'=>$webinar->id,'quiz'=>$quiz->id]) }}" id="status-{{$quiz->id}}" class="btn {{ $quiz->status==0?'btn-success':'btn-danger' }} btn-status" title="{{$quiz->status==1?'Hide':'Show'}}">{{$quiz->status==1?'Hide':'Show'}}</i></a>

                <div class="btn btn-default" id="timer-{{ $quiz->id }}" style="width: 60px">0</div>

              </div>
            </td>
            <td>
              <div>
              <?php $quiz_ans = \App\Helpers\Helper::getQuizSummery($quiz); ?>
              {{ $quiz_ans['total_correct_answer'] }}/{{ $quiz_ans['total_answers'] }} ({{ $quiz_ans['percent'] }})
              </div>

              <div class="btn-group">
                <a class="btn btn-primary btn-xs" href="{{ route('company.webinar.quiz.individual-result',['webinar'=>$webinar->id, 'quiz'=>$quiz->id]) }}"><i class="fa fa-eye"></i> View Log</a>

                @if($quiz->share_result==1)
                <a class="btn btn-danger btn-xs btn-share" id="{{ $quiz->id }}" href="{{ route('company.webinar.quiz.share-result',['webinar'=>$webinar->id, 'quiz'=>$quiz->id]) }}" title="Hide"><i class="fa fa-share"></i> Hide</a>
                @else
                <a class="btn btn-success btn-xs btn-share" id="{{ $quiz->id }}" href="{{ route('company.webinar.quiz.share-result',['webinar'=>$webinar->id, 'quiz'=>$quiz->id]) }}" title="Share result with audience"><i class="fa fa-share"></i> Share Result</a>
                @endif

              </div>
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="6" class="text-center">
              <h1 class="text-muted">Empty</h1>
              @if(is_copmpany())
              <br>
              <a href="{{route('company.webinar.quiz.create',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Add Question</a>
              <br>
              @endif
            </td>
          </tr>
        @endforelse
        </tbody>

      </table>
      
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
<script>
  Echo.channel('ShowUserQueryChannel')
  .listen('ShowUserQueryEvent', (e) => {
    //$('body').html(e.html);
    toastr.success('<a href="" style="color:#fff">' + e.query + ' Refresh</a>');
    console.log(e);
  });
</script>
@endpush