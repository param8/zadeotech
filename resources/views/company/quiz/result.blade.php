@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Quiz Users Answer')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Quiz Users Answer</h5>
    <div class="card-tools">
      <a href="{{route('company.webinar.quiz',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-comment"></i> Quiz</a>
      <a href="{{route('company.webinar.quiz.result.export',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Export</a>
    </div>
  </div>
  <div class="card-body">
    @include('company.common.webinar-title')
    <hr>
    
    

    <div class="table-responsive">
      <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>Phone</th>
              <th>Location</th>
              <th>Currect Answer</th>
            </tr>
          </thead>

          <tbody>
            @forelse($users as $key=>$usr)
              <tr>
                <td>{{$key+1}}</td>
                <td>{{$usr->name}}</td>
                <td>{{$usr->email}}</td>
                <td>{{$usr->phone}}</td>
                <td>{{$usr->state ? $usr->state->name.' / ' : null}}{{$usr->country ? $usr->country->name : null}}</td>
                <td>
                  
                  {{ App\Helpers\Helper::userQuizResult($usr, $webinar) }}
                  
                </td>
              </tr>
            @empty
              <tr>
                <td colspan="6">
                  <br>
                    <h1 class="text-muted text-center">Empty</h1>
                  <br>
                </td>
              </tr>
            @endforelse
          </tbody>
      
      </table>
    </div>
  
</div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
@endpush