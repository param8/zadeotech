@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Ticker')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Ticker</h5>
  </div>
  <div class="card-body">
    @include('company.common.webinar-title')
    <hr>
    
    <?php
    $tick = null;
    $speed = null;
    $speed_key = null;
    if($ticker)
    {
      if($ticker->speed==2){
        $speed = 'Slow';
        $speed_key = 'slow';
      }
      elseif($ticker->speed==5){
        $speed = 'Medium';
        $speed_key = 'medium';
      }
      elseif($ticker->speed==10){
        $speed = 'Fast';
        $speed_key = 'fast';
      }

      echo "<p><strong>Ticker Speed:</strong> ".ucfirst($speed)."</p>";
      $tick = $ticker->ticker;
      echo '<marquee onmouseover="this.stop();" onMouseOut="this.start();" scrollamount="'.$ticker->speed.'">'.$tick.'</marquee>';
    }
    ?>
    <form class="form-horizontal" action="{{ route('company.webinar.ticker', $webinar->id) }}" method="post"  enctype="multipart/form-data">
      @csrf
      @component('components.alert')
      @endcomponent

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Ticker</label>
        <div class="col-sm-9">
        <textarea required="" class="textarea {{ $errors->has('ticker') ? ' is-invalid' : '' }}" name="ticker" placeholder="Title of the webinar" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{!!old('ticker',$tick)!!}</textarea>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span> 
        @error('ticker')
          <span class="invalid-feedback"> 
            <strong>{{ $message }}</strong> 
          </span>
        @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
  <label class="col-sm-3 col-form-label">Status <span class="text-danger">*</span></label>
  <div class="col-sm-9">
    <?php
    $speed_arr = ['slow'=>'Slow', 'medium'=>'Medium', 'fast'=>'Fast'];
    ?>
  <select name="speed" class="form-control">
    @foreach($speed_arr as $key=>$v)
    <option value="{{$key}}" {{old('speed',$speed_key)==$key?'selected':null}}>{{$v}}</option>
    @endforeach
  </select>      
  </div></div>


      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Add/Update Ticker </button>
      </div>
    </form>
  
</div>
</div>
@endsection