<?php
function array2csv(array &$exportable_data)
{
   if (count($exportable_data) == 0) {
     return null;
   }
   ob_start();
   $df = fopen("php://output", 'w');
   
   $header = ['Name', 'Email', 'Phone', 'Country', 'State', 'Created At'];
   fputcsv($df, ['abc','def']);
   fputcsv($df, $header);
   foreach ($exportable_data as $key=>$row){ 
     fputcsv($df,$row); 
   }

   //fputcsv($df, array_keys(reset($exportable_data))); //default
   
   fclose($df);
   return ob_get_clean();
}
function download_send_headers($filename) {
    // disable caching
    $now = gmdate("D, d M Y H:i:s");
    header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
    header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
    header("Last-Modified: {$now} GMT");

    // force download  
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");

    // disposition / encoding on response body
    header("Content-Disposition: attachment;filename={$filename}");
    header("Content-Transfer-Encoding: binary");
}

download_send_headers($filename);
echo array2csv($exportable_data);
die();
?>