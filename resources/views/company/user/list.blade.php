@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','User')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">Total Registrations : {{ $webinar->users->count() }}</h5>
    <div class="card-tools">
      <div class="btn-group">
      <a href="{{route('company.webinar')}}" class="btn btn-sm btn-primary"><i class="fa fa-reply"></i> Back</a>
      <a href="javascript:void(0)" onclick="printDiv('printableArea')" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</a>
      <a href="{{route('company.webinar.user.export',$webinar->id)}}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Export</a>
      
      </div>
    </div>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    @include('company.common.webinar-title')
  	<div class="table-responsive" id="printableArea">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Country</th>
            <th>State</th>
            <th>Optional Field</th>
            <th>Created At</th>
          </tr>
        </thead>

        <tbody>
          @foreach($webinar->users as $key=>$user)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$user->name}}</td>
            <td>{{$user->email}}</td>
            <td>{{$user->phone}}</td>
            <td>{{$user->country?$user->country->name:null}}</td>
            <td>{{$user->state?$user->state->name:null}}</td>
            <td>{{ \App\Helpers\Helper::getOptionalFieldUserData($user->id,$webinar->id) }}</td>
            <td>{{\App\Helpers\Helper::changeDateFormat($user->created_at)}}</td>
          </tr>
          @endforeach
        </tbody>

      </table>
      
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
@endpush