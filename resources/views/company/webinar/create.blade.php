@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-create','active')
@section('title','Create Webinar')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
  <div class="col-sm-12">
    <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
  </div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Create Webinar</h5>
  </div>
  <div class="card-body">
    <form class="form-horizontal" action="{{ route('company.webinar.create') }}" method="post"
      enctype="multipart/form-data">
      @csrf
      @component('components.alert')
      @endcomponent

      {{--
        {!!'<pre>'!!}
        {{ print_r(Session::all()) }}
      {!!'</pre>'!!}
      --}}

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Start End Date <span class="text-danger">*</span></label>
        <div class="col-sm-9">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" required="" name="date_range" value="{{old('date_range')}}"
              class="form-control float-right" id="reservationtime">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('date_range')
            <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
          </div>
          <!-- /.input group -->
        </div>
      </div>
      <!-- /.form group -->

      <div class="row">
        <div class="col-sm-6">
          <div class="form-group row has-feedback">
            <label class="col-sm-6 col-form-label">Start Time <span class="text-danger">*</span></label>

            <div class="col-sm-6 input-group date">
              {{--
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
                --}}
              <input type="text" id="start_time" name="start_time" required="" class="form-control">
            </div>
          </div>
        </div>

        <div class="col-sm-6">
          <div class="form-group row has-feedback">
            <label class="col-sm-6 col-form-label text-right">End Time <span class="text-danger">*</span></label>

            <div class="col-sm-6 input-group date">
              {{--
                <div class="input-group-prepend">
                    <div class="input-group-text"><i class="far fa-clock"></i></div>
                </div>
                --}}
              <input type="text" id="end_time" name="end_time" required="" class="form-control">
            </div>
          </div>
        </div>


      </div>


      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Title of the webinar <span class="text-danger">*</span></label>
        <div class="col-sm-9">
          <textarea required="" class="textarea {{ $errors->has('webinar_title') ? ' is-invalid' : '' }}"
            name="webinar_title" placeholder="Title of the webinar"
            style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{!!old('webinar_title')!!}</textarea>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('webinar_title')
          <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Description of the webinar <span class="text-danger">*</span></label>
        <div class="col-sm-9">
          <textarea required="" class="textarea {{ $errors->has('description') ? ' is-invalid' : '' }}"
            name="description" placeholder="Description of the webinar"
            style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{!!old('description')!!}</textarea>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('description')
          <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
        </div>
      </div>


      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">iFrame Embedded URL <span class="text-danger">*</span></label>
        <div class="col-sm-9">
          <textarea placeholder="iFrame Embedded URL"
            class="form-control{{ $errors->has('ifram_url') ? ' is-invalid' : '' }}"
            name="ifram_url">{{old('ifram_url')}}</textarea>
          <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('ifram_url')
          <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Upload Banner</label>
        <div class="col-sm-9">
          <input accept="image/*" id="banner" type="file" class="file {{ $errors->has('banner') ? ' is-invalid' : '' }}"
            name="banner" value="{{old('banner')}}" placeholder="Logo">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -2Mb Dimension 1200:180
              Only JPG or PNG </small>
          </div>
          @error('banner')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Upload Brochure <span class="text-danger">*</span>
          <div><small class="text-primary"><i class="fa fa-info-circle"></i> To upload multiple file hold 'CTRL' and
              select files.</small></div>
        </label>
        <div class="col-sm-9">
          <input accept="image/*" multiple id="brochure" type="file"
            class="file {{ $errors->has('brochure') ? ' is-invalid' : '' }}" name="brochure[]"
            value="{{old('brochure')}}" placeholder="Preview">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800
              Only JPG or PNG </small>
          </div>
          @error('brochure')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Upload Logo 1</label>
        <div class="col-sm-9">
          <input accept="image/*" id="logo1" type="file" class="file {{ $errors->has('logo1') ? ' is-invalid' : '' }}"
            name="logo1" value="{{old('logo1')}}" placeholder="Preview">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800
              Only JPG or PNG </small>
          </div>
          @error('logo1')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Upload Logo 2</label>
        <div class="col-sm-9">
          <input accept="image/*" id="logo2" type="file" class="file {{ $errors->has('logo2') ? ' is-invalid' : '' }}"
            name="logo2" value="{{old('logo2')}}" placeholder="Preview">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800
              Only JPG or PNG </small>
          </div>
          @error('logo2')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Upload Logo 3</label>
        <div class="col-sm-9">
          <input accept="image/*" id="logo3" type="file" class="file {{ $errors->has('logo3') ? ' is-invalid' : '' }}"
            name="logo3" value="{{old('logo3')}}" placeholder="Preview">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800
              Only JPG or PNG </small>
          </div>
          @error('logo3')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Upload Header Banner</label>
        <div class="col-sm-9">
          <input accept="image/*" id="header_banner" type="file"
            class="file {{ $errors->has('header_banner') ? ' is-invalid' : '' }}" name="header_banner"
            value="{{old('header_banner')}}" placeholder="Preview">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension
              1500(w):150(h) Only JPG or PNG </small>
          </div>
          @error('header_banner')
          <span class="invalid-feedback">
            <strong>{{ $message }}</strong>
          </span>
          @enderror
        </div>
      </div>

      {{--Branding page--}}
      <div class="branding-section">
        <div class="form-group row has-feedback">
          <label class="col-sm-3 col-form-label">Enable Branding Page</label>
          <div class="col-sm-9">
            <div class="material-switch pull-right">
              <input id="brandingPageSwitcher" name="branding_page" value="1" type="checkbox" />
              <label for="brandingPageSwitcher" class="label-default"></label>
            </div>
          </div>
        </div>
        <div style="display: none" id="brandingPageSection">
          <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Branding Logo <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <input accept="image/*" id="branding_logo" type="file"
                class="file {{ $errors->has('branding_logo') ? ' is-invalid' : '' }}" name="branding_logo"
                value="{{old('branding_logo')}}" placeholder="Preview">
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              <div>
                <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension
                  1200:800 Only JPG or PNG </small>
              </div>
              @error('branding_logo')
              <span class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>
          <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Branding Youtube Video <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <textarea placeholder="Branding Youtube Video"
                class="form-control{{ $errors->has('branding_youtube_video') ? ' is-invalid' : '' }}"
                name="branding_youtube_video">{{old('branding_youtube_video')}}</textarea>
              <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('branding_youtube_video')
              <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
            </div>
          </div>
          <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Branding Banner <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <input accept="image/*" id="branding_banner" type="file"
                class="file {{ $errors->has('branding_banner') ? ' is-invalid' : '' }}" name="branding_banner"
                value="{{old('branding_banner')}}" placeholder="Logo">
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              <div>
                <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -2Mb Dimension
                  1200:180 Only JPG or PNG </small>
              </div>
              @error('branding_banner')
              <span class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>

          <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Background Image <span class="text-danger">*</span></label>
            <div class="col-sm-9">
              <input accept="image/*" id="branding_bg" type="file"
                class="file {{ $errors->has('branding_bg') ? ' is-invalid' : '' }}" name="branding_bg"
                value="{{old('branding_bg')}}" placeholder="Logo">
              <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
              <div>
                <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -2Mb Dimension
                  1200:180 Only JPG or PNG </small>
              </div>
              @error('branding_bg')
              <span class="invalid-feedback">
                <strong>{{ $message }}</strong>
              </span>
              @enderror
            </div>
          </div>

        </div>
      </div>
      {{--./end of Branding page--}}

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Facebook</label>
        <div class="col-sm-9">
          <input type="text" class="form-control {{ $errors->has('facebook') ? ' is-invalid' : '' }}" name="facebook"
            placeholder="Facebook" value="{{old('facebook')}}">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('facebook')
          <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
        </div>
      </div>

      <!-- <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">YouTube</label>
        <div class="col-sm-9">
          <input type="text" class="form-control {{ $errors->has('youtube') ? ' is-invalid' : '' }}" name="youtube"
            placeholder="YouTube" value="{{old('youtube')}}">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('youtube')
          <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
        </div>
      </div> -->

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Optional field name</label>
        <div class="col-sm-9">
          <input type="text" class="form-control {{ $errors->has('optional_field') ? ' is-invalid' : '' }}"
            name="optional_field" placeholder="Optional Field" value="{{old('optional_field')}}">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          <div>
            <small class="text-muted"><i class="fa fa-info-circle"></i> This field will appear on registeration form
            </small>
          </div>
          @error('optional_field')
          <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
        </div>
      </div>

      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Status <span class="text-danger">*</span></label>
        <div class="col-sm-9">
          <select name="status" class="form-control">
            <option value="1">Enable</option>
            <option value="0">Disable</option>
          </select>
        </div>
      </div>
      <div class="form-group row has-feedback">
        <label class="col-sm-3 col-form-label">Halls</label>
       
        <div class="col-sm-6">
          <select name="hall[]" id="halls" class="js-example-basic-multiple form-control" multiple onchange="getProductRows()">
            <option value="">Select Hall</option>
            @foreach($halls as $hall)
            <option value="{{$hall->id}}">{{$hall->name}}</option>
            @endforeach
          </select>
        </div>
        
      </div>
      <div class="form-group row has-feedback">
      <label class="col-sm-3 col-form-label">YouTube</label>
      <div class="col-sm-8 col-md-8" >
        <div class="row" id="getHallRows"> 

        </div>
          <!-- <input type="text" class="form-control {{ $errors->has('youtube') ? ' is-invalid' : '' }}" name="youtube"
            placeholder="YouTube" value="{{old('youtube')}}">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('youtube')
          <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror -->
        </div>
      </div>


      <div class="form-group">
        <button type="submit" class="btn btn-primary"> Create Webinar </button>
      </div>
    </form>

  </div>
</div>
@endsection
@push('js')
<script src="{{asset('dashboard/datatimepicker/js/gijgo.min.js')}}" type="text/javascript"></script>
<link href="{{asset('dashboard/datatimepicker/css/gijgo.min.css')}}" rel="stylesheet" type="text/css" />
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script>
$(function() {

  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth() + 1; //January is 0! 
  var yyyy = today.getFullYear();
  if (dd < 10) {
    dd = '0' + dd
  }
  if (mm < 10) {
    mm = '0' + mm
  }
  var today = mm + '/' + dd + '/' + yyyy;

  //Date range picker with time picker
  $('#reservationtime').daterangepicker({
    minDate: today,
    locale: {
      format: 'MM/DD/YYYY'
    }
  });

  $('#start_time').timepicker();
  $('#end_time').timepicker();

})
</script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
$(document).ready(function() {
  $('.js-example-basic-multiple').select2();
});

function getProductRows(){
    var products = $("#halls").val();
    //alert(products);
    $.ajax({
    url: "{{route('company.webinar.get_hall_form')}}",
    type: 'POST',
    data: {
      "_token": "{{ csrf_token() }}",
        "products": products
    },
    success: function(data) {
      $('#getHallRows').html(data);

    }
  });
}
</script>
@endpush