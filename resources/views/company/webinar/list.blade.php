@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','List Webinar')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Webinars ({{$webinars->count()}})</h5>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    <form action="">
      <div class="table-responsive">
        <table class="table table-bordered">
          <thead>
            <tr>
              <th width="*">Webinar Title</th>
              <th width="15%">Filter</th>
              <th width="15%"></th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td><input name="webinar_title" type="text" class="form-control" value="{{isset($_GET['webinar_title']) ? $_GET['webinar_title'] : null}}"></td>
              <td>
                <select class="form-control" name="webinar_filter">
                  <option value="">All</option>
                  <option value="today" {{isset($_GET['webinar_filter']) ? ($_GET['webinar_filter']=='today'?'selected':null) : null}}>Today</option>
                  <option value="upcomming" {{isset($_GET['webinar_filter']) ? ($_GET['webinar_filter']=='upcomming'?'selected':null) : null}}>Upcomming</option>
                  <option value="past" {{isset($_GET['webinar_filter']) ? ($_GET['webinar_filter']=='past'?'selected':null) : null}}>Past</option>
                </select>
              </td>
              <td>
                <button type="submit" name="filter" value="filter" class="btn btn-primary"><i class="fa fa-filter far"></i> Filter</button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </form>

  	<div class="table-responsive">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th>Webinar</th>
            <th>Start Date</th>
            <th>End Date</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Brochure</th>
            <th width="10%">Action</th>
          </tr>
        </thead>

        <tbody>
        @forelse($webinars as $key=>$webinar)
          <tr>
            <td>{{$key+1}}</td>
            <td>
              <a target="_blank" href="{{ route('show_webinar',[$webinar->company->slug,$webinar->slug]) }}">
              {!!$webinar->webinar_title!!}
              </a>
            </td>
            <td>
              <div>{{ $webinar->start_date->format('d-M-Y') }}</div>
              <div><small class="text-muted">{{$webinar->start_date->diffForHumans()}}</small></div>
            </td>
            <td>
              <div>{{$webinar->end_date->format('d-M-Y')}}</div>
              <div><small class="text-muted">{{$webinar->end_date->diffForHumans()}}</small></div>
            </td>
            <td>
              <div>{{$webinar->start_time}}</div>
            </td>
            <td>
              <div>{{$webinar->end_time}}</div>
            </td>
            <td>
              <?php  $br = explode(",",$webinar->brochure); ?>
              @foreach($br as $b)
              <a title="Click to view" href="{{asset('uploads/webinar/'.$b)}}" target="_blank"><img style="border: solid 1px #ccc; padding: 05px; margin-right: 02px;" src="{{asset('uploads/webinar/'.$b)}}" width="50px"></a>
              @endforeach
            <td>
                @if(is_copmpany())
              <div class="btn btn-group">
                {{--
                <a target="_blank" href="{{ route('company.webinar.show',$webinar->id) }}" class="btn btn-primary btn-xs" title="Open in dashboard"><i class="far fa fa-eye"></i></a>
                --}}
                <a target="_blank" title="Open in website" href="{{ route('show_webinar',[$webinar->company->slug,$webinar->slug]) }}" class="btn btn-primary btn-xs"><i class="far fa fa-external-link-alt"></i></a>
                <a href="{{ route('company.webinar.edit',$webinar->id) }}" class="btn btn-warning btn-xs" title="Edit"><i class="far fa fa-pencil-alt"></i></a>
                <a onclick="return confirm('Are you sure you want to delete?')" title="Delete webinar" href="{{ route('company.webinar.delete',$webinar->id) }}" class="btn btn-danger btn-xs"><i class="far fa fa-trash"></i></a>
              </div>

              <div class="btn btn-group">
                <a href="{{route('company.webinar.moderator',$webinar->id)}}" class="btn btn-success btn-xs"><i class="fa fa-user-tie"></i> Moderators ({{$webinar->moderators->count()}})</a>
              </div>
                @endif

              @if(is_copmpany())
              <div class="btn btn-group">
                <a href="{{route('company.webinar.user-query',$webinar->id)}}" class="btn btn-success btn-xs">Ques({{$webinar->user_queries->count()}})</a>
                <a href="{{route('company.webinar.quiz',$webinar->id)}}" class="btn btn-primary btn-xs">Quiz({{$webinar->quizzes->count()}})</a>
                <a href="{{route('company.webinar.poll',$webinar->id)}}" class="btn btn-warning btn-xs">Poll({{$webinar->polls->count()}})</a>
              </div>
              @endif

              @if(is_moderator())
              <div class="btn btn-group">
                <?php
                $mod = is_moderator()->moderator_webinar;
                ?>
                @if($mod->access_to_quiz==1)
                  <a href="{{route('company.webinar.quiz',$webinar->id)}}" class="btn btn-primary btn-xs">Quiz({{$webinar->quizzes->count()}})</a>
                @endif
                @if($mod->access_to_poll==1)
                  <a href="{{route('company.webinar.poll',$webinar->id)}}" class="btn btn-warning btn-xs">Poll({{$webinar->polls->count()}})</a>
                @endif
                @if($mod->access_to_query==1)
                  <a href="{{route('company.webinar.user-query',$webinar->id)}}" class="btn btn-success btn-xs">Ques({{$webinar->user_queries->count()}})</a>
                @endif
              </div>
              @endif

              @if(is_copmpany())
              <div class="btn btn-group">
              <a href="{{ route('company.webinar.user', $webinar->id) }}" class="btn btn-dark btn-xs">Registration({{$webinar->users->count()}})</a>
              
              <a href="{{ route('company.webinar.ticker',$webinar->id) }}" class="btn btn-default btn-xs">Ticker</a>
            </div>
              @endif
            </td>
          </tr>
        @empty
          <tr>
            <td colspan="8" class="text-center">
              <h1 class="text-muted">Empty</h1>
              <br>
              @if(is_copmpany())
              <a href="{{ route('company.webinar.create') }}" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Create Webinar</a>
              @endif
              <br>
            </td>
          </tr>
        @endforelse
        </tbody>

      </table>
      {{$webinars->links('vendor.pagination.bootstrap-4')}}
    </div>
  </div>
</div>
@endsection