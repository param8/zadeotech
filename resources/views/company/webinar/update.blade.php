@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','Update Webinar')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
   <div class="col-sm-12">
      <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
   </div>
   <!-- /.col -->
</div>
<!-- /.row -->
<div class="card card-primary card-outline">
   <div class="card-header">
      <h5 class="m-0">Update Webinar</h5>
   </div>
   <div class="card-body">
      <form class="form-horizontal" action="{{ route('company.webinar.edit',$webinar->id) }}" method="post"  enctype="multipart/form-data">
         @csrf
         @component('components.alert')
         @endcomponent
         <?php
            $date_range = \App\Helpers\Helper::toDateRange($webinar->start_date, $webinar->end_date);
            ?>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Start End Date <span class="text-danger">*</span></label>
            <div class="col-sm-9">
               <div class="input-group">
                  <div class="input-group-prepend">
                     <span class="input-group-text"><i class="far fa-clock"></i></span>
                  </div>
                  <input type="text" required="" name="date_range" value="{{old('date_range',$date_range)}}" class="form-control float-right" id="reservationtime">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('date_range')
                  <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
               </div>
               <!-- /.input group -->
            </div>
         </div>
         <!-- /.form group -->
         <div class="row">
            <div class="col-sm-6">
               <div class="form-group row has-feedback">
                  <label class="col-sm-6 col-form-label">Start Time <span class="text-danger">*</span></label>
                  <div class="col-sm-6 input-group date">
                     {{--
                     <div class="input-group-prepend">
                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                     </div>
                     --}}
                     <input type="text" value="{{old('start_time',$webinar->start_time)}}" id="start_time" name="start_time" required="" class="form-control">
                  </div>
               </div>
            </div>
            <div class="col-sm-6">
               <div class="form-group row has-feedback">
                  <label class="col-sm-6 col-form-label text-right">End Time <span class="text-danger">*</span></label>
                  <div class="col-sm-6 input-group date">
                     {{--
                     <div class="input-group-prepend">
                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                     </div>
                     --}}
                     <input type="text" id="end_time" value="{{old('end_time',$webinar->end_time)}}" name="end_time" required="" class="form-control">
                  </div>
               </div>
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Title of the webinar <span class="text-danger">*</span></label>
            <div class="col-sm-9">
               <textarea required="" class="textarea {{ $errors->has('webinar_title') ? ' is-invalid' : '' }}" name="webinar_title" placeholder="Title of the webinar" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{!!old('webinar_title',$webinar->webinar_title)!!}</textarea>
               <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('webinar_title')
               <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Description of the webinar <span class="text-danger">*</span></label>
            <div class="col-sm-9">
               <textarea required="" class="textarea {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" placeholder="Description of the webinar" style="width: 100%; height: 400px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{!!old('description',$webinar->description)!!}</textarea>
               <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('description')
               <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">iFrame Embedded URL <span class="text-danger">*</span></label>
            <div class="col-sm-9">
               <textarea placeholder="iFrame Embedded URL" class="form-control{{ $errors->has('ifram_url') ? ' is-invalid' : '' }}" name="ifram_url">{{old('ifram_url',$webinar->ifram_url)}}</textarea>
               <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('ifram_url')
               <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror 
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Upload Banner</label>
            <div class="col-sm-9">
               <input accept="image/*" id="banner" type="file" class="file {{ $errors->has('banner') ? ' is-invalid' : '' }}" name="banner" value="{{old('banner')}}" placeholder="Logo">
               <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               <div>
                  <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -2Mb Dimension 1200:180 Only JPG or PNG </small>
               </div>
               @error('banner')
               <span class="invalid-feedback">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
               @if(!empty($webinar->banner))
               <img src="{{$webinar->banner}}" width="100px">
               <a href="{{ route('company.webinar.remove-banner',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
               @endif
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">
               Upload Brochure <span class="text-danger">*</span>
               <div><small class="text-primary"><i class="fa fa-info-circle"></i> To upload multiple file hold 'CTRL' and select files.</small></div>
            </label>
            <div class="col-sm-9">
               <input accept="image/*" multiple id="brochure" type="file" class="file {{ $errors->has('brochure') ? ' is-invalid' : '' }}" name="brochure[]" value="{{old('brochure')}}" placeholder="Preview">
               <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               <div>
                  <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800 Only JPG or PNG </small>
               </div>
               @error('brochure')
               <span class="invalid-feedback">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
               <?php $br = explode(",",$webinar->brochure); ?>
               @foreach($br as $b)
               <img style="border: solid 1px #ccc; padding: 05px; margin-right: 02px;" src="{{asset('uploads/webinar/'.$b)}}" width="100px">
               @endforeach
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Upload Logo 1</label>
            <div class="col-sm-9">
               <input accept="image/*" id="logo1" type="file" class="file {{ $errors->has('logo1') ? ' is-invalid' : '' }}" name="logo1" value="{{old('logo1')}}" placeholder="Preview">
               <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               <div>
                  <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800 Only JPG or PNG </small>
               </div>
               @error('logo1')
               <span class="invalid-feedback">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
               @if(!empty($webinar->logo1))
               <img src="{{$webinar->logo1}}" width="100px">
               <a href="{{ route('company.webinar.remove-logo1',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
               @endif
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Upload Logo 2</label>
            <div class="col-sm-9">
               <input accept="image/*" id="logo2" type="file" class="file {{ $errors->has('logo2') ? ' is-invalid' : '' }}" name="logo2" value="{{old('logo2')}}" placeholder="Preview">
               <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               <div>
                  <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800 Only JPG or PNG </small>
               </div>
               @error('logo2')
               <span class="invalid-feedback">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
               @if(!empty($webinar->logo2))
               <img src="{{$webinar->logo2}}" width="100px">
               <a href="{{ route('company.webinar.remove-logo2',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
               @endif
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Upload Logo 3</label>
            <div class="col-sm-9">
               <input accept="image/*" id="logo3" type="file" class="file {{ $errors->has('logo3') ? ' is-invalid' : '' }}" name="logo3" value="{{old('logo3')}}" placeholder="Preview">
               <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               <div>
                  <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800 Only JPG or PNG </small>
               </div>
               @error('logo3')
               <span class="invalid-feedback">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
               @if(!empty($webinar->logo3))
               <img src="{{$webinar->logo3}}" width="100px">
               <a href="{{ route('company.webinar.remove-logo3',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
               @endif
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Upload Header Banner</label>
            <div class="col-sm-9">
               <input accept="image/*" id="header_banner" type="file" class="file {{ $errors->has('header_banner') ? ' is-invalid' : '' }}" name="header_banner" value="{{old('header_banner')}}" placeholder="Preview">
               <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
               <div>
                  <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1500(w):150(h) Only JPG or PNG </small>
               </div>
               @error('header_banner')
               <span class="invalid-feedback">
               <strong>{{ $message }}</strong>
               </span>
               @enderror
               @if(!empty($webinar->header_banner))
               <img src="{{$webinar->header_banner}}" width="100px">
               <a href="{{ route('company.webinar.remove-header',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
               @endif
            </div>
         </div>
         {{--Branding page--}}
         <div class="branding-section">
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Enable Branding Page</label>
            <div class="col-sm-9">
               <div class="material-switch pull-right">
                  <input id="brandingPageSwitcher" name="branding_page" value="1" type="checkbox" {{$webinar->branding_page==1?'checked':null}} />
                  <label for="brandingPageSwitcher" class="label-default"></label>
               </div>
            </div>
         </div>
         <div style="display: {{$webinar->branding_page==1?'block':'none'}}" id="brandingPageSection">
            <div class="form-group row has-feedback">
               <label class="col-sm-3 col-form-label">Branding Logo <span class="text-danger">*</span></label>
               <div class="col-sm-9">
                  <input accept="image/*" id="branding_logo" type="file" class="file {{ $errors->has('branding_logo') ? ' is-invalid' : '' }}" name="branding_logo" value="{{old('branding_logo')}}" placeholder="Preview">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  <div>
                     <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -3Mb Dimension 1200:800 Only JPG or PNG </small>
                  </div>
                  @error('branding_logo')
                  <span class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                  @if(!empty($webinar->branding_logo))
                  <img src="{{$webinar->branding_logo}}" width="100px">
                  {{--
                  <a href="{{ route('company.webinar.remove-branding_logo',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
                  --}}
                  @endif
               </div>
            </div>
            <div class="form-group row has-feedback">
               <label class="col-sm-3 col-form-label">Branding Youtube Video <span class="text-danger">*</span></label>
               <div class="col-sm-9">
                  <textarea placeholder="Branding Youtube Video" class="form-control{{ $errors->has('branding_youtube_video') ? ' is-invalid' : '' }}" name="branding_youtube_video">{{old('branding_youtube_video',$webinar->branding_youtube_video)}}</textarea>
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('branding_youtube_video')
                  <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror 
               </div>
            </div>
            <div class="form-group row has-feedback">
               <label class="col-sm-3 col-form-label">Branding Banner <span class="text-danger">*</span></label>
               <div class="col-sm-9">
                  <input accept="image/*" id="branding_banner" type="file" class="file {{ $errors->has('branding_banner') ? ' is-invalid' : '' }}" name="branding_banner" value="{{old('branding_banner')}}" placeholder="Logo">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  <div>
                     <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -2Mb Dimension 1200:180 Only JPG or PNG </small>
                  </div>
                  @error('branding_banner')
                  <span class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                  @if(!empty($webinar->branding_banner))
                  <img src="{{$webinar->branding_banner}}" width="100px">
                  {{--
                  <a href="{{ route('company.webinar.remove-branding_banner',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
                  --}}
                  @endif
               </div>
            </div>

            <div class="form-group row has-feedback">
               <label class="col-sm-3 col-form-label">Background Image <span class="text-danger">*</span></label>
               <div class="col-sm-9">
                  <input accept="image/*" id="branding_bg" type="file" class="file {{ $errors->has('branding_bg') ? ' is-invalid' : '' }}" name="branding_bg" value="{{old('branding_bg')}}" placeholder="Logo">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  <div>
                     <small class="text-muted"><i class="fa fa-info-circle"></i> File size between 100Kb -2Mb Dimension 1200:180 Only JPG or PNG </small>
                  </div>
                  @error('branding_bg')
                  <span class="invalid-feedback">
                  <strong>{{ $message }}</strong>
                  </span>
                  @enderror
                  @if(!empty($webinar->branding_bg))
                  <img src="{{$webinar->branding_bg}}" width="100px">
                  {{--
                  <a href="{{ route('company.webinar.remove-branding_bg',$webinar->id) }}" class="btn btn-danger btn-xs">Remove</a>
                  --}}
                  @endif
               </div>
            </div>

         </div>
      </div>
         {{--./end of Branding page--}}
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Facebook</label>
            <div class="col-sm-9">
               <input type="text" class="form-control {{ $errors->has('facebook') ? ' is-invalid' : '' }}" name="facebook" placeholder="Facebook" value="{{old('facebook',$webinar->facebook)}}">
               <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('facebook')
               <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror 
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">YouTube</label>
            <div class="col-sm-9">
               <input type="text" class="form-control {{ $errors->has('youtube') ? ' is-invalid' : '' }}" name="youtube" placeholder="YouTube" value="{{old('youtube',$webinar->youtube)}}">
               <span class="glyphicon glyphicon-lock form-control-feedback"></span> @error('youtube')
               <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror 
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Optional field name</label>
            <div class="col-sm-9">
               <input type="text" class="form-control {{ $errors->has('optional_field') ? ' is-invalid' : '' }}" name="optional_field" placeholder="Optional Field" value="{{old('optional_field',$webinar->optional_field)}}">
               <span class="glyphicon glyphicon-lock form-control-feedback"></span>
               <div>
                  <small class="text-muted"><i class="fa fa-info-circle"></i> This field will appear on registeration form </small>
               </div>
               @error('optional_field')
               <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> @enderror 
            </div>
         </div>
         <div class="form-group row has-feedback">
            <label class="col-sm-3 col-form-label">Status <span class="text-danger">*</span></label>
            <div class="col-sm-9">
               <select name="status" class="form-control">
               <option value="1" {{old('status',$webinar->status)==1?'selected':null}}>Enable</option>
               <option value="0" {{old('status',$webinar->status)==0?'selected':null}}>Disable</option>
               </select>      
            </div>
         </div>
         <div class="form-group">
            <button type="submit" class="btn btn-primary"> Update </button>
            <a href="{{ route('company.webinar') }}" class="btn btn-default"> Cancel </a>
         </div>
      </form>
   </div>
</div>
@endsection
@push('js')
<script src="{{asset('dashboard/datatimepicker/js/gijgo.min.js')}}" type="text/javascript"></script>
<link href="{{asset('dashboard/datatimepicker/css/gijgo.min.css')}}" rel="stylesheet" type="text/css" />
<script>
   $(function () {
    
     /*var today = new Date(); 
     var dd = today.getDate(); 
     var mm = today.getMonth()+1; //January is 0! 
     var yyyy = today.getFullYear(); 
     if(dd<10){ dd='0'+dd } 
     if(mm<10){ mm='0'+mm } 
     var today = mm+'/'+dd+'/'+yyyy; 
   */
     //Date range picker with time picker
     $('#reservationtime').daterangepicker({
       //minDate:today,
       locale: {
         format: 'MM/DD/YYYY'
       }
     });
   
      $('#start_time').timepicker();
      $('#end_time').timepicker();
     
   })
</script>
@endpush