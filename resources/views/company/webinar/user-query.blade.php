@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-webinar-list','active')
@section('title','User Query/Question')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="card-title">User Query/Question ({{$webinar->user_queries->count()}})</h5>
    <div class="card-tools">
      <div class="btn-group">


        <a href="{{ Auth::guard('company')->check() ? route('company.webinar') : route('moderator.webinar') }}" class="btn btn-sm btn-primary"><i class="fa fa-reply"></i> Back</a>
      
        <a href="{{ route('company.webinar.user-query.export',$webinar->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> Export</a>
        <a onclick="printDiv('printable-area')" href="javascript::void(0)" class="btn btn-sm btn-primary"><i class="fa fa-print"></i> Print</a>
      </div>
    </div>
  </div>
  <div class="card-body">
    @component('components.alert')
    @endcomponent
    @include('company.common.webinar-title')
  	<div class="table-responsive" id="printable-area">
      <table class="table table-bordered">
        <thead>
          <tr>
            <th>#</th>
            <th width="2%">Name</th>
            <th width="5%">Email</th>
            <th width="10%">Phone</th>
            <th width="10%">State</th>
            <th width="10%">Country</th>
            <th width="*">Question</th>
            <th width="10%">Created at</th>
          </tr>
        </thead>

        <tbody>
          <?php 
          $i = $webinar->user_queries->count();
          ?>
          @forelse($webinar->user_queries as $key=>$query)
            <tr>
              <td>{{$i}}</td>
              <td>{{$query->user ? $query->user->name : null}}</td>
              <td>{{$query->user ? $query->user->email : null}}</td>
              <td>{{$query->user ? $query->user->phone : null}}</td>
              <td>{{$query->user ? ($query->user->state ? $query->user->state->name : null) : null}}</td>
              <td>{{$query->user ? ($query->user->country ? $query->user->country->name : null) : null}}</td>
              <td>{{ $query->query }}</td>
              <td>{{ \App\Helpers\Helper::changeDateFormat($query->created_at) }}</td>
            </tr>
            <?php $i--; ?>
          @empty
          <tr>
            <td colspan="8" class="text-center">
              <br>
              <h1 class="text-muted">Empty</h1>
              <br>
            </td>
          </tr>
          @endforelse
        </tbody>

      </table>
      
    </div>
  </div>
</div>
@endsection
@push('js')
<script type="text/javascript" src="{{asset('dashboard/mycustom/custom.js')}}"></script>
<script>
  Echo.channel('ShowUserQueryChannel')
  .listen('ShowUserQueryEvent', (e) => {
    //$('body').html(e.html);
    toastr.success('<a href="" style="color:#fff">' + e.query + ' Refresh</a>');
    console.log(e);
  });
</script>
@endpush