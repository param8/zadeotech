@component('mail::message')
Dear {{ucfirst($admin->name)}}

Your password has been reset!


Thanks,<br>
{{ config('app.name') }}
@endcomponent
