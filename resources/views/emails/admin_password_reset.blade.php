@component('mail::message')
Dear {{ucfirst($admin->name)}},

<br>
<p>You are receiving this email because we received a password reset request for your account.</p>
<br>

@component('mail::button', ['url' => $link])
Reset Password
@endcomponent
<br>
<p>This password reset link will expire in 60 minutes.</p>
<br>
<p>If you did not request a password reset, no further action is required.</p>
<br>
Thanks,<br>
{{ config('app.name') }}


<hr>

<p>
<small>
If you're having trouble clicking the "Reset Password" button, copy and paste the URL below into your web browser: {{$link}}
</small>
</p>
@endcomponent