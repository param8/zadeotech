@component('mail::message')
Dear {{ucfirst($company->first_name)}}
 
<p>
Thanks so much for completing your registration process. We can’t describe how much it means to us. We at Zadeotech strive to make your Live streaming/Webinar requirements better and richer.
</p>
<p>
Below are your credentials for managing your portal. 
</p>

<p>
<strong>Email: </strong>{{$company->email}}<br>
<strong>Password: </strong>{{$password}}
</p>

<p>
For urgent/technical questions, make sure to email us at support@zadeotech.in . This will help you get a faster response
</p>
<p>
All the best and Enjoy Streaming!
</p>

Cheers
<br>
Team {{ config('app.name') }},

@endcomponent
