@component('mail::message')
Hi {{ucfirst($company->first_name)}},
 
<p>
Thanks you for registering for Live Streaming Platform by Zadeotech. We've received your application and we'll soon assign a dedicated point person for assisting you through the registration and activation process. 
</p>
<p>
We would love you learn about your Webinar/Live Streaming requirements & discuss how Zadeotech can help, so please feel free to contact us at info@zadeotech.in or +91-9250002999
</p>
<p>
Looking forward to hearing from your soon.
</p>

Team {{ config('app.name') }},<br>

@endcomponent
