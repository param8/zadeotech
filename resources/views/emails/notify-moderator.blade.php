@component('mail::message')
<p>Dear {{ $moderator->name }},

<p>You are added as moderator</p>
@include('company.common.webinar-title')
<p><strong>Login Detail</strong></p>
<p>Url: <a href="https://zadeotech.in/moderator/login">Login page</a></p>
<p>Email: {{ $moderator->email }}</p>
<p>Password: {{ $password }}</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
