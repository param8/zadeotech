@component('mail::message')
Dear {{ucfirst($company->first_name)}}

Your password has been reset!


Thanks,<br>
{{ config('app.name') }}
@endcomponent
