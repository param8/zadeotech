@component('mail::message')
Dear Admin

Query from {{ $request->name }}

<p><strong>Name :</strong> {{ $request->name }}<br>
<strong>Email :</strong> {{ $request->email }}<br>
<strong>Phone :</strong> {{ $request->phone }}<br>
<strong>Message :</strong> {{ $request->message }}<br>
<strong>Intrested in :</strong> {{ $request->intrested_in }}</p>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
