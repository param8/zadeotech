@component('mail::message')
Hi {{ $webinar->company->first_name }},
 
<p>Thank you for registering for 
{!!$webinar->webinar_title!!}
</p>
<p>
Please submit any questions to: webinar@zadeotech.in
</p>
<p>
Date Time: {{ webinarStartDateTime($webinar) }}, India
</p>
<p>
Join from any of the below links:<br>
Weblink: <a href="{{ route('show_webinar',['company'=>$webinar->company->slug,'webinar'=>$webinar->slug]) }}" class="text-dark">Go to webinar</a><br>
@if(!empty($webinar->youtube))
YouTube: {{ $webinar->youtube }}<br>
@endif
@if(!empty($webinar->facebook))
Facebook: {{ $webinar->facebook }}<br>
@endif
</p>
<p>
Happy Viewing.<br>
Team {{ config('app.name') }}<br>
www.zadeotech.in
</p>
Thanks,<br>
{{ config('app.name') }}
@endcomponent