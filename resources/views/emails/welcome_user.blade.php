@component('mail::message')
Dear {{$user->name}},

<p>You are successfully registered.</p>

<p>{!!$webinar->webinar_title!!}</p>
<div class="size-14"> From : {{\App\Helpers\Helper::changeTimeFormat($webinar->start_time)}} - To: {{\App\Helpers\Helper::changeTimeFormat($webinar->end_time)}} |
              {{\App\Helpers\Helper::changeDateFormat($webinar->start_date)}} - {{\App\Helpers\Helper::changeDateFormat($webinar->end_date)}} </div>

@php
$url = url(route('show_webinar', 
				[
				'company'=>$webinar->company->slug, 
				'webinar'=>$webinar->slug
				]
			)).'?email='.urlencode($user->email);
@endphp

@component('mail::button', ['url' => $url])
Go to webinar
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
