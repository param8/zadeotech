<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{asset(env('APP_LOGO'))}}">

<title>{{preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title))}}</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<style type="text/css" media="screen">
body{
  background-image: url('{{$webinar->branding_bg}}') !important;
  background-repeat: no-repeat;
  background-size: cover;
}
.brdr{
    border: solid 1px #ccc;
}

.button {
    border-radius: 4px;
    background-color: #0078d7;
    border: solid 5px #f7f6f663;
    color: #FFFFFF;
    text-align: center;
    font-size: 20px;
    padding: 10px;
    width: 200px;
    transition: all 0.5s;
    cursor: pointer;
    margin: 5px;

}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
  color: #fff;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

</style>
</head>
<body>
    <div class="container-fluid">
        <div class="container">
            <div class="header-banner">
                <center><img class="mb-3" src="{{ $webinar->branding_logo }}" style="max-width:250px;"></center>
                <center>
                    <a href="{{ route('show_webinar',['company'=>$company->slug,'webinar'=>$webinar->slug]) }}" class="button">
                    <span>Skip to webinar</span></a>
                </center>
            </div>
            <br>
            @if(!empty($webinar->branding_youtube_video))
                <div class="iframe_wrapper text-center">
                  {!! $webinar->branding_youtube_video !!}
                </div>
            @endif
            <br>
            <center><img src="{{$webinar->branding_banner}}" style="max-width: 100%;"></center>

        </div>
    </div>
</body>
</html>