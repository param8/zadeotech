<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{asset(env('APP_LOGO'))}}">

<title>{{env('APP_NAME')}}</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custom-style.css')}}">
</head>
<?php
$bg = $company->bg_image;
?>
@if(!empty($bg))
<style type="text/css" media="screen">
body{
  background: url("{{$bg}}") no-repeat center top;
  background-attachment: fixed;
}  
</style>
@else
<style type="text/css" media="screen">
body{
  background: url('{{asset('frontend/images/webinar-bg.jpg')}}') no-repeat center top;
  background-attachment: fixed;
}  
</style>
@endif

<body>
<div class="container">
  <div class="">
    <div class="row no-gutters">
      <div class="col-sm-3 bg-pink d-flex1 item-items-center">
        <div class="text-center logo w-100 bg-light">
          <!-- <img src="https://zadeotech.in/webinar/aios/images/logo.png" class="img-fluid w-75"> -->
          <img src="{{$webinar->company->logo}}" class="img-fluid w-75"> </div>
        <!--   -->
        @if(Auth::check())
        <div id="query_wrapper">
        @include('front.quer-user')
        </div>
        <div id="quiz_wrapper">
        @include('front.quiz')
        </div>
        <div id="poll_wrapper">
        @include('front.poll')
        </div>
        @else
        <div id="register_wrapper">
          @if(isset($_GET['login']))
            @include('front.login-form')
          @else
            @include('front.register-form')
          @endif
        </div>
        @endif
        
        @if(Auth::check() and !empty($webinar->facebook) or !empty($webinar->youtube))
        <div class="mt-2 mb-3 col-sm-12">
          <p class="session-text">Session will also be live on</p>
          <a href="{{$webinar->facebook}}" target="_blank"><img src="{{asset('frontend/images/fb-live.png')}}"></a>
          <a href="{{$webinar->youtube}}" target="_blank"><img src="{{asset('frontend/images/youtube.png')}}"></a>
        </div>
        @endif
      </div>
      <!-- col-sm-3-->
      <div class="col-sm-9">
        <div class="bg-light">
          {{-- 
          <div class="py-2 px-4 header-1">
            <div class="size-18 text-blue"><a href="{{ route('show_webinar',['company'=>$company->slug,'webinar'=>$webinar->slug]) }}" class="text-dark">
              {!!$webinar->webinar_title!!}</a></div>
            <div class="size-14"> From : {{\App\Helpers\Helper::changeTimeFormat($webinar->start_time)}} - To: {{\App\Helpers\Helper::changeTimeFormat($webinar->end_time)}} |
              {{\App\Helpers\Helper::changeDateFormat($webinar->start_date)}} </div>
          </div>
           --}}
           <div class="py-2 px-4">
            
            <div class="text-blue"><a href="{{ route('show_webinar',['company'=>$company->slug,'webinar'=>$webinar->slug]) }}" class="text-dark">
              {!!$webinar->webinar_title!!}</a></div>

            <div style="background: #000; color: #fff; padding: 15px; text-align: center"> From : {{\App\Helpers\Helper::changeTimeFormat($webinar->start_time)}} - To: {{\App\Helpers\Helper::changeTimeFormat($webinar->end_time)}} |
              {{\App\Helpers\Helper::changeDateFormat($webinar->start_date)}} </div>
          </div>
          <div class="px-2">
            <div class="row">
              <div class="col-sm-12">
                @if($webinar->ticker and !empty($webinar->ticker->ticker))
                <div class="mb-3 mt-3" style="color:black; font-size: 20px" >
                  <marquee scrollamount="{{$webinar->ticker->speed}}" onmouseover="this.stop();" onMouseOut="this.start();">
                  {!!$webinar->ticker->ticker!!}
                  </marquee>
                </div>
                @endif
                <div class="mb-3 mt-3">
                  <p>{!! $webinar->description !!}</p>
                </div>

                @if(Auth::check() and !empty($webinar->ifram_url))
                <div class="iframe_wrapper mt-3 mb-3">
                  {!!$webinar->ifram_url!!}
                </div>
                @endif
                @if(!Auth::check())
                <img class="card-img-top" src="{{$webinar->brochure}}" > </div>
                @endif
              </div>
            </div>
            <br>
            <div class="row pl-2 pr-2">
              <div class="col-sm-12">
                <img src="{{ $webinar->banner }}" width="100%">
              </div>
            </div>
              <br>
            <div class="row pl-2 pr-2">
              @if(!empty($webinar->logo1))
              <div class="col-sm-4">
                <img src="{{ $webinar->logo1 }}" width="100%">
              </div>
              @endif

              @if(!empty($webinar->logo1))
              <div class="col-sm-4">
                <img src="{{ $webinar->logo2 }}" width="100%">
              </div>
              @endif

              @if(!empty($webinar->logo1))
              <div class="col-sm-4">
                <img src="{{ $webinar->logo3 }}" width="100%">
              </div>
              @endif
            </div>
            <!--row-->
          </div>
        </div>
      </div>
      <!--col-sm-9-->
    </div>
    <!--row-->
    <!-- <br><br> -->
  </div>
<div class="footer-box text-center">
  <p class="mb-0">&copy; {{date('Y')}} zadeotech.in</p>
</div>
<script type="text/javascript">
  $(document.body).on("change","#country", function(e){
    $(".loader").remove();
    $("#states_container").html('');
    $.ajax({
      type: "post",
      url:  "{{ route('get_state') }}",
      data: {country:$(this).val()},
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        $("#formWrapper").append("<div class='loader'><p>Loading...</p></div>");
      },
      success: function(data)
      {
        $(".loader").remove();
        $("#states_container").html(data.states);
      }
    });
  });
  $(document.body).on("click","#quiz_wrapper .pagination a", function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $.get(url,{type:'quiz'}, function(data){
      $('#quiz_wrapper').html(data.html);
    });
  });
  $(document.body).on("click","#poll_wrapper .pagination a", function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $.get(url,{type:'poll'}, function(data){
        $('#poll_wrapper').html(data.html);
    });
  });

  $(document.body).on("submit","#queryForm", function(e){
    e.preventDefault();
    $(".invalid-feedback").remove();
    $(".text-success").remove();
    $(".loader").remove();
    frm = $("#queryForm");
    $("#snackbar").html('');
    $.ajax({
      type: "post",
      url:  frm.attr('action'),
      data: $("#queryForm").serialize(),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        frm.append("<div class='loader'><p>Loading...</p></div>");
      },
      success: function(data)
      {
        $(".loader").remove();
        if($.isEmptyObject(data.success)){
          $.each( data.error, function( key, value ) {
            $("."+key).append('<span class="invalid-feedback d-block"><strong>'+value+'</strong></span>');
          });
        }
        else{
          frm[0].reset();
          $.fn.snackbar(data.success);
        }
      }
    });
  });
  $(document.body).on("submit","#quizForm", function(e){
    e.preventDefault();
    $(".invalid-feedback").remove();
    $(".text-success").remove();
    $(".loader").remove();
    frm = $("#quizForm");
    $("#snackbar").html('');

    $.ajax({
      type: "post",
      url:  frm.attr('action'),
      data: frm.serialize(),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        frm.append("<div class='loader'><p>Loading...</p></div>");
      },
      success: function(data)
      {
        $(".loader").remove();
        if($.isEmptyObject(data.success)){
          $.each( data.error, function( key, value ) {
            $("."+key).append('<span class="invalid-feedback d-block"><strong>'+value+'</strong></span>');
          });
        }
        else{
          frm[0].reset();
          frm.append('<p class="text-success">'+data.success+'</p>');
          $("#quiz_wrapper").html(data.html);
          $.fn.snackbar(data.success);
        }
      }
    });
  });

  $(document.body).on("submit","#pollForm", function(e){
    e.preventDefault();
    $(".invalid-feedback").remove();
    $(".text-success").remove();
    $(".loader").remove();
    frm = $("#pollForm");
    $("#snackbar").html('');

    $.ajax({
      type: "post",
      url:  frm.attr('action'),
      data: frm.serialize(),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        frm.append("<div class='loader'><p>Loading...</p></div>");
      },
      success: function(data)
      {
        $(".loader").remove();
        if($.isEmptyObject(data.success)){
          $.each( data.error, function( key, value ) {
            $("."+key).append('<span class="invalid-feedback d-block"><strong>'+value+'</strong></span>');
          });
        }
        else{
          frm[0].reset();
          frm.append('<p class="text-success">'+data.success+'</p>');
          $("#poll_wrapper").html(data.attended_poll_result);
          $.fn.snackbar(data.success);
        }
      }
    });
  });

  /*$(document.body).on("click","#btn-next-poll", function(e){
    e.preventDefault();
    $.ajax({
      type: "get",
      url:  $(this).attr('href'),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      success: function(data)
      {
        $("#poll_wrapper").html(data.html);
      }
    });
  });*/
  
  $.fn.snackbar=function(message){
    $("#snackbar").html(message);
    var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ 
        x.className = x.className.replace("show", ""); 
      }, 3000);
  };
</script>

<div id="snackbar"></div>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
<script>
  Echo.channel('ShowQuizChannel')
                .listen('ShwoQuizEvent', (e) => {
                  $('#quiz_wrapper').html(e.html);
                  console.log(e);
                });
  Echo.channel('ShowPollChannel')
                .listen('ShowPollEvent', (e) => {
                  $('#poll_wrapper').html(e.poll);
                  console.log(e);
                });
</script>
</body>
</html>