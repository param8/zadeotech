<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{asset(env('APP_LOGO'))}}">

<title>{{preg_replace('/[^a-zA-Z0-9\s]/', '', strip_tags($webinar->webinar_title))}}</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custom-style.css')}}">
</head>
<?php
$bg = $company->bg_image;
?>
@if(!empty($bg))
<style type="text/css" media="screen">
body{
  background: url("{{$bg}}") repeat center top;
  background-attachment: fixed;
}  
</style>
@else
<style type="text/css" media="screen">
body{
  background: url('{{asset('frontend/images/webinar-bg.jpg')}}') no-repeat center top;
  background-attachment: fixed;
}  
</style>
@endif

<body>
  @if(!empty($webinar->header_banner))
  <div class="header-banner">
    <img src="{{ $webinar->header_banner }}">
  </div>
  @endif
  <div class="header-banner">
    
  </div>
<div class="container">
  <div class="">
    <div class="row no-gutters">
      
      <div class="col-sm-9">
        <div class="bg-light">
          
           <div class="py-2 px-4">
            
            <div class="text-blue"><a href="{{ route('show_webinar',['company'=>$company->slug,'webinar'=>$webinar->slug]) }}" class="text-dark">
              {!!$webinar->webinar_title!!}</a></div>

            <div style="background: #000; color: #fff; padding: 10px; text-align: center; font-size: 18px"> From : {{\App\Helpers\Helper::changeTimeFormat($webinar->start_time)}} - To: {{\App\Helpers\Helper::changeTimeFormat($webinar->end_time)}} |
              @if($webinar->start_date->eq($webinar->end_date))
              {{ $webinar->start_date->format('d-M-Y') }}
              @else
              From : {{ $webinar->start_date->format('d-M-Y') }} - To : {{ $webinar->end_date->format('d-M-Y') }}
              @endif
            </div>
          </div>
          <div class="px-2">
            <div class="row">
              <div class="col-sm-12">
                @if($webinar->ticker and !empty($webinar->ticker->ticker))
                <div class="mb-3 mt-3" style="color:black; font-size: 20px" >
                  <marquee scrollamount="{{$webinar->ticker->speed}}" onmouseover="this.stop();" onMouseOut="this.start();">
                  {!!$webinar->ticker->ticker!!}
                  </marquee>
                </div>
                @endif
                <div class="mb-3 mt-3">
                  <p>{!! $webinar->description !!}</p>
                </div>

                @if(Auth::check() and !empty($webinar->ifram_url))
                <div class="iframe_wrapper mt-3 mb-3">
                  {!!$webinar->ifram_url!!}
                </div>
                @endif
                @if(!Auth::check())
                <?php $br = explode(",",$webinar->brochure); $i=0;?>
                  
                  @if(count($br) > 1)
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                      @foreach($br as $b)
                      <li data-target="#carouselExampleIndicators" data-slide-to="{{$i}}" class="{{ $loop->first ? 'active' : null }}"></li>
                      @php $i++; @endphp
                      @endforeach
                    </ol>
                    <div class="carousel-inner">

                      @foreach($br as $b)
                        <div class="carousel-item {{ $loop->first ? 'active' : null }}">
                          <img class="d-block w-100 card-img-top" src="{{asset('uploads/webinar/'.$b)}}">
                        </div>
                      @endforeach
                      
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                      <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                      <span class="carousel-control-next-icon" aria-hidden="true"></span>
                      <span class="sr-only">Next</span>
                    </a>
                  </div>
                  @else
                  @foreach($br as $b)
                    <img class="d-block w-100 card-img-top" src="{{asset('uploads/webinar/'.$b)}}">
                  @endforeach
                  @endif

                @endif
              </div>
            </div>
              @if(Auth::check())
              <div class="row bg-pink" style="margin: 0;">
                <div class="col-lg-4 col-md-4" style="padding:0">
                  <div id="query_wrapper">
                  @include('front.quer-user')
                  </div>
                </div>
                <div class="col-lg-4 col-md-4" style="padding:0">
                  <div id="quiz_wrapper">
                    @include('front.quiz')
                  </div>
                </div>
                <div class="col-lg-4 col-md-4" style="padding:0">
                  <div id="poll_wrapper">
                  @include('front.poll')
                  </div>
                </div>
              </div>
              @endif
              <br>
            @if(!empty($webinar->banner))
            <div class="row">
              <div class="col-sm-12">
                <img src="{{ $webinar->banner }}" width="100%">
              </div>
            </div>
            @endif
              <br>
            <div class="row">
              @if(!empty($webinar->logo1))
              <div class="col-sm-4">
                <img src="{{ $webinar->logo1 }}" width="100%">
              </div>
              @endif

              @if(!empty($webinar->logo1))
              <div class="col-sm-4">
                <img src="{{ $webinar->logo2 }}" width="100%">
              </div>
              @endif

              @if(!empty($webinar->logo1))
              <div class="col-sm-4">
                <img src="{{ $webinar->logo3 }}" width="100%">
              </div>
              @endif
            </div>
            <!--row-->
          </div>
        </div>
      </div>
      <!--col-sm-9-->
      <div class="col-sm-3 bg-pink d-flex1 item-items-center">
        <div class="text-center logo w-100 bg-light">
          <!-- <img src="https://zadeotech.in/webinar/aios/images/logo.png" class="img-fluid w-75"> -->
          <img src="{{$webinar->company->logo}}" class="img-fluid w-75"> </div>
        <!--   -->
        @if(Auth::check())
        <div id="query_wrapper_a">
        <form method="post" action="{{ route('logout_user') }}" style="padding-left: 15px; padding-top: 15px;">
          @csrf
          <button type="submit" class="btn btn-primary mt-1">Logout</button>
        </form>

        
        
        <div id="quiz_result_wrapper">
        @include('front.quiz-result')
        </div>
        
        <div id="poll_result_wrapper">
        @include('front.poll-result')
        </div>
        @else
        <div id="register_wrapper">
          @if(isset($_GET['login']))
            @include('front.login-form')
          @else
            @include('front.register-form')
          @endif
        </div>
        @endif
        
        @if(!empty($webinar->facebook) or !empty($webinar->youtube))
        <div class="mt-2 mb-3 col-sm-12">
          <p class="session-text">Session will also be live on</p>
          <a href="{{$webinar->facebook}}" target="_blank"><img src="{{asset('frontend/images/fb-live.png')}}"></a>
          <a href="{{$webinar->youtube}}" target="_blank"><img src="{{asset('frontend/images/youtube.png')}}"></a>
        </div>
        @endif

        <?php  
          $br = explode(",",$webinar->brochure); 
        ?>
        @if(Auth::check())
        <div style="padding: 15px;">
        @foreach($br as $b)
        <a title="Click to view" href="{{asset('uploads/webinar/'.$b)}}" target="_blank"><img style="border: solid 1px #ccc; padding: 05px; margin-right: 02px;" src="{{asset('uploads/webinar/'.$b)}}" width="50px"></a>
        @endforeach
        </div>
        @endif
        </div>

      </div>
      <!-- col-sm-3-->
    </div>
    <!--row-->
    <!-- <br><br> -->
  </div>
<div class="footer-box text-center">
  <p class="mb-0">&copy; {{date('Y')}} <a href="https://www.zadeotech.in/">zadeotech.in</a></p>
</div>
<script type="text/javascript">
  $(document.body).on("change","#country", function(e){
    //$(".loader").remove();
    $("#states_container").html('');
    $.ajax({
      type: "post",
      url:  "{{ route('get_state') }}",
      data: {country:$(this).val()},
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        //$("#formWrapper").append("<div class='loader'><p>Loading...</p></div>");
      },
      success: function(data)
      {
        //$(".loader").remove();
        $("#states_container").html(data.states);
      }
    });
  });
  $(document.body).on("click","#quiz_wrapper .pagination a", function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $.get(url,{type:'quiz'}, function(data){
      $('#quiz_wrapper').html(data.html);
    });
  });
  $(document.body).on("click","#poll_wrapper .pagination a", function(e){
    e.preventDefault();
    var url = $(this).attr('href');
    $.get(url,{type:'poll'}, function(data){
      $('#poll_wrapper').html(data.html);
    });
  });

  $(document.body).on("submit","#queryForm", function(e){
    e.preventDefault();
    $(".invalid-feedback").remove();
    $(".text-success").remove();
    //$(".loader").remove();
    frm = $("#queryForm");
    $("#snackbar").html('');
    $.ajax({
      type: "post",
      url:  frm.attr('action'),
      data: $("#queryForm").serialize(),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        //frm.append("<div class='loader'><p>Loading...</p></div>");
        $('.btn-query').prop('disabled', true);
      },
      success: function(data)
      {
        $('.btn-query').prop('disabled', false);
        //$(".loader").remove();
        if($.isEmptyObject(data.success)){
          $.each( data.error, function( key, value ) {
            $("."+key).append('<span class="invalid-feedback d-block"><strong>'+value+'</strong></span>');
          });
        }
        else{
          frm[0].reset();
          $.fn.snackbar(data.success);
        }
      }
    });
  });
  $(document.body).on("submit","#quizForm", function(e){
    e.preventDefault();
    $(".invalid-feedback").remove();
    $(".text-success").remove();
    //$(".loader").remove();
    frm = $("#quizForm");
    $("#snackbar").html('');

    $.ajax({
      type: "post",
      url:  frm.attr('action'),
      data: frm.serialize(),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        $('.btn-quiz').prop('disabled', true);
        //frm.append("<div class='loader'><p>Loading...</p></div>");
      },
      success: function(data)
      {
        $('.btn-quiz').prop('disabled', false);
        //$(".loader").remove();
        if($.isEmptyObject(data.success)){
          $.each( data.error, function( key, value ) {
            $("."+key).append('<span class="invalid-feedback d-block"><strong>'+value+'</strong></span>');
          });
        }
        else{
          frm[0].reset();
          frm.append('<p class="text-success">'+data.success+'</p>');
          $("#quiz_wrapper").html(data.html);
          $.fn.snackbar(data.success);
        }
        $.fn.ShareQuizResult();
      }
    });
  });

  $(document.body).on("submit","#pollForm", function(e){
    e.preventDefault();
    $(".invalid-feedback").remove();
    $(".text-success").remove();
    //$(".loader").remove();
    frm = $("#pollForm");
    $("#snackbar").html('');

    $.ajax({
      type: "post",
      url:  frm.attr('action'),
      data: frm.serialize(),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      beforeSend: function() {
        //frm.append("<div class='loader'><p>Loading...</p></div>");
        $('.btn-poll').prop('disabled', true);
      },
      success: function(data)
      {
        $('.btn-poll').prop('disabled', false);
        //$(".loader").remove();
        if($.isEmptyObject(data.success)){
          $.each( data.error, function( key, value ) {
            $("."+key).append('<span class="invalid-feedback d-block"><strong>'+value+'</strong></span>');
          });
        }
        else{
          frm[0].reset();
          frm.append('<p class="text-success">'+data.success+'</p>');
          $("#poll_wrapper").fadeOut();
          $.fn.snackbar(data.success);
        }
        $.fn.SharePollResult();
      }
    });
  });

  /*$(document.body).on("click","#btn-next-poll", function(e){
    e.preventDefault();
    $.ajax({
      type: "get",
      url:  $(this).attr('href'),
      headers: {
        'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
      },
      dataType:'JSON',
      success: function(data)
      {
        $("#poll_wrapper").html(data.html);
      }
    });
  });*/
  
  $.fn.snackbar=function(message){
    $("#snackbar").html(message);
    var x = document.getElementById("snackbar");
      x.className = "show";
      setTimeout(function(){ 
        x.className = x.className.replace("show", ""); 
      }, 3000);
  };
</script>

<div id="snackbar"></div>
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
<script>
  Echo.channel('ShowQuizChannel')
                .listen('ShwoQuizEvent', (e) => {
                  var url = "{{ route('show_webinar', ['company'=>$company->slug, 'webinar'=>$webinar->slug]) }}";
                  $.get(url,{type:'quiz'}, function(data){
                    $('#quiz_wrapper').html(data.html);
                    console.log(data);
                  });

                  //$('#quiz_wrapper').html(e.html);
                  //console.log(e);
                });
  Echo.channel('ShowPollChannel')
                .listen('ShowPollEvent', (e) => {
                  var url = "{{ route('show_webinar', ['company'=>$company->slug, 'webinar'=>$webinar->slug]) }}";
                  $.get(url,{type:'poll'}, function(data){
                    $('#poll_wrapper').html(data.html);
                    console.log(data);
                  });
                  //$('#poll_wrapper').html(e.poll);
                  //console.log(e);
                });

  Echo.channel('SharePollResultChannel')
                .listen('SharePollResultEvent', (e) => {
                  $.fn.SharePollResult();
                  //$('#poll_wrapper').html(e.poll);
                  //console.log(e);
                });

                $.fn.SharePollResult=function(){
                  var pollResultShow = "{{ route('poll-result-show', ['webinar'=>$webinar->id]) }}";
                  $.get(pollResultShow,{type:'poll'}, function(data){
                    $('#poll_result_wrapper').html(data.html);
                    console.log(data);
                  });
                }

  Echo.channel('ShareQuizResultChannel')
              .listen('ShareQuizResultEvent', (e) => {
                $.fn.ShareQuizResult();
                //$('#poll_wrapper').html(e.poll);
                //console.log(e);
              });
    
    $.fn.ShareQuizResult=function(){
      var quizResultShow = "{{ route('quiz-result-show', ['webinar'=>$webinar->id]) }}";
      $.get(quizResultShow,{type:'quiz'}, function(data){
        $('#quiz_result_wrapper').html(data.html);
        console.log(data);
      });
    }
  
</script>
</body>
</html>