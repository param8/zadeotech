@extends('layouts.web-app')
@section('content')
<!-- ***** Welcome Area Start ***** -->
<section id="home" class="section welcome-area overflow-hidden d-flex align-items-center">
    <div class="container">
        <div class="row align-items-center">

             @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
        @endif

            <!-- Welcome Intro Start -->
            <div class="col-12 col-md-7">
                <div class="welcome-intro">
                    <h1>PROPEL YOUR BRAND WITH ZADEOTECH CONSULTANCY</h1>
                    <h4>Innovative Digital Solutions</h4>
                    <p class="my-4">ZADEOTECH CONSULTANCY is NCR based Consulting Firm that has been delivering solutions to clients since 2014.
We offer an impressive portfolio of professional consulting services that are completely customized for your busines. Whatever your needs may be, we can make it happen. Get in touch with us TODAY.</p>
                    <!-- Buttons -->
                    <div class="button-group">
                        <a href="https://zadeotech.in" class="btn btn-bordered">More about us</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Welcome Area End ***** -->

<!-- ***** Service Area End ***** -->
<section id="service" class="section service-area ptb_100 pt-md-0">
    <div class="container">
            <h2 class="text-center mb-3">ZADEOTECH SOLUTIONS</h2>
        <div class="row">
            <div class="col-12 col-md-6 col-lg-4">
                <!-- Single Service -->
                <div class="single-service p-4">
                    <span class="flaticon-rocket-launch color-1 icon-bg-1"></span>
                    <h3 class="my-3">HR SOLUTIONS</h3>
                    <ul class="ul-style">
                        <li>Organizational Management</li>
                        <li>Payroll and Compliance services</li>
                        <li>Assessment and Training services</li>
                        <li>Training and Placement</li>
                        <li>Support and Advisory services</li>
                    </ul>
                    <!--
                    <a class="service-btn mt-3" href="#">Learn More</a>
                    -->
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <!-- Single Service -->
                <div class="single-service p-4">
                    <span class="flaticon-monitoring color-2 icon-bg-2"></span>
                    <h3 class="my-3">DIGITAL MARKETING SOLUTIONS</h3>
                    <ul class="ul-style">
                        <li>Social Media & Advertising</li>
                        <li>Online Reputation Management</li>
                        <li>Search Engine Marketing</li>
                        <li>Email & Hosting Solution</li>
                        <li>Experiential marketing</li>
                    </ul>
                    <!--
                    <a class="service-btn mt-3" href="#">Learn More</a>
                    -->
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-4">
                <!-- Single Service -->
                <div class="single-service p-4">
                    <span class="flaticon-web color-3 icon-bg-3"></span>
                    <h3 class="my-3">LIVE STREAMING SOLUTIONS</h3>
                    <ul class="ul-style">
                        <li>Webcasting/Live Streaming</li>
                        <li>Customized Webinar Platform</li>
                        <li>Social Media Live Streaming</li>
                        <li>E-Learning Solutions</li>
                        <li>Video Conferencing Solutions</li>
                    </ul>
                    <!--
                    <a class="service-btn mt-3" href="#">Learn More</a>
                    -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Service Area End ***** -->

<!-- ***** Video Area Start ***** -->
<section class="section video-area bg-grey ptb_150">
    <!-- Shape Top -->
    <div class="shape shape-top">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none" fill="#FFFFFF">
            <path class="shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
        c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
        c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"></path>
        </svg>
    </div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-7">
                <!-- Section Heading -->
                <div class="section-heading text-center">
                    <h5>ABOUT US</h5>
                    <h2>Committed to Excellence</h2>
                    <p class="d-none d-sm-block mt-4">ZADEOTECH CONSULTANCY offers the comprehensive capabilities and deep industry knowledge necessary to help you solve the most complex issues of your organization. Since opening our doors in 2014, we’re proud to say that each year we have a bigger list of returning and new clients.</p>
                    <p class="d-block d-sm-none mt-4">Want to experience the expertise of ZADEOTECH CONSULTANCY for yourself? Give us a call today and let’s discuss what we can do for you.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center align-items-center">
            <div class="col-12 col-md-9">
               <img src="{{ asset('frontend/assets/img/live-stream.jpg') }}">
            </div>
        </div>
    </div>
    <!-- Shape Bottom -->
    <div class="shape shape-bottom">
        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none" fill="#FFFFFF">
            <path class="shape-fill" d="M421.9,6.5c22.6-2.5,51.5,0.4,75.5,5.3c23.6,4.9,70.9,23.5,100.5,35.7c75.8,32.2,133.7,44.5,192.6,49.7
c23.6,2.1,48.7,3.5,103.4-2.5c54.7-6,106.2-25.6,106.2-25.6V0H0v30.3c0,0,72,32.6,158.4,30.5c39.2-0.7,92.8-6.7,134-22.4
c21.2-8.1,52.2-18.2,79.7-24.2C399.3,7.9,411.6,7.5,421.9,6.5z"></path>
        </svg>
    </div>
</section>
<!-- ***** Video Area End ***** -->

<!-- ***** Review Area Start ***** -->
<section id="review" class="section review-area bg-overlay ptb_100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 col-lg-7">
                <!-- Section Heading -->
                <div class="section-heading text-center">
                    <h2 class="text-white">MEET THE TEAM</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- Client Reviews -->
            <div class="client-reviews owl-carousel">
                <!-- Single Review -->
                <div class="single-review p-5">
                    <!-- Review Content -->
                    <div class="review-content">
                        <!-- Review Text -->
                        <div class="review-text">
                            <p>A serial entrepreneur with a history in bootstrapping and scaling grass-root ventures in the most sustainable manner.</p>

<p>Currently Founder of CoworkIn, Chairman of GEN Asia, Entrepreneurship Advocate and a Farmer.</p>
                        </div>
                        <!-- Quotation Icon -->
                        <div class="quot-icon">
                            <img class="avatar-sm" src="{{ asset('frontend/assets/img/quote.png') }}">
                        </div>
                    </div>
                    <!-- Reviewer -->
                    <div class="reviewer media mt-3">
                        <!-- Reviewer Thumb -->
                        <div class="reviewer-thumb">
                            <img class="avatar-lg radius-100" src="{{ asset('frontend/assets/img/team/yatin.jpg') }}">
                        </div>
                        <!-- Reviewer Media -->
                        <div class="reviewer-meta media-body align-self-center ml-4">
                            <h5 class="reviewer-name color-primary mb-2">YATIN THAKUR</h5>
                            <!--
                            <h6 class="text-secondary fw-6">CEO, Themeland</h6>
                        -->
                        </div>
                    </div>
                </div>
                <!-- Single Review -->
                <div class="single-review p-5">
                    <!-- Review Content -->
                    <div class="review-content">
                        <!-- Review Text -->
                        <div class="review-text">
                            <p>Neisha has 15+ of experience in various multinationals Heading HR and Recruitment processes. A Yoga practitioner, She is a MBA graduate with a degree in Psychology.</p>
                        </div>
                        <!-- Quotation Icon -->
                        <div class="quot-icon">
                            <img class="avatar-sm" src="{{ asset('frontend/assets/img/quote.png') }}">
                        </div>
                    </div>
                    <!-- Reviewer -->
                    <div class="reviewer media mt-3">
                        <!-- Reviewer Thumb -->
                        <div class="reviewer-thumb">
                            <img class="avatar-lg radius-100" src="{{ asset('frontend/assets/img/team/nikki.jpg') }}">
                        </div>
                        <!-- Reviewer Media -->
                        <div class="reviewer-meta media-body align-self-center ml-4">
                            <h5 class="reviewer-name color-primary mb-2">NEISHA BHATNAGAR</h5>
                            <!--
                            <h6 class="text-secondary fw-6">Founder, Themeland</h6>
                            -->
                        </div>
                    </div>
                </div>
                <!-- Single Review -->
                <div class="single-review p-5">
                    <!-- Review Content -->
                    <div class="review-content">
                        <!-- Review Text -->
                        <div class="review-text">
                            <p>Deepak is Guru of Digital Media and Marketing. He has an in-depth experience in offline marketing. His expertise includes building strategies for all the digital platforms to help you take a substantial digital leap.</p>
                        </div>
                        <!-- Quotation Icon -->
                        <div class="quot-icon">
                            <img class="avatar-sm" src="{{ asset('frontend/assets/img/quote.png') }}">
                        </div>
                    </div>
                    <!-- Reviewer -->
                    <div class="reviewer media mt-3">
                        <!-- Reviewer Thumb -->
                        <div class="reviewer-thumb">
                            <img class="avatar-lg radius-100" src="{{ asset('frontend/assets/img/team/deepak.jpg') }}">
                        </div>
                        <!-- Reviewer Media -->
                        <div class="reviewer-meta media-body align-self-center ml-4">
                            <h5 class="reviewer-name color-primary mb-2">DEEPAK THAKKAR</h5>
                            <!--
                            <h6 class="text-secondary fw-6">CEO, Themeland</h6>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ***** Review Area End ***** -->

<!--====== Contact Area Start ======-->
<section id="contact" class="contact-area ptb_100">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-12 col-lg-5">
                <!-- Section Heading -->
                <div class="section-heading text-center mb-3">
                    <h2>Let's connect!</h2>
                    <p class="d-none d-sm-block mt-4">13/28, LGF, East Patel Nagar, New Delhi - 110008</p>
                </div>
                <!-- Contact Us -->
                <div class="contact-us">
                    <ul>
                        <!-- Contact Info -->
                        <li class="contact-info color-1 bg-hover active hover-bottom text-center p-5 m-3">
                            <span><i class="fas fa-mobile-alt fa-3x"></i></span>
                            <a class="d-block my-2" href="tel:+91-9250002999">
                                <h3>+91-9250002999</h3>
                            </a>
                        </li>
                        <!-- Contact Info -->
                        <li class="contact-info color-3 bg-hover active hover-bottom text-center p-5 m-3">
                            <span><i class="fas fa-envelope-open-text fa-3x"></i></span>
                            <a class="d-none d-sm-block my-2" href="#">
                                <h3>mail@zadeotech.in</h3>
                            </a>
                            <a class="d-block d-sm-none my-2" href="mailto:mail@zadeotech.in">
                                <h3>mail@zadeotech.in</h3>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-12 col-lg-6 pt-4 pt-lg-0">
                <!-- Contact Box -->
                <div class="contact-box text-center">
                    <!-- Contact Form -->
                    <form method="POST" action="{{ route('contact-us') }}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Name" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" name="email" placeholder="Email" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="phone" placeholder="Phone" required="required">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" name="message" placeholder="Message" required="required"></textarea>
                                </div>
                                <div class="form-group">
                                    <select class="form-control" name="intrested_in" required="required">
                                        <option value="">Interested in?</option>
                                        <option value="HR Solution">HR Solution</option>
                                        <option value="Digital Marketing Solution">Digital Marketing Solution</option>
                                        <option value="Live Streaming Solution">Live Streaming Solution</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-bordered active btn-block mt-3"><span class="text-white pr-3"><i class="fas fa-paper-plane"></i></span>Send Message</button>
                            </div>
                        </div>
                    </form>
                    <p class="form-message"></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== Contact Area End ======-->

<!--====== Call To Action Area Start ======-->
<section class="section cta-area bg-overlay ptb_100">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-12 col-lg-10">
                <!-- Section Heading -->
                <div class="section-heading text-center m-0">
                    <h2 class="text-white">Brand Strategy & Innovation</h2>
                    <p class="text-white d-none d-sm-block mt-4">PROPEL YOUR BRAND WITH ZADEOTECH CONSULTANCY</p>
                    <a href="#contact" class="btn btn-bordered-white mt-4">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--====== Call To Action Area End ======-->
@endsection