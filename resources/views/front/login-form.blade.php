<div class="sideForm brdr-radius-15 shadow-box">
  <div class="text-center py-2 w-100 heading-box">
    <h4 class="text-white mb-0 lead"> Login Here </h4>
  </div>
  <div class="py-1 px-3 mb-3 mt-3 w-100" id="formWrapper">
    <form method="POST" action="{{ route('user_login') }}">
        @csrf
        {{--
        @if($errors->any())
            {{ implode('', $errors->all('<div>:message</div>')) }}
        @endif
        --}}
        @if(\Session::has('error'))
          <div class="alert alert-danger">{{\Session::get('error')}}</div>
        @endif
      <input type="hidden" name="webinar" value="{{$webinar->id}}">
     
      <div class="form-group has-feedback">
          <input required="" type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{old('email')}}">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @error('email')
                <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> 
            @enderror
      </div>
      

      <div class="form-group">
        <button type="submit" class="btn btn-primary"> LOGIN </button>
        <p class="mt-2">New user - <a href="{{route('show_webinar', ['company'=>$webinar->company->slug, 'webinar'=>$webinar->slug])}}">Login</a></p>
      </div>
    </form>
  </div>
</div>