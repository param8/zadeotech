@php ($polls = \App\Helpers\Helper::sharePollResultView($webinar->id)) @endphp
@if($polls->count())
<div class="sideForm brdr-radius-15 shadow-box">
	<div class="text-center py-2 w-100 heading-box">
		<h4 class="text-white mb-0 lead"> Poll Result </h4>
	</div>
	<div class="py-1 px-3 mb-3 mt-3 w-100">
		@forelse($polls as $key=>$pol)
		@php($total_votes = $pol->user_poll_answers->count())
			{!! $key!=0 ? '<hr style="border:0; border-bottom:dashed 1px #ccc">' : null !!}
			<div>{{$pol->poll}}</div>
				@if($pol->poll_options->count())
					<div class="poll-result">
						<ol type="A" class="ol-style poll_option">
						  @foreach ($pol->poll_options as $poll_option)
						  <div class="poll-question">
						    <li>{{$poll_option->option}}
						      @php($poll_option_total_votes = $poll_option->user_poll_answers->count())
						      @if($poll_option_total_votes)
						        @php($poll_option_ans_percent = round(($poll_option_total_votes/$total_votes)*100))
						        <div class="result-bar" style= "width:{{$poll_option_ans_percent}}%">
						          <label for="{{$poll_option->id}}p">{{$poll_option_ans_percent}}%</label>
						        </div>
						      @else
						      <div class="result-bar" style= "width:0%">
						          <label>0%</label>
						        </div>
						      @endif
						    </li>
						  </div>
						  @endforeach
						</ol>
					</div>
				@else
			<small class="text-muted">No Options</small>
		@endif
		@endforeach
	</div>
</div>
@endif