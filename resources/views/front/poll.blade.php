@if(!empty($polls) and $polls->count())
<div class="sideForm brdr-radius-15 shadow-box">
  <div class="text-center py-2 w-100 heading-box">
    <h4 class="text-white mb-0 lead"> Poll </h4>
  </div>
  <div class="py-1 px-3 mb-3 mt-3 w-100">
    @foreach($polls as $key=>$poll)
    <form id="pollForm" action="{{ route('user_poll_answer',['webinar'=>$webinar->id, 'poll'=>$poll->id]) }}" method="post">
      <p class="text-bold"><strong>{{$poll->poll}}</strong></p>
      <ol type="A" class="ol-style poll_option">
        @foreach($poll->poll_options as $key=>$option)
          <li><label for="{{$option->id}}p">{{$option->option}}</label> <input type="radio" name="poll_option" id="{{$option->id}}p" value="{{$option->id}}"></li>
        @endforeach
      </ol>
    <button type="submit" class="btn btn-primary mb-2 btn-poll">SUBMIT</button>
    </form>
    @endforeach
    @if($polls->total()>1)
    <hr>
    @endif
    {{$polls->links('vendor.pagination.bootstrap-4')}}
  </div>
</div>
@endif