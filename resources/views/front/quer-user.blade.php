<div class="sideForm brdr-radius-15 shadow-box">
  <div class="text-center py-2 w-100 heading-box">
    <h4 class="text-white mb-0 lead font-16"> Please submit your Questions/Comments </h4>
  </div>
  <div class="py-1 px-3 mb-3 mt-3 w-100">
  	<form action="{{ route('user_query',['company'=>$company->slug,'webinar'=>$webinar->slug]) }}" method="post" id="queryForm">
  		<div class="form-group has-feedback user_query">
  			<textarea class="query form-control{{ $errors->has('query') ? ' is-invalid' : '' }}" name="user_query"></textarea>
  			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @error('query')
                <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> 
            @enderror
  		</div>
  		<div class="form-group">
        	<button type="submit" class="btn btn-primary btn-query"> SUBMIT </button>
      	</div>
  	</form>
  </div>
</div>