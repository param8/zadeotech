@php ($quizzes1 = \App\Helpers\Helper::shareQuizResultView($webinar->id)) @endphp
@if($quizzes1->count())
<div class="sideForm brdr-radius-15 shadow-box">
  <div class="text-center py-2 w-100 heading-box">
    <h4 class="text-white mb-0 lead"> Quiz Result </h4>
  </div>
  @forelse($quizzes1 as $key=>$quiz)
  <div class="py-1 px-3 mb-1 mt-1 w-100">
    {!! $key!=0 ? '<hr style="border:0; border-bottom:dashed 1px #ccc">' : null !!}
  <p>{{$quiz->question}}</p>
    @forelse($quiz->options as $k=>$opt)
      <div class="{{$opt->is_correct==true?'text-success':'text-danger'}}">{{$k+1}}. {{$opt->option}}</div>
    @empty
    <small class="text-muted">No Options</small>
    @endforelse

    <div class="mt-1">
    <?php $quiz_ans = \App\Helpers\Helper::getQuizSummery($quiz); ?>
    Result Summery : {{ $quiz_ans['total_correct_answer'] }}/{{ $quiz_ans['total_answers'] }} ({{ $quiz_ans['percent'] }})
    </div>

  </div>
  @empty
  <p>Empty</p>
  @endforelse
  <br>
</div>
@endif