@if(!empty($quizzes) and $quizzes->count())
<div class="sideForm brdr-radius-15 shadow-box">
  <div class="text-center py-2 w-100 heading-box">
    <h4 class="text-white mb-0 lead"> Quiz </h4>
  </div>
  <div class="py-1 px-3 mb-3 mt-3 w-100">
  	@foreach($quizzes as $key=>$quiz)
    <form id="quizForm" action="{{ route('user_quiz_answer',['webinar'=>$webinar->id, 'quiz'=>$quiz->id]) }}" method="post">
      <p class="text-bold"><strong>{{$quiz->question}}</strong></p>
      <ol class="ol-style quiz_option" type="A">
        @foreach($quiz->options as $key=>$option)
          <li><label for="{{$option->id}}">{{$option->option}}</label> <input type="radio" name="quiz_option" id="{{$option->id}}" value="{{$option->id}}"></li>
        @endforeach
      </ol>
    <button type="submit" class="btn btn-primary mb-2 btn-quiz">SUBMIT</button>
    </form>
    @endforeach
    @if($quizzes->total()>1)
    <hr>
    @endif
    {{$quizzes->links('vendor.pagination.bootstrap-4')}}
  </div>
</div>
@endif