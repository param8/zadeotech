<div class="sideForm brdr-radius-15 shadow-box">
  <div class="text-center py-2 w-100 heading-box">
    <h4 class="text-white mb-0 lead"> Register Here </h4>
  </div>
  <div class="py-1 px-3 mb-3 mt-3 w-100" id="formWrapper">
    <form method="POST" action="{{ route('user_register') }}">
        @csrf
        @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li class="text-danger">{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
      <input type="hidden" name="webinar" value="{{$webinar->id}}">
      <div class="form-group has-feedback">
          <input required="" type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Full Name" value="{{old('name')}}">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @error('name')
                <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> 
            @enderror
      </div>
     
      <div class="form-group has-feedback">
          <input required="" type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" value="{{old('email')}}">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @error('email')
                <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> 
            @enderror
      </div>
      <div class="form-group has-feedback">
          <div class="input-group-addon mt-2"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
          <input required="" type="text" name="phone" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="Contact Number" value="{{old('phone')}}">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @error('phone')
                <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> 
            @enderror
      </div>
      <div class="form-group has-feedback">
          <select required="" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country"  id="country">
            <option value="">Select Country</option>
            @foreach($countries as $key=>$value)
                <option {{old('country')==$value->id?'selected':null}} value="{{$value->id}}">{{$value->name}}</option>
            @endforeach
          </select>
          @error('country')
                <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> 
            @enderror
      </div>
      <div class="form-group has-feedback" id="states_container">
        @if(old('state'))
          @php($states = \App\Models\State::whereCountry_id(old('country'))->whereStatus(1)->orderBy('name')->get())
          @include('front.states')
        @endif
      </div>

      @if(!empty($webinar->optional_field))
      <div class="form-group has-feedback">
          <div class="input-group-addon mt-2"> <i class="fa fa-phone" aria-hidden="true"></i> </div>
          <input required="" type="text" name="optional_field" class="form-control{{ $errors->has('optional_field') ? ' is-invalid' : '' }}" placeholder="{{ $webinar->optional_field }}" value="{{old('optional_field')}}">
          <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            @error('optional_field')
                <span class="invalid-feedback"> <strong>{{ $message }}</strong> </span> 
            @enderror
      </div>
      @endif

      <div class="form-group">
        <button type="submit" class="btn btn-primary"> REGISTER </button>
        <p class="mt-2">Already registered? <a href="?login=true">Login</a></p>
      </div>
    </form>
  </div>
</div>