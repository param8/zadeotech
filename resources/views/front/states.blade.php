<div class="form-group has-feedback" id="state">
  <select class="form-control" name="state"  id="state" required>
    <option>Select State</option>
    @forelse($states as $key=>$value)
      <option {{old('state')==$value->id?'selected':null}} value="{{$value->id}}">{{$value->name}}</option>
      @empty
      <option value="100000">Other Provinces</option>
    @endforelse
  </select>
</div>