<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    
    <li class="nav-item">
      <a href="{{ route('admin.dashboard') }}" class="nav-link @yield('active-dashboard')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('admin.company') }}" class="nav-link @yield('active-company-list')">
        <i class="nav-icon fas fa fa-building"></i>
        <p>
          Company
        </p>
      </a>
    </li>
    @if(Auth::guard('admin')->check() and Auth::guard('admin')->id()==1)
    <li class="nav-item">
      <a href="{{ route('admin.company.trashed') }}" class="nav-link @yield('active-trashed')">
        <i class="far fa-recycle fa nav-icon"></i>
        <p>
          Recycle Company
        </p>
      </a>
    </li>
    @endif
    <li class="nav-item">
      <a href="{{ route('admin.change-password') }}" class="nav-link @yield('active-change-password')">
        <i class="far fa-key fa nav-icon"></i>
        <p>
          Change Password
        </p>
      </a>
    </li>
    @if(Auth::guard('admin')->check() and Auth::guard('admin')->id()==1)
    <li class="nav-item">
      <a href="{{ route('admin.manager') }}" class="nav-link @yield('active-manager')">
        <i class="far fa-user-tie fa nav-icon"></i>
        <p>
          Manager
        </p>
      </a>
    </li>
    @endif
    
  </ul>
</nav>
<!-- /.sidebar-menu -->