<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{asset(env('APP_LOGO'))}}">

        <title>@yield('title')</title>

        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custom-style.css')}}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custom-style.css')}}">

    </head>
    <body>
        
    <div class="container-fluid">
        <div class="container">
            <div class="mt-2 mb-2"> <img src="{{asset('frontend/images/logo.png')}}"> </div>
                @yield('content')
            </div>
            <!--./container-->
        </div>
    <!--./container-fluid-->
    <div class="footer-box text-center">
      <p class="mb-0">&copy; {{date('Y')}} <a href="https://www.zadeotech.in/">zadeotech.in</a></p>
    </div>
    </body>
</html>
