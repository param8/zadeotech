<div class="user-panel mt-2 pb-2 mb-2" style="max-height: 200px; overflow: auto">
    <img src="@yield('logo')" class="elevation-2">
</div>
<!-- Sidebar Menu -->
<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    
    <li class="nav-item">
      <a href="{{ route('company.dashboard') }}" class="nav-link @yield('active-dashboard')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('company.webinar.create') }}" class="nav-link @yield('active-webinar-create')">
        <i class="far fa-pager fa nav-icon"></i>
        <p>
          Create Webinar
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('company.webinar') }}" class="nav-link @yield('active-webinar-list')">
        <i class="far fa fa-server nav-icon"></i>
        <p>
          Webinar List
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('company.manage-profile') }}" class="nav-link @yield('active-manage-profile')">
        <i class="far fa-user nav-icon"></i>
        <p>
          Manage Profile
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('company.change-password') }}" class="nav-link @yield('active-change-password')">
        <i class="far fa-key fa nav-icon"></i>
        <p>
          Change Password
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('company.change-bg') }}" class="nav-link @yield('active-change-bg')">
        <i class="far fa-image fa nav-icon"></i>
        <p>
          Change Background
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('company.webinar.hall') }}" class="nav-link @yield('active-hall')">
        <i class="far fa fa-server nav-icon"></i>
        <p>
          Hall
        </p>
      </a>
    </li>
  </ul>
</nav>
<!-- /.sidebar-menu -->