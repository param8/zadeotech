<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="shortcut icon" href="{{asset(env('APP_LOGO'))}}">

        <title>@yield('title')</title>

        <link rel="stylesheet" href="{{asset('frontend/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('frontend/css/custom-style.css')}}">
    </head>
    <body>
        
    <div class="container-fluid">
        <div class="row">
        <div class="col-sm-12">
        <div class="inner-content">
        <div class="text-center logo"> <img src="{{asset(env('APP_LOGO'))}}"> </div>
        <br>

        

        @yield('content')

        </div>
        <!-- ./inner-content-->
        </div>
        <!--./col-sm-12-->
        </div>
        <!--./row-->
        <div class="footer-box footer-bootom text-center">
        <p class="blue-color mb-0">&copy; {{date('Y')}} zadeotech.in</p>
        </div>

    </div>
    <!--./container-fluid--> 

    <script src="{{asset('frontend/js/jquery-3.4.1.min.js')}}"></script>
    <script src="{{asset('frontend/js/popper.min.js')}}" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    <script src="{{asset('frontend/js/custom.js')}}"></script>
    </body>
</html>
