<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <li class="nav-item">
      <a href="{{ route('moderator.dashboard') }}" class="nav-link @yield('active-dashboard')">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Dashboard
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ route('moderator.webinar') }}" class="nav-link @yield('active-webinar-list')">
        <i class="far fa fa-server nav-icon"></i>
        <p>
          Webinar List
        </p>
      </a>
    </li>
  </ul>
</nav>
<!-- /.sidebar-menu -->