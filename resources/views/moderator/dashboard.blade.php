@extends('layouts.dashboard')
@php($user = admin_loggedin())
@section('active-dashboard','active')
@section('title','Dashboard')
@section('logo',$user->logo)
@section('user_name',$user->first_name)
@section('content')
<div class="row mb-2">
<div class="col-sm-12">
  <h1 class="m-0 text-dark">{{$user->company_name}}</h1>
</div><!-- /.col -->
</div><!-- /.row -->

<div class="card card-primary card-outline">
  <div class="card-header">
    <h5 class="m-0">Dashboard</h5>
  </div>
  <div class="card-body">
  	<br>
    @component('components.alert')
      @endcomponent
  	<h1 class="text-center">Welcome to dashboard</h1>
  </div>
</div>
@endsection