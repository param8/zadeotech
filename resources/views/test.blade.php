<!DOCTYPE html>
<html>
<head>
	<title>Test Broadcast</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/app.css')}}">
	<meta name="csrf-token" content="{{ csrf_token() }}">


</head>
<body>
 
<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
<script>
	Echo.channel('testChannel')
                .listen('TestEvent', (e) => {
                    console.log(e);
                });
</script>
</body>
</html>