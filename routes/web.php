<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
 
use App\Http\Controllers\Admin\LoginController as AdminLoginController;
use App\Http\Controllers\Admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\Admin\CompanyController as AdminCompanyController;
use App\Http\Controllers\Admin\ManagerController;


use App\Http\Controllers\Company\LoginController as CompanyLoginController;
use App\Http\Controllers\Company\DashboardController as CompanyDashboardController;
use App\Http\Controllers\Company\WebinarController;
use App\Http\Controllers\Company\QuizController;
use App\Http\Controllers\Company\PollController;
use App\Http\Controllers\Company\TickerController;
use App\Http\Controllers\Company\UserController as CompanyUserController;
use App\Http\Controllers\Company\ModeratorController as CompanyModeratorController;

use App\Http\Controllers\Moderator\LoginController as ModeratorLoginController;
use App\Http\Controllers\Moderator\DashboardController as ModeratorDashboardController;


use App\Http\Controllers\Front\FinessseController;
use App\Http\Controllers\Front\CronController;
use App\Http\Controllers\TestController;
use App\Events\TestEvent;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/event', function () {
    event(new TestEvent('Hay, How r u.'));
});

Route::get('/listen', function () {
    return view('test');
});

*/
Route::get('test', [TestController::class, 'index']);
Route::post('logout_user', [FinessseController::class, 'logout'])->name('logout_user');
Route::post('contact-us', [FinessseController::class, 'contact_us'])->name('contact-us');

Route::get('/', function () {
    return view('front.index');
})->name('home');

Route::get('/home', function () {
	/*$user = \App\Models\User::find(\Auth::id());
	$webinar = $user->webinars()->first();
	$company = $user->company;
	Redirect::route();*/
	if(\Session::has('webinar'))
	{
		$webinar = \Session::get('webinar');
		return Redirect::route('show_webinar',['company'=>$webinar->company->slug,'webinar'=>$webinar->slug]);
	}
});


/*
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
*/

/***** User Must Verify *****/
/*
Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware(['auth'])->name('verification.notice');



Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('status', 'verification-link-sent');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::get('profile', function () {
    // Only verified users may enter...
})->middleware('verified');
*/
/***** End User Must Verify *****/

/** admin **/

Route::group(['prefix' => 'cron', 'as' => 'cron'], function() {
	Route::get('/webinar-reminder', [CronController::class, 'webinar_reminder']);
});


Route::group(['prefix' => 'admin', 'as' => 'admin'], function() {
	Route::group(['prefix' => 'forgot-password', 'as' => '.forgot-password'], function() {
		Route::get('/', [AdminLoginController::class, 'password_reset_form']);
		Route::post('/', [AdminLoginController::class, 'password_reset_request']);
	});

	Route::group(['prefix' => 'password/reset', 'as' => '.password.reset'], function() {
		Route::get('/{token}', [AdminLoginController::class, 'enter_password_form']);
		Route::post('/', [AdminLoginController::class, 'enter_reset']);
	});
});

Route::group(['prefix' => 'admin', 'as' => 'admin'], function() {
	Route::group(['prefix' => 'login', 'as' => '.login'], function() {
		Route::get('/', [AdminLoginController::class, 'login_form']);
		Route::post('/', [AdminLoginController::class, 'login']);
	});

	Route::group(['prefix' => 'change-password', 'as' => '.change-password'], function() {
		Route::get('/', [AdminDashboardController::class, 'change_password']);
		Route::post('/', [AdminDashboardController::class, 'update_password']);
	});
	
	Route::group(['prefix' => 'manager', 'as' => '.manager'], function() {
		Route::get('/', [ManagerController::class, 'index']);

		Route::group(['prefix' => 'create', 'as' => '.create'], function() {
			Route::get('/', [ManagerController::class, 'create']);
			Route::post('/', [ManagerController::class, 'store']);
		});

		Route::group(['prefix' => 'edit', 'as' => '.edit'], function() {
			Route::get('/{admin}', [ManagerController::class, 'edit']);
			Route::post('/{admin}', [ManagerController::class, 'update']);
		});
		
		Route::post('delete/{admin}', [ManagerController::class, 'destroy'])->name('.destroy');
	});

	Route::group(['prefix' => 'company', 'as' => '.company'], function() {
		Route::get('/', [AdminCompanyController::class, 'index']);
		Route::get('/export', [AdminCompanyController::class, 'export'])->name('.export');
		Route::post('/delete/{company}', [AdminCompanyController::class, 'delete'])->name('.delete');
		Route::post('/approve/{company}', [AdminCompanyController::class, 'approve'])->name('.approve');
		//Route::post('/disapprove/{company}', [AdminCompanyController::class, 'disapprove'])->name('.disapprove');

		Route::post('/deactivate/{company}', [AdminCompanyController::class, 'deactivate'])->name('.deactivate');
		Route::post('/reactivate/{company}', [AdminCompanyController::class, 'reactivate'])->name('.reactivate');

		Route::group(['prefix' => 'edit', 'as' => '.edit'], function() {
			Route::get('/{company}', [AdminCompanyController::class, 'edit']);
			Route::post('/{company}', [AdminCompanyController::class, 'update']);
		});

		Route::group(['prefix' => 'trashed', 'as' => '.trashed'], function() {
			Route::get('/', [AdminCompanyController::class, 'trashed']);
			Route::post('/restore/{company}', [AdminCompanyController::class, 'restore'])->name('.restore');
		});
	});
	Route::get('/dashboard', [AdminDashboardController::class, 'dashboard'])->name('.dashboard');
});
/** end admin **/

/** company **/
Route::group(['prefix' => 'company', 'as' => 'company'], function() {
	Route::group(['prefix' => 'forgot-password', 'as' => '.forgot-password'], function() {
		Route::get('/', [CompanyLoginController::class, 'password_reset_form']);
		Route::post('/', [CompanyLoginController::class, 'password_reset_request']);
	});

	Route::group(['prefix' => 'password/reset', 'as' => '.password.reset'], function() {
		Route::get('/{token}', [CompanyLoginController::class, 'enter_password_form']);
		Route::post('/', [CompanyLoginController::class, 'enter_reset']);
	});

	Route::group(['prefix' => 'change-password', 'as' => '.change-password'], function() {
		Route::get('/', [CompanyDashboardController::class, 'change_password']);
		Route::post('/', [CompanyDashboardController::class, 'update_password']);
	});
});

Route::group(['prefix' => 'login', 'as' => 'login'], function() {
	Route::get('/', [CompanyLoginController::class, 'login_form']);
	Route::post('/', [CompanyLoginController::class, 'login']);
});
Route::group(['prefix' => 'register', 'as' => 'company.register'], function() {
	Route::get('/', [CompanyLoginController::class, 'register_form']);
	Route::post('/', [CompanyLoginController::class, 'register']);
});

Route::group(['prefix' => 'company', 'as' => 'company', 'middleware'=>['force_to_change_pwd']], function() {
	Route::get('/dashboard', [CompanyDashboardController::class, 'dashboard'])->name('.dashboard');

	
	Route::group(['prefix' => 'change-bg', 'as' => '.change-bg'], function() {
		Route::get('/', [CompanyDashboardController::class, 'change_bg_form']);
		Route::post('/', [CompanyDashboardController::class, 'change_bg']);
		Route::post('/remove', [CompanyDashboardController::class, 'change_bg_remove'])->name('.change_bg_remove');
	});

	/*Route::group(['prefix' => 'login', 'as' => '.login'], function() {
		Route::get('/', [CompanyLoginController::class, 'login_form']);
		Route::post('/', [CompanyLoginController::class, 'login']);
	});
	Route::group(['prefix' => 'register', 'as' => '.register'], function() {
		Route::get('/', [CompanyLoginController::class, 'register_form']);
		Route::post('/', [CompanyLoginController::class, 'register']);
	});*/
	//webinar
	Route::group(['prefix' => 'webinar', 'as' => '.webinar', 'middleware'=>'validate_company'], function() {
		Route::get('/', [WebinarController::class, 'index']);
		Route::get('/delete-moderator/{moderator}', [WebinarController::class, 'delete_moderator'])->name('.delete_moderator');

		Route::group(['prefix' => '{webinar}/user-query', 'as' => '.user-query'], function() {
			Route::get('/', [WebinarController::class, 'user_query']);
			Route::get('/export', [WebinarController::class, 'user_query_export'])->name('.export');
		});



		Route::group(['prefix' => 'create', 'as' => '.create'], function() {
			Route::get('/', [WebinarController::class, 'create']);
			Route::post('/', [WebinarController::class, 'store']);
		});

		Route::group(['prefix' => 'edit', 'as' => '.edit'], function() {
			Route::get('/{webinar}', [WebinarController::class, 'edit']);
			Route::post('/{webinar}', [WebinarController::class, 'update']);
		});

		Route::group(['prefix' => 'hall', 'as' => '.hall'], function() {
			Route::get('/', [WebinarController::class, 'hall']);
			//Route::post('/', [WebinarController::class, 'store']);
		});

		Route::group(['prefix' => 'store-hall', 'as' => '.store-hall'], function() {
			Route::post('/', [WebinarController::class, 'store_hall']);
		});

		Route::group(['prefix' => 'get_hall_form', 'as' => '.get_hall_form'], function() {
			Route::post('/', [WebinarController::class, 'get_hall_form']);
		});

		// HALL
		//Route::get('hall', 'WebinarController@hall')->name('company.hall');
		// Route::group(['prefix' => 'hall', 'as' => '.hall', 'middleware'=>'validate_company'], function() {
		// 	Route::get('/', [WebinarController::class, 'hall']);
		// 	//Route::get('/delete-moderator/{moderator}', [WebinarController::class, 'delete_moderator'])->name('.delete_moderator');
	
		// 	});
	
		
		//quiz
		Route::group(['prefix' => '{webinar}/quiz', 'as' => '.quiz'], function() {
			Route::get('/', [QuizController::class, 'index']);
			Route::get('/export', [QuizController::class, 'export'])->name('.export');

			//Route::get('/result', [QuizController::class, 'result'])->name('.result');
			Route::group(['prefix' => 'result', 'as' => '.result'], function() {
				Route::get('/', [QuizController::class, 'result']);
				Route::get('/export', [QuizController::class, 'export_result'])->name('.export');
			});
			Route::group(['prefix' => 'create', 'as' => '.create'], function() {
				Route::get('/', [QuizController::class, 'create']);
				Route::post('/', [QuizController::class, 'store']);
			});
			Route::group(['prefix' => 'edit', 'as' => '.edit'], function() {
				Route::get('/{quiz}', [QuizController::class, 'edit']);
				Route::post('/{quiz}', [QuizController::class, 'update']);
			});			
			Route::get('/{quiz}/result', [QuizController::class, 'individual_result'])->name('.individual-result');
			Route::get('/{quiz}/result/export', [QuizController::class, 'individual_result_export'])->name('.individual-result.export');
			Route::get('/delete/{quiz}', [QuizController::class, 'destroy'])->name('.delete');
			Route::post('/status/{quiz}', [QuizController::class, 'status'])->name('.status');
			Route::post('/share-result/{quiz}', [QuizController::class, 'share_result'])->name('.share-result');
		});

		//poll
		Route::group(['prefix' => '{webinar}/poll', 'as' => '.poll'], function() {
			Route::get('/', [PollController::class, 'index']);
			Route::get('/export', [PollController::class, 'export'])->name('.export');
			
			//Route::get('/view-result', [PollController::class, 'view_result'])->name('.view-result');
			
			Route::group(['prefix' => 'create', 'as' => '.create'], function() {
				Route::get('/', [PollController::class, 'create']);
				Route::post('/', [PollController::class, 'store']);
			});
			Route::group(['prefix' => 'edit', 'as' => '.edit'], function() {
				Route::get('/{poll}', [PollController::class, 'edit']);
				Route::post('/{poll}', [PollController::class, 'update']);
			});	
			Route::get('/{poll}/result', [PollController::class, 'individual_result'])->name('.individual-result');
			Route::get('/{poll}/result/export', [PollController::class, 'individual_result_export'])->name('.individual-result.export');

			Route::get('/delete/{poll}', [PollController::class, 'destroy'])->name('.delete');
			Route::post('/status/{poll}', [PollController::class, 'status'])->name('.status');
			Route::post('/share-result/{poll}', [PollController::class, 'share_result'])->name('.share-result');
		});

		Route::group(['prefix' => '{webinar}/user', 'as' => '.user'], function() {
			Route::get('/', [CompanyUserController::class, 'user']);
			Route::get('/export', [CompanyUserController::class, 'export'])->name('.export');
		});

		Route::group(['prefix' => '{webinar}/ticker', 'as' => '.ticker'], function() {
			Route::get('/', [TickerController::class, 'index']);
			Route::post('/', [TickerController::class, 'updateOrCreate']);
		});

		Route::group(['prefix' => '{webinar}/moderator', 'as' => '.moderator'], function() {
		
			Route::get('/', [CompanyModeratorController::class, 'index']);

			Route::group(['prefix' => 'create', 'as' => '.create'], function() {
				Route::get('/', [CompanyModeratorController::class, 'create']);
				Route::post('/', [CompanyModeratorController::class, 'store']);
			});

			Route::group(['prefix' => 'edit', 'as' => '.edit'], function() {
				Route::get('/{moderator}', [CompanyModeratorController::class, 'edit']);
				Route::post('/{moderator}', [CompanyModeratorController::class, 'update']);
			});

			Route::get('/delete/{moderator}', [CompanyModeratorController::class, 'destroy'])->name('.detach');
		});


		Route::get('/remove-banner/{webinar}', [WebinarController::class, 'remove_banner'])->name('.remove-banner');
		Route::get('/remove-logo1/{webinar}', [WebinarController::class, 'remove_logo1'])->name('.remove-logo1');
		Route::get('/remove-logo2/{webinar}', [WebinarController::class, 'remove_logo2'])->name('.remove-logo2');
		Route::get('/remove-logo3/{webinar}', [WebinarController::class, 'remove_logo3'])->name('.remove-logo3');
		Route::get('/remove-header/{webinar}', [WebinarController::class, 'remove_header'])->name('.remove-header');

		Route::get('/show/{webinar}', [WebinarController::class, 'show'])->name('.show');
		Route::get('/delete/{webinar}', [WebinarController::class, 'destroy'])->name('.delete');
	});
	Route::group(['prefix' => 'manage-profile', 'as' => '.manage-profile'], function() {
		Route::get('/', [CompanyDashboardController::class, 'manage_profile']);
		Route::post('/', [CompanyDashboardController::class, 'update_profile']);
	});
});


/** end admin **/

/** moderator **/
Route::group(['prefix' => 'moderator', 'as' => 'moderator'], function() {
	Route::group(['prefix' => 'login', 'as' => '.login'], function() {
		Route::get('/', [ModeratorLoginController::class, 'login_form']);
		Route::post('/', [ModeratorLoginController::class, 'login']);
	});
	Route::get('/dashboard', [ModeratorDashboardController::class, 'dashboard'])->name('.dashboard');
	Route::get('/webinar', [ModeratorDashboardController::class, 'webinar'])->name('.webinar');
});
/** end moderator **/

//frontend
Route::post('/user_register', [FinessseController::class, 'user_register'])->name('user_register');
Route::post('/user_login', [FinessseController::class, 'user_login'])->name('user_login');

Route::post('/get_state', [FinessseController::class, 'get_state'])->name('get_state');

Route::post('/user_query/{company}/{webinar}', [FinessseController::class, 'user_query'])->name('user_query');

Route::post('/user_quiz_answer/{webinar}/{quiz}', [FinessseController::class, 'user_quiz_answer'])->name('user_quiz_answer');

Route::post('/user_poll_answer/{webinar}/{poll}', [FinessseController::class, 'user_poll_answer'])->name('user_poll_answer');

Route::get('/next_poll/{webinar}', [FinessseController::class, 'next_poll'])->name('next_poll');

Route::get('/quiz-result-show/{webinar}', [FinessseController::class, 'quiz_result'])->name('quiz-result-show');

Route::get('/poll-result-show/{webinar}', [FinessseController::class, 'poll_result'])->name('poll-result-show');


Route::get('/{company}/{webinar}/branding', [FinessseController::class, 'show_webinar_branding'])->name('show_webinar_branding');
Route::get('/{company}/{webinar}', [FinessseController::class, 'show_webinar'])->name('show_webinar');